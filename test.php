<?php wp_reset_query(); // reset query	 ?>
<?php
$args1 = array(
        'post_type' => 'publication',
        'posts_per_page'   => 3,
        'post_status'   =>  array('publish'),
        'post__not_in' => array($post->ID),
        'orderby'        =>'rand',
	    'order'     => 'desc'
    );

$the_query = new WP_Query($args1);

if ( $the_query->have_posts() ) {
 ?>
	<div class="recommended_reading fullwidth fleft">
		<h3 class="boldTitle">Recommended Reading</h3>
		<div class="row resource-library">
				<?php
				while ( $the_query->have_posts() ) : $the_query->the_post();
					// echo $resource_type_acf = get_field('resource_type_cf');exit;
				 	 // $resource_type_acf = get_post_meta($item->ID, "resource_type_cf", true);
				 	 /*$resource_type_acf = the_field('resource_type_cf',$item->ID)->name;

				      $resource_language_items =  get_field('resource_language_cf',$item->ID);

				      $resource_language_arr = array();
				      foreach ($resource_language_items as $key => $lang_item) {
				        array_push($resource_language_arr, $lang_item->name);
				      }

				      $language_acf = implode('/ ', $resource_language_arr);

				      $date_of_report_publication = date('Y',strtotime(get_field('date_of_report_publication',$item->ID)));
					                    <span class="resource-type">'.$resource_type_acf.'</span>
					                    <span class="resource-lang">'.$language_acf.'</span>
					                    <span class="report-date">'.$date_of_report_publication.'</span>
*/

					 	echo '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">';
						echo '<a href="'.get_the_permalink().'">
					                  <figure>
					                    <img src="'.wp_get_attachment_url(get_post_thumbnail_id($item->ID)).'" class="img-responsive" alt="">
					                  </figure>
					                  <aside class="withimage caption fullwidth fleft">
										<span>'.get_the_ID().'	</span>
					                    <h4>'.get_the_title().'</h4>

					                  </aside>
					                </a>';

					echo '</div>';
				 endwhile;
				  ?>
		</div>
	</div>
<?php } ?>

<?php if (count($recommended_reading)> 0 ): ?>
		<div class="fullwidth fleft">
			<h3><a href="<?php echo home_url().'/'.$post->post_type ?>">See all publications <span>&raquo;</span></a></h3>
		</div>
<?php endif ?>
<?php wp_reset_query(); // reset query	 ?>