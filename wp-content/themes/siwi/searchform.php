<div class="search">

<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">

		<input name="s" type="text" class="search_input" value="<?php the_search_query(); ?>" placeholder="Search..." />

		<input type="submit" class="search_submit" />

	</form>

</div>