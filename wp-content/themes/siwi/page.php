<?php get_header(); ?>
<?php the_post(); ?>
<main id="mainblock" role="main" class="commonpage content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</div>
			
			<div class="main_single col-md-8 col-sm-7 col-xs-12">
				<?php if (has_post_thumbnail()): ?>
					<div class="impact_figure fullwidth fleft">
					<figure class="featured-image">
						<?php the_post_thumbnail('large',array('class'=>'img-responsive')); ?>
					</figure>
					</div>
				<?php endif ?>
				<?php //if (has_post_thumbnail()): ?>
				<div class="preamble fullwidth fleft">
					<?php echo the_content(); ?>
				</div>
				<?php //endif ?>

				<?php 

				if( have_rows('see_all') ):
						// loop through the rows of data
					while ( have_rows('see_all') ) : the_row();
							$text = get_sub_field('text');
							$link = get_sub_field('link');
							$target = get_sub_field('target');
							$target = (!empty($target)) ? $target: '_self';

					$link_span = '<span><a target="'.$target.'" href="'.get_proper_url($link).'">'.$text.'</a></span>';
					if ($text && $link) {
						echo '<div class="more_media fullwidth fleft">
									'.$link_span.'
								</div>';
					}
						endwhile;
				endif;
					
				 ?>

				<?php include(locate_template('boxes/page-contacts.php')); ?>
				<?php include(locate_template('boxes/flexible-content-block.php')); ?>

				<div class="after_post fullwidth fleft">
						<?php include(locate_template('boxes/share-article.php')); ?>

				</div>
				<?php include(locate_template('boxes/related-news.php')); ?>
				<?php include(locate_template('boxes/related-publications.php')); ?>
				<?php include(locate_template('boxes/related-projects.php')); ?>

		</div>
			<div class=" col-md-4 col-sm-5 col-xs-12 pull-right">
				<?php get_template_part('boxes/sidebar','page-left'); ?>				
				<?php get_template_part('boxes/sidebar','right-widget'); ?>			

			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>