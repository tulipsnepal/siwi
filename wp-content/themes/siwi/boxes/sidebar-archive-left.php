<div class="sidebar_left">
	<div class="widget">
		
		<h3 class="area_title">News</h3>
		<?php
		$args = array(
		'taxonomy'     => 'news_category',
		'orderby'      => 'name',
		'show_count'   => 0,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0,
		'parent' => 796
		);
		?>
		<ul class="area_ul">
			<li><a href="<?php echo get_bloginfo('url').'/news';?>">All News</a><li>
			<?php wp_list_categories( $args ); ?>
		</ul>
	</div>
	
</div>