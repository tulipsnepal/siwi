<?php
$args1 = array(
	'type'                     => 'project',
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'project_category',
	'pad_counts'               => false 

); 
$project_categories = get_categories($args1);
?>
<div class="col-md-3 col-sm-5 col-xs-12 filter-project pull-right">
	<div class="filterpanel fullwidth fleft">
		<h3 class="panel-title">Filter</h3>
		<div class="list-group" id="accordion">
			<div class="list-group-item">
				<input class="content-toggle" name="filter-project_category" id="filter-project_category" value="all" type="checkbox">
				<label for="filter-project_category" class=" control-label">Project Type</label>
				<div class="content-accordion">
					<?php echo generateCheckbox('project_category',$project_categories); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="filterpanel fullwidth fleft">
		<h3 class="panel-title">Search</h3>
		<form class="searchform fullwidth fleft">
			<div class="form-group inputg">
				<input name="s" class="form-control" placeholder="Enter keywords..." id="s" type="text" value="<?php echo $search_query ?>">
				<label for="s">
					&nbsp;
				</label>
			</div>
			<button type="submit" id="searchsubmit" class="btn btn-warning btn-yellow">GO</button>
		</form>
	</div>

	<div class="filterpanel fullwidth fleft sidebar_left">
			
		<div class="widget">

			<div id="sub-page-menu" class="widget filterpanel fullwidth fleft">
				<h3 class="area_title panel-title"> </h3>
				<ul class="area_ul list-group fullwidth fleft">					
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/news' ) ) ; ?>">News</a></div>
					</li>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/publication' ) ) ; ?>">Publications</a></div>
					</li>
				</ul>
			</div>
		</div>
		
	</div>
</div>