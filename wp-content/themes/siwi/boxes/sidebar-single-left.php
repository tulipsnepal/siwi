<div id="sub-page-menu" class="sidebar_left widget filterpanel fullwidth fleft">
	<?php if(isset($post->post_type)) {
		switch($post->post_type) {
	case 'news': ?>
	
	<div class="widget">
		<h3 class="area_title  panel-title"><span>News</span></h3>
		<?php
		$args = array(
		'taxonomy'     => 'news_category',
		'orderby'      => 'name',
		'show_count'   => 0,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0,
		'parent' => 796
		);
		?>
		<ul class="area_ul list-group fullwidth fleft">
			<li><a href="<?php echo get_bloginfo('url').'/news';?>">All News</a><li>
			<?php wp_list_categories( $args ); ?>
		</ul>
	</div>
	<?php break; ?>
	
	<?php case 'publication': ?>
	
	<div class="widget">
		<h3 class="area_title panel-title">Publications</h3>
		<?php
		$exclude = isset($category->term_id) ? $category->term_id : 0;
		$args = array(
		'taxonomy'     => 'publication_category',
		'orderby'      => 'name',
		'show_count'   => 0,
		'pad_counts'   => 0,
		'hierarchical' => 1,
		'title_li'     => '',
		'hide_empty'   => 0,
		'exclude' => $exclude
		);
		?>
		<ul class="area_ul list-group fullwidth fleft">
			<?php wp_list_categories( $args ); ?>
		</ul>
	</div>
	
	<?php break; ?>
	
	<?php case 'project': ?>
	
	<div class="widget">
		<h3 class="area_title panel-title"><span>Project</span></h3>
		<?php
		$exclude = isset($category->term_id) ? $category->term_id : 0;
		$args = array(
		'taxonomy'     => 'project_category',
		'orderby'      => 'name',
		'hierarchical' => 1,
		'hide_empty'   => 0,
		'exclude' => $exclude,
		);
		$categories  = get_categories( $args );
		// echo "<pre>";print_r($categories);exit;
		?>
		<ul class="area_ul list-group fullwidth fleft">
			<?php foreach ($categories as $key => $category):
				$has_children_cls = ($category->parent==0) ? 'has_children': 'children';
			?>
				<li class="<?php echo $has_children_cls; ?>"><a href="<?php echo esc_url(get_term_link($category) ); ?>"><?php echo $category->name; ?></a></li>
			<?php endforeach ?>
		</ul>
	</div>
	
	<?php break; ?>
	
	
	<?php default: ?>
	<?php break; ?>
	
	<?php } /* end switch */ ?>
	
	<?php } /* end if */ ?>
	
</div>