<?php if(is_page()) :?>
<?php
global $post;
wp_reset_postdata();
$siwi_meta = get_post_meta($post->ID,'_siwi_contact_meta',true);

if(!empty($siwi_meta)) : ?>
<?php
$query_args = array(
'post_type' => 'siwi_contact',
'posts_per_page' => -1,
'post__in' => $siwi_meta/*,
'orderby' => 'title',
'order' => 'ASC'*/
);

$contact = new WP_Query($query_args);
// echo "<pre>";print_r($contact->posts);exit;
if($contact->have_posts()):
?>
<div class="featurebox contacts page-contacts fullwidth fleft">
	<!-- <h4>contacts</h4> -->
	<div class="contact_widget fullwidth fleft">
		<?php while($contact->have_posts()): $contact->the_post();
				$checked = in_array($post->ID, $siwi_meta) ? 'checked': '';

				$meta = get_post_meta($post->ID, '_siwi_meta', true);
				?>

		<div class="contactlist fleft">
			<figure>
				<?php
				if(has_post_thumbnail()) {
					the_post_thumbnail('contact-img',array( 'class' => 'img-responsive' ));
				}
				 ?>
			</figure>
			<aside class="withimage">
				<h5><?php the_title(); ?></h5>
				<?php if($meta['company']) { ?><span><?php echo $meta['company']; ?></span><?php } ?>
				<?php if($meta['position']) { ?><span><?php echo $meta['position']; ?></span><?php } ?>
				<?php if($meta['phone']) { ?><span><?php echo 'Phone: '.$meta['phone']; ?></span><?php } ?>
				<?php if ($meta['email']): ?>
					<span><a href="mailto:<?php echo $meta['email'] ?>"><?php echo $meta['email'] ?></a></span>
				<?php endif ?>

			</aside>
		</div>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>

	</div>
</div>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>