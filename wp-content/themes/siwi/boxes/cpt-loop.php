<?php while ( have_posts() ) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		    
	<header class="entry-header">

		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="post_meta">

			<span class="time">
				
				<?php the_time('M d, Y'); ?>
				
			</span> |
			<span class="time">
			Category:<?php siwi_the_taxonomy(array('category','news_category','project_category','publication_category')); ?>
			</span>
			<div class="clear"></div>
			<span class="time tags_wrapper">
				<?php the_tags('Tagged with:',',',''); ?>
			</span>

		</div>

		<?php if ( 'post' == get_post_type() ) : ?>

		<?php endif; ?>

	</header>

	<div class="entry-content">

	<?php the_content(); ?>

	</div>

</article>

<?php endwhile; // end of the loop. ?>
<div class="after_post">

	<div class="publications_download">
	<?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { /* if post has post thumbnail */ ?>
      
      	<?php if(isset($post->post_type) && 'publication' == $post->post_type) : ?>
		<?php $download_link = get_post_meta($post->ID,'_download_link', true); 
		if(!empty($download_link)): ?>
		<a target="_blank" href="<?php echo $download_link; ?>" class="download_file">		
       		<?php the_post_thumbnail('featured-single'); ?>     
       	</a>
       <?php endif; ?>
		<?php endif; ?>   
            
    <?php }
    else { ?>
	<?php if(isset($post->post_type) && 'publication' == $post->post_type) : ?>
		<?php $download_link = get_post_meta($post->ID,'_download_link', true); 
		if(!empty($download_link)): ?>
		<a target="_blank" href="<?php echo $download_link; ?>" class="download_file">	
    		<img src="<?php bloginfo('template_directory'); ?>/images/no_image.jpg" class="no_image"/>
    	</a>
    	  <?php endif; ?>
		<?php endif; ?>   

    <?php } ?>
    <div class="clear"></div>
		<?php if(isset($post->post_type) && 'publication' == $post->post_type) : ?>
		<?php $download_link = get_post_meta($post->ID,'_download_link', true); 
					if(!empty($download_link)): ?>
		<a target="_blank" href="<?php echo $download_link; ?>" class="download_file">Download file</a>
		<?php endif; ?>
		<?php endif; ?>
	</div>

		<div class="share_post">
		<!-- Lockerz Share BEGIN -->
		<div class="a2a_kit a2a_default_style">
		<span class="share_this">Share this page</span>

		<div class="sharez">
			<a class="icons facebook a2a_button_facebook"></a>
			<a class="icons email a2a_button_email"></a>
			<a class="icons twitter a2a_button_twitter"></a>
			<a class="icons share a2a_dd" href="http://www.addtoany.com/share_save"></a>
		</div>

		</div>
		<script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>
		<!-- Lockerz Share END -->
		</div>

</div>