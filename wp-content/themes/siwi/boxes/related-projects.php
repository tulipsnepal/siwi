<?php
global $post;
$related_publication = get_field('related_projects',$post->ID);
if (!empty($related_publication)): ?>
<!-- publication block-->
<div class="resources_area fullwidth fleft relatedblock publicationBlock">
	<div class="row">
		<div class="fullwidth fleft pullme">
			<div class="titles col-xs-12">
				<h3 class="title_lined boldTitle">related projects</h3>
				<!-- <div class="title_line"></div> -->
			</div>
			<?php foreach ($related_publication as $key => $item):

					$project_type = get_the_terms($item->ID,'project_category');
					
					$resourcetype = '';

					foreach ($project_type as $key => $item1) {
						$keyname = $key>0 ? ', ': '';
						$itemname =  $item1->name;					
						$resourcetype .= $keyname.$itemname;
					}

					$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');
	              	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
					$image = aq_resize( $img_url, 210, 126 , true,true,true); 
  					$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';	

			?>
			<div id="" class="widget widget_sp_image one_third borderwhite col-xs-12 col-sm-12 col-md-4 news_repeater">
				<a class="tab_link" href="<?php echo esc_url( post_permalink($item->ID) ); ?>">
					<figure>
					<?php echo $imageTag; ?>
					</figure>
					<aside class="withimage box_inside fullwidth fleft">
						<div class="caption fullwidth fleft">
							<span class="resource-type"><?php echo $resourcetype; ?> </span>
							<span class="report-date"><?php echo date('M d, Y',strtotime($item->post_date)); ?></span>
						</div>
						<h5><?php echo $item->post_title; ?></h5>
					</aside>
				</a>
			</div>
			<?php endforeach ?>
		</div>
	</div>
</div>
<?php endif ?>