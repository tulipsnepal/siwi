<?php
global $post;
$interact_repeater = get_field( 'interact_repeater' );
?>
<div class="resources_area interact">
	<div class="row">
	<div class="pullme fullwidth fleft">
			<div class="titles col-xs-12">
				<h3 class="title_lined boldTitle"><?php echo get_field('interact_headline',$post->ID) ?></h3>
			</div>
			<div class="fullwidth fleft">
			<?php foreach ($interact_repeater as $key => $item) {

				$img_url = $item['image'];

	              	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
					$image = aq_resize( $img_url, 330, 179 , true,true,true); 
  					$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

				 	echo '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">';
					echo '<a target="'.$item['target'].'" href="'.get_proper_url( $item['link'] ).'">
				                  <figure>
				                  '.$imageTag.'
				                  </figure>
				                  <aside class="withimage caption fullwidth fleft">

				                    <h5>'.$item['headline'].'</h5>

				                  </aside>
				                </a>';

				echo '</div>';
			 } ?>

			 <div id="custom_twitter_widget-3" class="widget custom_twitter_widget one_third posts_archive col-md-4 col-sm-6 col-xs-12 librarylist">
			<a class="twitter-timeline" href="https://twitter.com/siwi_water" data-widget-id="626630409711783936">Tweets by @siwi_water</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			</div>

		</div>		
	</div>
	</div>
</div>

