<?php
global $post;
$related_publication = get_field('reated_publication',$post->ID);
if (!empty($related_publication)): ?>
<!-- publication block-->
<div class="resources_area fullwidth fleft relatedblock publicationBlock">
	<div class="row">
		<div class="fullwidth fleft pullme">
			<div class="titles col-xs-12">
				<h3 class="title_lined boldTitle">related publications</h3>
				<!-- <div class="title_line"></div> -->
			</div>
			<?php foreach ($related_publication as $key => $item):
			$resource_type_acf = get_field('resource_type_cf',$item->ID)->name;
			$date_of_report_publication = date('Y',strtotime(get_field('date_of_report_publication',$item->ID)));
			
			$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');
          	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
					
			$image = aq_resize( $img_url, 210, 139 , true,true,true); 
			$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';	

			?>
			<div id="" class="widget widget_sp_image one_third borderwhite col-xs-12 col-sm-12 col-md-4 news_repeater">
				<a class="tab_link" href="<?php echo esc_url( post_permalink($item->ID) ); ?>">
					<figure>
						<?php echo $imageTag; ?>
					</figure>
					<aside class="withimage box_inside fullwidth fleft">
						<div class="caption fullwidth fleft">
							<span class="resource-type"><?php echo $resource_type_acf; ?> </span>
							<span class="report-date"><?php echo $date_of_report_publication; ?></span>
						</div>
						<h5><?php echo $item->short_headline; ?></h5>
					</aside>
				</a>
			</div>
			<?php endforeach ?>
		</div>
	</div>
</div>
<?php endif ?>