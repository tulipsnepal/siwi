<?php
    global $post;
    $slider_items = get_slider_data();

 ?>
<div class="row">
        <section class="slider">
          <div class="option-textlayeroverimage pull-left col-xs-12">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="relativeClass">
              <!-- Indicators -->
              <ol class="carousel-indicators">
              <?php
              $count = 0;
              foreach($slider_items as $item) :
                $active_class = ($count == 0)?'active':'';
                ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $count; ?>" class="<?php echo $active_class;?>"></li>
                <?php
                $count++;
                endforeach;
                ?>

              </ol>
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
<?php
    $count = 0;
    foreach($slider_items as $item) :
        $active_class = ($count == 0)?'active':'';
     ?>
                <div class="item <?php echo $active_class;?>">
                  <div class="workingBlock">
                   <a href="<?php echo esc_url($item->link_href) ?>" target="_blank">
                    <div class="imageLayer">
                      <img src="<?php echo esc_url($item->img_src) ?>" data-src="<?php echo esc_url($item->img_src) ?>" class="img-responsive" alt="">
                    </div>

                    <div class="slider_bottom_text caption">
                    <?php echo $item->post_content ?>
                    </div>
                    </a>
                  </div>
                </div>
              <?php
              $count++;
              endforeach;
              ?>

              </div>
              <!-- Controls -->
              <a class="left carousel-control small" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control small" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>

              </div>
              <div class="textLayer positioned right-top">
                      <div class="textBox">
                        <div class="textHeader"><?php echo $post->post_title; ?></div>
                        <div class="textShortDetails">
                        <?php echo $post->post_content; ?>
                        </div>
                      </div>
                    </div>
            </div>
          </div>
        </section>
      </div>
