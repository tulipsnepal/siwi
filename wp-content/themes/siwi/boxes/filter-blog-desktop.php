<?php
global $post;
$args1 = array(
	'type'                     => 'post',
	'child_of'                 => '',
	'parent'                   => '',
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'category',
	'pad_counts'               => false 

); 
$blog_categories = get_categories($args1);
?>

<div class="sidebar_left">
	
	<div class="widget">
	
			<div id="sub-page-menu" class="widget filterpanel fullwidth fleft">
				<h3 class="area_title panel-title"> </h3>
				<ul class="area_ul list-group fullwidth fleft">					
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/feed' ) ) ; ?>">Subscribe to RSS</a></div>
					</li>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/about' ) ) ; ?>">About SIWI</a></div>
					</li>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/blog-authors' ) ) ; ?>">Blog Authors</a></div>
					</li>
				</ul>
			</div>
		</div>
		<div class="widget">
			<div id="sub-page-menu" class="widget filterpanel fullwidth fleft">
				<h3 class="panel-title">Topics</h3>
				<?php foreach ($blog_categories as $key => $category):
				 ?>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/' ).'bloggedbysiwi/'.$category->slug ) ; ?>"><?php echo $category->name; ?></a></div>
					</li>
				<?php endforeach ?>
			</div>
		</div>

		



</div>



