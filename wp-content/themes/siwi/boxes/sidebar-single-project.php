<div class="filterpanel fullwidth fleft sidebar_left">
			
		<div class="widget">

			<div id="sub-page-menu" class="widget filterpanel fullwidth fleft">
				<h3 class="area_title panel-title"> </h3>
				<ul class="area_ul list-group fullwidth fleft">					
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/project' ) ) ; ?>">Projects</a></div>
					</li>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/news' ) ) ; ?>">News</a></div>
					</li>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/publication' ) ) ; ?>">Publications</a></div>
					</li>
				</ul>
			</div>
		</div>
		
	</div>