<?php
$topic_categories = get_terms('topic_categories');
$regions_tax = get_terms('region_categories');
$language_tax = get_terms('resource_language_categories');
$resource_type_tax = get_terms('resource_type_categories');
$search_query = (isset($_GET['s'])) ? $_GET['s']: the_search_query();
?>
<div class="col-md-3 col-sm-5 col-xs-12 filter-resources pull-right">
	<div class="filterpanel fullwidth fleft">
		<h3 class="panel-title">Filter</h3>
		<div class="list-group" id="accordion">
			<div class="list-group-item">
				<input class="content-toggle" name="filter-focus_area" id="filter-focus_area" value="all" type="checkbox">
				<label for="filter-focus_area" class=" control-label">Topic</label>
				<div class="content-accordion">
					<?php echo generateCheckbox('focus_area',$topic_categories); ?>
				</div>
				<div class="list-group-item">
					<input class="content-toggle" name="filter-region" id="filter-region" type="checkbox">
					<label for="filter-region" class=" control-label">Region</label>
					<div class="content-accordion">
						<?php echo generateCheckbox('region',$regions_tax); ?>
					</div>
				</div>
				<div class="list-group-item">
					<input class="content-toggle" name="filter-language" id="filter-language" type="checkbox">
					<label for="filter-language" class=" control-label">Language</label>
					<div class="content-accordion">
						<?php echo generateCheckbox('language',$language_tax); ?>
					</div>
				</div>
				<div class="list-group-item">
					<input class="content-toggle" name="filter-type" id="filter-type" type="checkbox">
					<label for="filter-type" class=" control-label">Resource type</label>
					<div class="content-accordion">
						<?php echo generateCheckbox('type',$resource_type_tax); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="filterpanel fullwidth fleft">
		<h3 class="panel-title">Search</h3>
		<form class="searchform fullwidth fleft">
			<div class="form-group inputg">
				<input name="s" class="form-control" placeholder="Enter keywords..." id="s" type="text" value="<?php echo $search_query ?>">
				<label for="s">
					&nbsp;
				</label>
			</div>
			<button type="submit" id="searchsubmit" class="btn btn-warning btn-yellow">GO</button>
		</form>
	</div>
	<div class="hidden">
		<?php get_template_part('boxes/sidebar','single-left'); ?>
	</div>
</div>