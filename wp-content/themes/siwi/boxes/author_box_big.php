<div class="author_box widget">
<div class="author_centered fullwidth fleft">
	<!-- <div class="author_title">Blog author</div> -->
	<h3 class="havebreak author_title widget_title">Blog author</h3>
	<div class="havebreak thumbnail"><?php if(function_exists("the_author_image_size")){ the_author_image_size(101, 148, get_the_author_meta('ID')); } ?></div>
	<div class="havebreak author_fullname"><?php the_author_meta('first_name'); echo " "; the_author_meta('last_name'); ?></div>
	<div class="havebreak user_title"><?php the_author_meta('user_title'); ?></div>
	<div class="havebreak author_phone">Phone: <?php the_author_meta('user_phone'); ?></div>
	<div class="havebreak author_email">Email: <a href="mailto:<?php the_author_meta('email'); ?>"><?php the_author_meta('email'); ?></a></div>
	<div class="author_bio"><?php echo nl2br(get_the_author_meta('user_description')); ?></div>
	</div>
</div>