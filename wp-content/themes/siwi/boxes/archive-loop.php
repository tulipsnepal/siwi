<?php
$count = 0;
while (have_posts()) : the_post(); ?>
<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">
	<?php
	if (has_post_thumbnail()) : ?>

	<figure id="blog_banner_<?php $count?>" class="news_thumb">
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	<?php
		the_post_thumbnail('post-img')
		?>
	</a>
	</figure>
	<?php
	endif ?>
	<aside class="withimage">

		<div class="archive_post_meta caption fullwidth fleft">
			<?php siwi_the_taxonomy(array('project_category'));?><span class="time report-date"><?php the_time('M Y.'); ?></span>
		</div>
		<h5 class="archive_title">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h5>
	</aside>

	<p class="hidden">
	<?php echo wp_trim_words( get_the_content(), 20 ); ?>
	<a class="cat_link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">View more</a>
	</p>
	<div class="clear"></div>
	<div class="archive_tags_box hidden">
		<?php the_tags('Tagged with: '); ?>
	</div>
</div>
<?php $count++; endwhile; ?>