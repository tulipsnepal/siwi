<?php
$args = array(
        'post_type' => 'news',
        'posts_per_page'   => 6,
        'post_status'   =>  array('publish'),
        'orderby'        =>'date',
	    'order'     => 'desc'
    );
?>
<div class="news_area news_list fullwidth fleft">
	<div class="titles">
		<h3 class="title_lined boldTitle"><?php echo get_field('news_headline','option') ?></h3>
		<!-- <div class="title_line"></div> -->
	</div>
	<div class="row">
		<?php
		$queryObject = new WP_Query( $args );
		if ($queryObject->have_posts()) { ?>

		<?php
		$x = 0;
		while ($queryObject->have_posts()) {
			$x++;
			$queryObject->the_post();
			if( $x == 3) {
				$style = 'last';
				$x= 0;
			}
			else $style='';
			?>
			<div class="one_third border news_repeater col-xs-6 col-sm-6 col-md-4 <?php echo $style; ?>">
			<a href="<?php the_permalink(); ?>">
				<?php

				$img_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()),'full');

	              	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
					$image = aq_resize( $img_url, 350, 190 , true,true,true); 
  					$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

				// if ( get_the_post_thumbnail($post->ID) != '' ) {
					echo '<figure class="news_thumb">';
					echo $imageTag;
					echo '</figure>';
				/*} else {

					echo '<figure class="news_thumb"><img src="';
					echo siwi_catch_image();
					echo '" alt="" class="img-responsive" /></figure>';*/

				// }
				?>

				<aside class="box_inside withimage">

					<h5>
						<?php the_title(); ?>
					</h5>

					<!-- <aside class="withimage"> -->
						<?php the_excerpt(); ?>
				</aside>
				<div class="tags_box">
					<?php
					$posttags = get_the_tags();
					$count=0;
					if ($posttags)
					{
						foreach($posttags as $tag)
						{
							$count++;
							if ($count <= 3)
							{
								echo ('<a href="'.get_bloginfo('url').'/tag/'.$tag->slug.'">'.$tag->name.'</a>, ');
							}
						}
					}
					?>
				</div>
				<a href="<?php the_permalink(); ?>" title="View post: <?php the_title(); ?>" class="view_post">View</a>
				<!-- </div> -->

				</a>
			</div>
			<?php
		}
		?>
		<?php } ?>
	</div>
</div>
<?php wp_reset_query(); ?>