<div class="sidebar_right">

<?php if(is_single()): ?>

<?php if($post->post_type == 'post'){ get_template_part('boxes/author_box_big'); } ?>
<?php if($post->post_type == 'post'){ get_template_part('boxes/twitter_box'); } ?>

<?php switch($post->post_type){

	case 'publication': dynamic_sidebar( 'publications' ); break;

	case 'project': dynamic_sidebar( 'projects' ); break;

	case 'post': break;

	default: dynamic_sidebar( 'single_post' ); break;

} ?>



<?php endif; ?>



</div>