<?php
$banner_url = siwi_get_option('banner_url');
if($banner_url){
	// echo '<div id="blog_banner"><img src="'.$banner_url.'"></div>';
}
function siwi_blog_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'siwi_blog_excerpt_more');
function siwi_blog_excerpt_length($length) {
	return 45;
}
add_filter('excerpt_length', 'siwi_blog_excerpt_length');
?>
<?php $first = true ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php 

	// $date_of_report_publication = date('Y',strtotime(get_the_date()));
	$date_of_report_publication = get_the_date();
		     $img_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

		      $img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
		      
		      $image = aq_resize( $img_url, 250, 166 , true,true,true); 
		      $imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';	      

		      $news_type = get_the_terms(get_the_ID(),'category');
		        $resourcetype = '';

		        foreach ($news_type as $key => $item1) {
		          $keyname = $key>0 ? ', ': '';
		          $itemname =  $item1->name;          
		          $resourcetype .= $keyname.$itemname;
		        }

		      ?>

		        <div class="news_repeater fleft">
		              <a href="<?php echo the_permalink(); ?>">
		            <figure class="news_thumb">
		           	<?php echo $imageTag ?>
		          </figure>
		          <aside class="withimage">
		            <div class="caption fullwidth fleft">                  
		                   <span class="resource-type"><?php echo get_the_author() ?></span>
		                  <span class="resource-lang"><?php echo $resourcetype; ?></span> 
		                  <span class="report-date"><?php  echo $date_of_report_publication ?></span>
		                  </div>
		              <h2><?php echo the_title(); ?></h2>
		              <?php echo the_excerpt(); ?>
		          </aside>
		          </a>
		          </div>
<?php endwhile	 ?>
