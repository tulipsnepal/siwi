<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content summarybox">
			<?php the_content(); ?>
		</div>
	</article>
	<?php
	endwhile; // end of the loop.
	?>
	<div class="after_post fullwidth fleft">
		<!-- <a href="#" class="download_file">Download file</a> -->
		<div class="share_post">
			<!-- Lockerz Share BEGIN -->
			<div class="a2a_kit a2a_default_style tipsa">
				<span class="share_this share-text">Share article</span>
				<div class="sharez share">
					<a class="icons facebook a2a_button_facebook"></a>
					<a class="icons twitter a2a_button_twitter"></a>
					<a class="icons gplus a2a_button_google_plus"></a>
					<a class="icons linkedin a2a_button_linkedin"></a>
					<!-- <a class="icons share a2a_dd" href="http://www.addtoany.com/share_save"></a> -->
				</div>
			</div>
			<script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>
			<!-- Lockerz Share END -->
		</div>
	</div>