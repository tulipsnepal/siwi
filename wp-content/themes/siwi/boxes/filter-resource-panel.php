<?php
$date_of_report_publication_cf = get_field('date_of_report_publication',$post->ID);
$date_of_report_publication= date('F Y',strtotime($date_of_report_publication_cf));

 ?>

<h3 class="panel-title">Details</h3>

<figure class="featured-image hidden-xs">
	<img src="<?php echo $featured_image; ?>" class="img-responsive" alt="" />
</figure>
<!-- List group -->
<div class="list-group fullwidth fleft">

		<div class="list-group-item hidden-xs">
			<?php if ($resource_pdfs): ?>
					<?php foreach ($resource_pdfs as $key => $item):
						$pdf_button_name = (!empty($item['button_name'])) ? $item['button_name']: 'Download pdf';
					 ?>
						<a class="btn btn-yellow btn-lg btn-block" href="<?php echo $item['pdf_file']; ?>" download><?php echo $pdf_button_name; ?></a>
					<?php endforeach ?>
				<?php endif ?>

				 <?php if ($pdfLink): ?>
					<a class="btn btn-lightblue btn-lg btn-block" href="<?php echo $pdfLink; ?>" target="_blank">View in browser</a>
				 <?php endif ?>

		</div>

	<?php if ($partners_items): ?>
		<div class="list-group-item">
			<strong>Partners</strong>

			<?php foreach ($partners_items as $key => $item) {
					$partner_link = get_field('partner_link', $item->taxonomy . '_' . $item->term_id);
					$partner_link = get_proper_url($partner_link);
					$partner_image = get_field('partner_image', $item->taxonomy . '_' . $item->term_id);
					$logo_width = get_field('logo_width', $item->taxonomy . '_' . $item->term_id);
					$partner_width = (!empty($logo_width)) ? $logo_width: '210px';
				?>

				<div class="media">
				<?php if ($partner_link): ?>
					<a target="_blank" href="<?php echo $partner_link ?>">
						<img width="<?php echo $partner_width ?>" class="media-object" src="<?php echo $partner_image ?>" class="img-responsive" /> </a>
				<?php else: ?>
					<img width="<?php echo $partner_width ?>" class="media-object" src="<?php echo $partner_image ?>" class="img-responsive" />
				<?php endif ?>

				</div>

			<?php } ?>
		</div>
	<?php endif ?>


	<?php if ($part_of_a_program_items): ?>
		<div class="list-group-item">
			<strong>Part of program</strong>
			<p>
				<?php foreach ($part_of_a_program_items as $key => $id) {
						echo $key>0 ? ', ': '';
						echo get_the_title($id);
					} ?>
			</p>
		</div>
	<?php endif ?>


<?php if ($date_of_report_publication): ?>
		<div class="list-group-item">
			<strong>Published</strong>
			<p><?php echo $date_of_report_publication ?></p>
		</div>
	<?php endif ?>

	<?php if ($keywords_items): ?>
		<div class="list-group-item">
			<strong>Keywords</strong>
			<p>
				<?php foreach ($keywords_items as $key => $item) {
					echo $key>0 ? ', ': '';
					$keywordLink = home_url().'/'.$post->post_type.'s/?keyword='.$item->slug;
					echo '<a href="'.$keywordLink.'">'.$item->name.'</a>';
				} ?>

			</p>
		</div>
	<?php endif ?>

	<?php if ($perspective_cf): ?>
		<div class="list-group-item">
			<strong>Perspectives</strong>
			<p><?php echo $perspective_cf; ?></p>
		</div>
	<?php endif ?>

	<?php if ($regions): ?>
		<div class="list-group-item">
			<strong>Regions</strong>
			<p>
			<?php foreach ($regions as $key => $item) {
					echo $key>0 ? ', ': '';
					echo $item->name;
				} ?>
			</p>
		</div>
	<?php endif ?>


	<?php if ($focus_area_items): ?>
		<div class="list-group-item">
			<strong>Topic</strong>
			<p>
				<?php foreach ($focus_area_items as $key => $item) {
						echo $key>0 ? ', ': '';
						echo $item->name;
					} ?>
			</p>
		</div>
	<?php endif ?>


	<?php if ($resource_language_items): ?>
		<div class="list-group-item">
			<strong>Language</strong>
			<p>
				<?php foreach ($resource_language_items as $key => $item) {
					echo $key>0 ? '/ ': '';
					echo $item->name;
				} ?>
			</p>
		</div>
	<?php endif ?>
</div>
