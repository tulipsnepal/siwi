<?php
global $post;
$hashtag = get_post_meta($post->ID, '_siwi_hashtag_tag', true);

if($hashtag){
?>
<div class="widget widget_tweetblender">
<h3><?php echo $hashtag; ?> on Twitter</h2>
<?php
if(function_exists('tweet_blender_widget')){
	tweet_blender_widget(array(
		'unique_div_id' => 'post_hashtag',
		'sources' => $hashtag,
		'refresh_rate'=> 1,
		'tweets_num' => 3,
		'view_more_url' => '',
		'view_more_text' => ''
	));
	}
?>
</div>
<?php } ?>