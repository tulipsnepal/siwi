<div class="sidebar_right">
	<?php if(is_page()) :?>
	<?php
		wp_reset_postdata();
		$siwi_meta = get_post_meta($post->ID,'_siwi_contact_meta',true);

	if(!empty($siwi_meta)) : ?>
	<?php
	$query_args = array(
		'post_type' => 'siwi_contact',
		'posts_per_page' => -1,
		'post__in' => $siwi_meta,
		'orderby' => 'title',
		'order' => 'ASC'
	);

	$contact = new WP_Query($query_args);

	if($contact->have_posts()): ?>
	<div class="widget">
		<ul class="contact_widget">
			<?php while($contact->have_posts()): $contact->the_post();
				$checked = in_array(get_the_ID(), $siwi_meta) ? 'checked': '';
			?>
			<li>
				<h3>Contact</h3>
				<?php
				$meta = get_post_meta(get_the_ID(), '_siwi_meta', true);
				if(has_post_thumbnail()) {
				the_post_thumbnail('contact-img');
				}
				?>
				<div class="clear"></div>
				<h4><?php the_title(); ?></h4>
				<?php if($meta['position']) { ?><span><?php echo 'Position: '.$meta['position']; ?></span><?php } ?>
				<?php if($meta['company']) { ?><span><?php echo 'Company: '.$meta['company']; ?></span><?php } ?>
				<?php if($meta['phone']) { ?><span><?php echo 'Phone: '.$meta['phone']; ?></span><?php } ?>
				<?php if($meta['email']) { ?><span><?php echo 'Email: '.$meta['email']; ?></span><?php } ?>
			</li>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</ul>
	</div>
	<?php endif; ?>
	<?php endif; ?>
	<?php endif; ?>

</div>

