<div class="filter-resources visible-xs">
	<div class="tabbed mobiled fullwidth fleft">
		<ul class="nav nav-tabs">
			<li class=""><a href="#tab12" data-toggle="tab"><h3 class="panel-title">Filter</h3></a></li>
			<li><a href="#tab22" data-toggle="tab"><h3 class="panel-title">Search</h3></a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane" id="tab12">
				<div class="filterpanel fullwidth fleft">
					<!-- <h3 class="panel-title">Filter</h3> -->
					<div class="list-group" id="accordion">
						<div class="list-group-item">
							<input class="content-toggle" name="filter-focus_area" id="filter-focus_area" value="all" type="checkbox">
							<label for="filter-focus_area" class=" control-label">Topic</label>
							<div class="content-accordion">
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="focus_area0" id="focus_area0" data-lbl="278" type="checkbox"><label for="focus_area0">Sanitation and Hygiene</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="focus_area1" id="focus_area1" data-lbl="277" type="checkbox"><label for="focus_area1">Water</label>
								</div>
							</div>
						</div>
						<div class="list-group-item">
							<input class="content-toggle" name="filter-region" id="filter-region" type="checkbox">
							<label for="filter-region" class=" control-label">Region</label>
							<div class="content-accordion">
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="region0" id="region0" data-lbl="196" type="checkbox"><label for="region0">East Asia and the Pacific</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="region1" id="region1" data-lbl="363" type="checkbox"><label for="region1">Eastern Europe</label>
								</div>
							</div>
						</div>
						<div class="list-group-item">
							<input class="content-toggle" name="filter-language" id="filter-language" type="checkbox">
							<label for="filter-language" class=" control-label">Language</label>
							<div class="content-accordion">
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="language0" id="language0" data-lbl="360" type="checkbox"><label for="language0">Arabic</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="language1" id="language1" data-lbl="7" type="checkbox"><label for="language1">English</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="language2" id="language2" data-lbl="359" type="checkbox"><label for="language2">French</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="language3" id="language3" data-lbl="361" type="checkbox"><label for="language3">Portuguese</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="language4" id="language4" data-lbl="72" type="checkbox"><label for="language4">Spanish</label>
								</div>
							</div>
						</div>
						<div class="list-group-item">
							<input class="content-toggle" name="filter-type" id="filter-type" type="checkbox">
							<label for="filter-type" class=" control-label">Resource type</label>
							<div class="content-accordion">
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="type0" id="type0" data-lbl="123" type="checkbox"><label for="type0">Brief</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="type1" id="type1" data-lbl="11" type="checkbox"><label for="type1">Report</label>
								</div>
								<div class="checkbox-inline">
									<input class="filter-chkboxes" name="type2" id="type2" data-lbl="22" type="checkbox"><label for="type2">Tool</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="tab22">
				<div class="filterpanel fullwidth fleft">
					<!-- <h3 class="panel-title">Search</h3> -->
					<form class="searchform fullwidth fleft">
						<div class="form-group inputg">
							<input name="s" class="form-control" placeholder="" id="s" value="" type="text">
							<label for="s">
								Enter keywords...
							</label>
						</div>
						<button type="submit" id="searchsubmit" class="btn btn-warning btn-yellow">GO</button>
					</form>
				</div>
			</div>
		</div>
		<div class="samebox visible-xs selecter">
			<div class="form-group fullwidth fleft">
				<select name="filter-date" id="filter-date" class="selectpicker">
					<option value="desc" selected="selected">Sort by date</option>
					<option value="alphabetical">Sort by alphabet</option>
				</select>
			</div>
		</div>
	</div>
</div>