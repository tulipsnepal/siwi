<?php while ( have_posts() ) : the_post(); ?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>



	<?php
	$banner_url = siwi_get_option('banner_url');
	if($banner_url){
		echo '<div id="blog_banner"><img src="'.$banner_url.'"></div>';
	}
	?>

	<header class="entry-header">



		<h1 class="entry-title"><?php the_title(); ?></h1>



		<div class="post_meta">

			By: <a class="author" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author() ?></a>
			in &ldquo;<span class="category"><?php the_category(','); ?></span>&rdquo;
			on <span class="time"><?php the_date(); ?></span>
			<div class="clearline"></div>
			<span class="time tags_wrapper">

				<?php the_terms(get_the_ID(), 'blog_tag', 'Tagged with: ', ', ', ''); ?>

			</span>



		</div>



		<?php if ( 'post' == get_post_type() ) : ?>



		<?php endif; ?>



	</header>



	<div class="entry-content">



		<?php the_content(); ?>



	</div>
	<div class="clear"></div>
</article>



<?php endwhile; // end of the loop. ?>

<div class="after_post">

	<?php if(isset($post->post_type) && 'publication' == $post->post_type) : ?>

	<?php $download_link = get_post_meta($post->ID,'_download_link', true);

				if(!empty($download_link)): ?>

	<a target="_blank" href="<?php echo $download_link; ?>" class="download_file">Download file</a>

	<?php endif; ?>

	<?php endif; ?>


	<div class="share_post">

	<!-- Lockerz Share BEGIN -->

	<div class="a2a_kit a2a_default_style">

	<span class="share_this">Share this page</span>



	<div class="sharez">

		<a class="icons facebook a2a_button_facebook"></a>

		<a class="icons email a2a_button_email"></a>

		<a class="icons twitter a2a_button_twitter"></a>

		<a class="icons share a2a_dd" href="http://www.addtoany.com/share_save"></a>

	</div>



	</div>

	<script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>

	<!-- Lockerz Share END -->

	</div>

</div>