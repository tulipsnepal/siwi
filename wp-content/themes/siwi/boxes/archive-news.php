<?php

$custom_args = array(
'post_type'     =>  'news',
'post_status'   =>  array('publish'),
'posts_per_page' => get_field('publications_limit','option'),
'orderby' => 'date',
'order' => 'desc'
);
$custom_query = new WP_Query( $custom_args );
$items = $custom_query->posts;

?>

<div class="row news-list">
<div class="col-xs-12">
	<div class="news_list fullwidth fleft">

		<?php foreach ($items as $key => $item){
			$date_of_report_publication = date('d F Y',strtotime(get_field('post_date',$item->ID)));
			echo '<div class="news_repeater fleft">
				<a href="'.esc_url( post_permalink($item->ID) ).'">
						<figure class="news_thumb">
							<img src="'.wp_get_attachment_url(get_post_thumbnail_id($item->ID)).'" class="img-responsive" alt="">
						</figure>
					<aside class="withimage">
					<div class="caption fullwidth fleft">
						<span class="report-date">'.$date_of_report_publication.'</span>
																</div>
						<h2>'.$item->post_title.'</h2>
						'.$item->post_excerpt.'
				</aside>
			</a>

		</div>';
	 } ?>
</div>
<div class="row news-paginate">
	<div class="col-xs-12" id="paginationWrapper">
		<nav class="fleft fullwidth" data-paged="1">
		<?php
		if (function_exists(custom_pagination)) {
			echo custom_pagination($custom_query->max_num_pages,"",$paged);
		}
		 ?>
		 </nav>
	</div>
</div>
<div class="clear"></div>
</div>
</div>

