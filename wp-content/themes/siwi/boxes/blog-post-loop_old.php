<?php
$banner_url = siwi_get_option('banner_url');
if($banner_url){
	echo '<div id="blog_banner"><img src="'.$banner_url.'"></div>';
}
function siwi_blog_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'siwi_blog_excerpt_more');
function siwi_blog_excerpt_length($length) {
	return 45;
}
add_filter('excerpt_length', 'siwi_blog_excerpt_length');
?>
<?php $first = true ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			<div class="post_meta">
				By: <a class="author" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author() ?></a>
				in &ldquo;<span class="category"><?php the_category(','); ?></span>&rdquo;
				on <span class="time"><?php the_date(); ?></span>
			</div>
			<?php if ( 'post' == get_post_type() ) : ?>
			<?php endif; ?>
		</header>
		<div class="entry-content">
			<div class="blog-thumb">
			<figure class="impact_figure">
				<?php the_post_thumbnail('blog-thumbnail') ?>
				</figure>
				<div class="blog_meta">
					<div class="share_post">
						<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style addthis_16x16_style" addthis:url="<?php the_permalink() ?>">
							<a class="addthis_button_facebook"></a>
							<a class="addthis_button_twitter"></a>
							<a class="addthis_button_email"></a>
							<a class="addthis_button_compact"></a>
						</div>
						<!-- AddThis Button END -->
						<?php if($first){ ?>
						<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52fb2c380b911e21"></script>
						<?php
						$first = false;
					}	?>
				</div>
				<p class="readmore"><a href="<?php the_permalink() ?>">Read post</a></p>
			</div>
		</div>
		<?php the_excerpt(); ?>
		<div class="post_meta">
			<span class="time tags_wrapper">
				<?php the_terms(get_the_ID(), 'blog_tag', 'Tagged with: ', ', ', ''); ?>
			</span>
		</div>
		<div class="clear"></div>
	</div>
</article>
<?php endwhile; // end of the loop. ?>