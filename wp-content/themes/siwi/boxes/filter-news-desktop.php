<?php
$args1 = array(
	'type'                     => 'news',
	'child_of'                 => 796,
	'parent'                   => '796',
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'news_category',
	'pad_counts'               => false 

); 
$news_categories = get_categories($args1);
?>
<div class="col-md-3 col-sm-5 col-xs-12 filter-news pull-right">
	<div class="filterpanel fullwidth fleft">
		<h3 class="panel-title">Filter</h3>
		<div class="list-group" id="accordion">
			<div class="list-group-item">
				<input class="content-toggle" name="filter-news_category" id="filter-news_category" value="all" type="checkbox">
				<label for="filter-news_category" class=" control-label">News Type</label>
				<div class="content-accordion">
					<?php echo generateCheckbox('news_category',$news_categories); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="filterpanel fullwidth fleft">
		<h3 class="panel-title">Search</h3>
		<form class="searchform fullwidth fleft">
			<div class="form-group inputg">
				<input name="s" class="form-control" placeholder="Enter keywords..." id="s" type="text" value="<?php echo $search_query ?>">
				<label for="s">
					&nbsp;
				</label>
			</div>
			<button type="submit" id="searchsubmit" class="btn btn-warning btn-yellow">GO</button>
		</form>
	</div>

	<div class="filterpanel fullwidth fleft sidebar_left">
			
		<div class="widget">

			<div id="sub-page-menu" class="widget filterpanel fullwidth fleft">
				<h3 class="area_title panel-title"> </h3>
				<ul class="area_ul list-group fullwidth fleft">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/publication' ) ) ; ?>">Publications</a></div>
					</li>
					<li class="page_item first_level list-group-item">
						<div class="control-label list-group-item"><a href="<?php echo esc_url( home_url( '/project' ) ) ; ?>">Projects</a></div>
					</li>
				</ul>
			</div>
		</div>
		
	</div>
</div>