<div class="author_box widget">
	<div class="thumbnail"><?php if(function_exists("the_author_image_size")){ the_author_image_size(101, 148, get_the_author_meta('ID')); } ?></div>
	<div class="author_title">Blog author</div>
	<div class="author_fullname"><?php the_author_meta('first_name'); echo " "; the_author_meta('last_name'); ?></div>
	<div class="user_title"><?php the_author_meta('user_title'); ?></div>
	<div class="author_phone">Phone: <?php the_author_meta('user_phone'); ?></div>
	<div class="author_email">Email: <a href="mailto:<?php the_author_meta('email'); ?>"><?php the_author_meta('email'); ?></a></div>
</div>