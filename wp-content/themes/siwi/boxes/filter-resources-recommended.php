<?php
$args = array(
        'post_type' => 'publication',
        'posts_per_page'   => 3,
        'post_status'   =>  array('publish'),
        'post__not_in' => array($post->ID),
        'orderby'        =>'rand',
	    'order'     => 'desc'
    );

$recommended_reading = get_posts($args);

if ($recommended_reading): ?>
	<div class="recommended_reading fullwidth fleft">
		<h3 class="boldTitle">Recommended Reading</h3>
		<div class="row resource-library">
				<?php foreach ($recommended_reading as $key => $item) {

						$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

							$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
							$image = aq_resize( $img_url, 250, 166 , true,true,true);	
							$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';
							
					 	 $resource_type_acf = get_field('resource_type_cf',$item->ID)->name;

					      $resource_language_items =  get_field('resource_language_cf',$item->ID);

					      $resource_language_arr = array();
					      foreach ($resource_language_items as $key => $lang_item) {
					        array_push($resource_language_arr, $lang_item->name);
					      }
					      $language_acf = implode('/ ', $resource_language_arr);

					      $date_of_report_publication = date('Y',strtotime(get_field('date_of_report_publication',$item->ID)));

					 	echo '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">';
						echo '<a href="'.esc_url( post_permalink($item->ID) ).'">
					                  <figure>
					                    '.$imageTag.'
					                  </figure>
					                  <aside class="withimage caption fullwidth fleft">

					                    <span class="resource-type">'.$resource_type_acf.'</span>
					                    <span class="resource-lang">'.$language_acf.'</span>
					                    <span class="report-date">'.$date_of_report_publication.'</span>
					                    <h4>'.$item->short_headline.'</h4>

					                  </aside>
					                </a>';

					echo '</div>';
				 } ?>


<?php if (count($recommended_reading)> 0 ): ?>
		<div class="col-xs-12 fleft">
			<h3><a href="<?php echo home_url().'/'.$post->post_type ?>">See all publications <span>&raquo;</span></a></h3>
		</div>
<?php endif ?>
		</div>
	</div>
<?php endif ?>
