<?php
$args = array(
        'post_type' => 'publication',
        'posts_per_page'   => 3,
        'post_status'   =>  array('publish'),
        'orderby'        =>'date',
	    'order'     => 'desc'
    );

$publication_posts = get_posts($args);

if ($publication_posts): ?>
		<div class="resources_area">
			<div class="row">
		<div class="pullme fullwidth fleft">
				<div class="titles col-xs-12">
					<h3 class="title_lined boldTitle"><?php echo get_field('publication_headline','option') ?></h3>
				</div>
				<div class="fullwidth fleft">
				<?php foreach ($publication_posts as $key => $item) {

	              	$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');
	              	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
					$image = aq_resize( $img_url, 330, 166 , true,true,true); 
  					$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

					 	echo '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">';
						echo '<a href="'.esc_url( post_permalink($item->ID) ).'">
					                  <figure>
					                  '.$imageTag.'
					                  </figure>
					                  <aside class="withimage caption fullwidth fleft">

					                    <h5>'.$item->short_headline.'</h5>

					                  </aside>
					                </a>';

					echo '</div>';
				 } ?>
		</div>
		</div>
		</div>
	</div>
<?php endif ?>
