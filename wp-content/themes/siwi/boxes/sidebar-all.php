<?php
$class = "";
global $post;
if($post->post_type == 'post') $class = " blog_left";
?>
<div id="sub-page-menu" class="widget filterpanel fullwidth fleft sidebar_left<?php echo $class; ?>">
	
	<?php if($post->post_type == "post") {
		
		dynamic_sidebar( 'blog-left' );
	}elseif($post->post_type = "news" && !is_archive('news') && !taxonomy_exists('news_category')) { ?>
	<div class="widget">
		<h3 class="area_title  panel-title"><span>News</span></h3>
		<?php
		$args = array(
			'taxonomy'     => 'news_category',
			'orderby'      => 'name',
			'show_count'   => 0,
			'pad_counts'   => 0,
			'hierarchical' => 1,
			'title_li'     => '',
			'hide_empty'   => 0,
			'parent' => 796
			);
			?>
			<ul class="area_ul list-group fullwidth fleft">
				<li><a href="<?php echo get_bloginfo('url').'/news';?>">All News</a><li>
					<?php wp_list_categories( $args ); ?>
				</ul>
			</div>
			<?php }elseif($post->post_type = "project" && !is_archive('news') && !taxonomy_exists('news_category')) { ?>
			<div class="widget">
				<h3 class="area_title  panel-title"><span>Project</span></h3>
				<?php
				$exclude = isset($category->term_id) ? $category->term_id : 0;
				$args = array(
					'taxonomy'     => 'project_category',
					'orderby'      => 'name',
					'show_count'   => 0,
					'pad_counts'   => 0,
					'hierarchical' => 1,
					'title_li'     => '',
					'hide_empty'   => 0,
					'exclude' => $exclude
					);
					?>
					<ul class="area_ul list-group fullwidth fleft">
						<?php wp_list_categories( $args ); ?>
					</ul>
				</div>
				<?php }elseif($post->post_type = "publication" && !is_archive('news') && !taxonomy_exists('news_category')) { ?>
				<div class="widget">
					<h3 class="area_title  panel-title"><span>Publications</span></h3>
					<?php
					$exclude = isset($category->term_id) ? $category->term_id : 0;
					$args = array(
						'taxonomy'     => 'publication_category',
						'orderby'      => 'name',
						'show_count'   => 0,
						'pad_counts'   => 0,
						'hierarchical' => 1,
						'title_li'     => '',
						'hide_empty'   => 0,
						'exclude' => $exclude
						);
						?>
						<ul class="area_ul list-group fullwidth fleft">
							<?php wp_list_categories( $args ); ?>
						</ul>
					</div>
					<?php } if(is_archive() && $post->post_type != "post") { ?>

					<div class="widget">
						<h3 class="area_title  panel-title"><span>Archive</span></h3>

						<ul class="area_ul list-group fullwidth fleft">
							<li><a href="<?php bloginfo('url'); ?>/news" title="Check out News Archive">News</a></li>
							<li><a href="<?php bloginfo('url'); ?>/publication" title="Check out Publications Archive">Publications</a></li>
							<li><a href="<?php bloginfo('url'); ?>/project" title="Check out Project Archive">Projects</a></li>
						</ul>
					</div>
					<?php } ?>
				</div>