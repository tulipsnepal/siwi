<?php
global $post;
if (have_rows('page_content_block') ):
	echo '<div class="summarybox fullwidth fleft">';
		while ( have_rows('page_content_block') ) : the_row();
		
		if (get_row_layout() == 'summary_block'):
			$summary_content = get_sub_field('summary_content');
						if ($summary_content) {
				echo '<div class="content_from_editor fullwidth fleft">
							'.$summary_content.'
						</div>';
			}
		endif;
		if (get_row_layout() == 'quote_block'):
			$quote_by = get_sub_field('quote_by');
			$quote_content = get_sub_field('quote_content');
			if ($quote_content) {
				echo '<div class="quoteblock fullwidth fleft">
									<blockquote>
											'.$quote_content.'
									</blockquote>
									<div class="quotedby">
											'.$quote_by.'
									</div>
							</div>';
			}
		endif;
		if (get_row_layout() == 'contact_block'):?>
		<div class="memberList fullwidth fleft">
		<?php // check if the nested repeater field has rows of data
			if( have_rows('contact_repeater') ):
				// loop through the rows of data
			while ( have_rows('contact_repeater') ) : the_row();
					$image = get_sub_field('image');
					$contact_text = get_sub_field('contact_text');
					$year = get_sub_field('year');
					$link_text = get_sub_field('link_text');
					$link = get_sub_field('link');
					if ($year)
						$year_span = $year;
					if ($contact_text)
						$contact_text_span =$contact_text;
					if ($link_text && $link)
						$link_span = '<a href="'.$link.'">'.$link_text.'</a>';
					echo '<div class="contactlist fullwidth fleft">
									<figure>
											<img src="'.$image.'" class="img-responsive" alt="" />
									</figure>
									<aside class="withimage">
										<span class="swp-year">'.$year_span.'</span>
										<span class="swp-name">'.$contact_text_span.'</span>
										<span class="swp-more">'.$link_span.'</span>
									</aside>
					</div>';
				endwhile;
			endif;?>
			</div>
			<?php
		endif;
		if (get_row_layout() == 'additional_content_block'):
			$additional_content = get_sub_field('additional_content');
			if ($additional_content) {
				echo '<div class="content_from_editor fullwidth fleft">
							'.$additional_content.'
						</div>';
			}
		endif;

	if (get_row_layout() == 'press_release' ): ?>
	<div id="pressRelease" class="documentSection pdf_format one_third fullwidth fleft">
		<h5><?php echo the_sub_field('press_release_title'); ?></h5>
		<?php
		// check if the nested repeater field has rows of data
		if( have_rows('press_release_pdf') ):
					echo '<ul>';
						// loop through the rows of data
					while ( have_rows('press_release_pdf') ) : the_row();
							$pdf_title = get_sub_field('pdf_title');
							$pdf_file = get_sub_field('pdf_file');
							echo '<li><a target="_blank" href="'.$pdf_file.'">'.$pdf_title.'</a></li>';
						endwhile;
					echo '</ul>';
				endif;
		?>
	</div>
	<?php endif ?>
	<?php if (get_row_layout() == 'discover_work' ): ?>
	<div id="discoverWork" class="documentSection discoverWork one_third fullwidth fleft">
		<h5><?php echo the_sub_field('discover_work_title'); ?></h5>
		<?php
		// check if the nested repeater field has rows of data
		if( have_rows('discover_work_pdf') ):
					echo '<ul>';
						// loop through the rows of data
					while ( have_rows('discover_work_pdf') ) : the_row();
							$pdf_title = get_sub_field('pdf_title');
							$pdf_file = get_sub_field('pdf_file');
							echo '<li><a target="_blank" href="'.$pdf_file.'">'.$pdf_title.'</a></li>';
						endwhile;
					echo '</ul>';
				endif;
		?>
	</div>
	<?php endif ?>
	<?php if (get_row_layout() == 'discover_photos' ): ?>
	<div id="discoverPhotos" class="documentSection discoverPhotos one_third fullwidth fleft">
		<h5><?php echo the_sub_field('discover_photos_title'); ?></h5>
		<div class="thumbmailWrapper fullwidth fleft">
			<div class="row">
				<?php
				// check if the nested repeater field has rows of data
				if( have_rows('discover_photos_images') ):
							// loop through the rows of data
						while ( have_rows('discover_photos_images') ) : the_row();
								$img_url = get_sub_field('image');
									$image = aq_resize( $img_url, 213, 170 , true);	

								echo '<div class="col-xs-12 col-sm-12 col-md-4">
										<a class="tab_link" target="_blank" href="'.$img_url.'">
										<figure>
												<img src="'.$image.'" class="img-responsive" alt="" />
										</figure>
										</a>
								</div>';
							endwhile;
						// echo '</ul>';
					endif;
				?>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php if (get_row_layout() == 'discover_videos' ): ?>
	<div id="discoverVideos" class="documentSection discoverVideos one_third fullwidth fleft">
		<h5><?php echo the_sub_field('discover_videos_title'); ?></h5>
		<div class="thumbmailWrapper fullwidth fleft">
			<div class="row">
				<?php
				// check if the nested repeater field has rows of data
				if( have_rows('high_resolution_video') ):
							// loop through the rows of data
						while ( have_rows('high_resolution_video') ) : the_row();
								
								$video_title = get_sub_field('video_title');
								$video_link = get_sub_field('video_link');
								$embedVideo = str_replace('watch?v=', 'embed/', $video_link['url']);

								echo '<div class="col-xs-12 col-md-4 vdo">
								<a target="_blank" class="tab_link" href="'.$video_link['url'].'">
								<iframe type="text/html" 
											    width="640" 
											    height="385" 
											    src="'.$embedVideo.'"
											    frameborder="0">
											</iframe>
											<aside class="withimage caption fullwidth fleft">
												'.$video_title.'
											</aside>
								</a>
								</div>';
							endwhile;
					endif;
				?>
				<?php
				// check if the nested repeater field has rows of data
				if( have_rows('high_resolution_images') ):
								// loop through the rows of data
							while ( have_rows('high_resolution_images') ) : the_row();

									$image_content = get_sub_field('image_content');
									$img_url = get_sub_field('image');
									
					              	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
											
									$image = aq_resize( $img_url, 213, 170 , true,true,true); 
				  					$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

									echo '<div class="col-xs-12 col-sm-6 col-md-4 hdimages">
													<a target="_blank" class="tab_link" href="'.$img_url.'">
															<figure>
																	<img src="'.$image.'" class="img-responsive" alt="">
															</figure>
															<aside class="withimage caption fullwidth fleft">
																	'.$image_content.'
															</aside>
													</a>
											</div>';
								endwhile;
						endif;
				?>
			</div>
		</div>
	</div>
	<?php endif ?>
	<?php  endwhile;
	
echo '</div>';
endif
?>