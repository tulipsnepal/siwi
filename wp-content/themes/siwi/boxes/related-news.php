<?php
global $post;
$related_news = get_field('related_news',$post->ID);
$news_post = get_post(11426);
?>
<?php if (!empty($related_news)): ?>
	<div class="resources_area fullwidth fleft relatedblock newsBlock">
		<div class="row">
			<div class="fullwidth fleft pullme">
				<div class="titles col-xs-12">
					<h3 class="title_lined boldTitle"><?php echo get_field('related_title',$news_post->ID); ?></h3>
					<!-- <div class="title_line"></div> -->
					<p><?php echo get_field('related_content',$news_post->ID); ?></p>
				</div>

				<?php foreach ($related_news as $key => $item):
					$news_type = get_the_terms($item->ID,'news_category');
					
					$resourcetype = '';

					foreach ($news_type as $key => $item1) {
						$keyname = $key>0 ? ', ': '';
						$itemname =  $item1->name;					
						$resourcetype .= $keyname.$itemname;
					}

					$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');
	              	$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
					$image = aq_resize( $img_url, 132, 126 , true,true,true); 
  					$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';	

				 ?>
				<div id="" class="widget widget_sp_image one_third borderwhite col-xs-12 news_repeater">
					<a class="tab_link" href="<?php echo esc_url( post_permalink($item->ID) ); ?>">
						<figure>
							<?php echo $imageTag; ?>
						</figure>
						<aside class="withimage box_inside">
							<div class="caption fullwidth fleft">
								<span class="resource-type">
									<?php echo $resourcetype; ?>
								</span>
								<span class="report-date"><?php echo date('M d, Y',strtotime($item->post_date)); ?></span>
							</div>
							<h5><?php echo $item->post_title; ?></h5>
							<?php if (!empty($item->post_excerpt)): ?>
								<p><?php echo $item->post_excerpt; ?></p>
							<?php else: ?>
								<p><?php echo wp_trim_words($item->post_content,30); ?></p>
							<?php endif ?>
						</aside>
					</a>
				</div>
				<?php endforeach ?>
				
				
			</div>

		</div>
	</div>
<?php endif ?>