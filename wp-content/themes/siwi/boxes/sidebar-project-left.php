<div class="sidebar_left">

	<div class="widget">
		<h3 class="area_title">Publications</h3>
		<?php
		$exclude = isset($category->term_id) ? $category->term_id : 0;
		$args = array(
		  'taxonomy'     => 'project_category',
		  'orderby'      => 'name',
		  'show_count'   => 0,
		  'pad_counts'   => 0,
		  'hierarchical' => 1,
		  'title_li'     => '',
		  'hide_empty'   => 0,
		  'exclude' => $exclude
		);
		?>
		<ul class="area_ul">
			<?php wp_list_categories( $args ); ?>
		</ul>
	</div>	

</div>