<?php
/**
 * Template Name: Blog Template
 *
 * @package SIWI
 */
?>
<?php
get_header();
$post = get_post(8013);
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$custom_args = array(
	'posts_per_page' => get_field('news_limit','option'),
	'paged' => $paged,
	'post_type'     =>  'post',
	'post_status'   =>  array('publish'),
	// 'orderby' => 'date',
	'order' => 'desc',
	);

 $temp = $wp_query; 
 $wp_query = null; 
 $wp_query = new WP_Query($custom_args); 

$wp_query->query($custom_args); 

 // $wp_query->set('posts_per_page', 1);
 $wp_query->query($wp_query->query_vars);

 // echo "<pre>";print_r($wp_query);exit;
 $search_results = $wp_query->posts;
 $count_x_results = $wp_query->found_posts;
 $post_count = $wp_query->post_count;
?>
<main id="mainblock" role="main" class="news-page newslistpage content-wrapper">
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="h1">
				<?php echo $post->post_title; ?>
			</h1>
		</div>
	</div>
	<div class="row relativebox">
		<div class="col-md-9 col-sm-7 col-xs-12">
			<div class="row">
				<div class="post-content col-sm-12 col-xs-12 col-md-11">
						<div class="preamble"><?php echo $post->post_content; ?></div>
					</div>
			</div>
		</div>		

	</div>
	<div class="row news-list">

	<?php if (!md_is_mobile()):
			get_template_part('boxes/filter','blog-desktop');
			endif ?>
		<div class="col-md-9 col-sm-7 col-xs-12">

		<?php if (md_is_mobile()):
				get_template_part('boxes/filter','blog-mobile');
				endif ?>
			<div class="blog-library">
				<?php 
					foreach ($search_results as $item) {
					      $date_of_report_publication = date('M d, Y',strtotime($item->post_date));

					     $img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

					      $img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
					      
					      $image = aq_resize( $img_url, 250, 166 , true,true,true); 
					      $imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';
					      
					        $news_type = get_the_terms($item->ID,'category');
					        $resourcetype = '';

					        foreach ($news_type as $key => $item1) {
					          $keyname = $key>0 ? ', ': '';
					          $itemname =  $item1->name;          
					          $resourcetype .= $keyname.$itemname;
					        }

					        $author_id=$item->post_author;

					        echo '<div class="news_repeater fleft">
					              <a href="'.esc_url( post_permalink($item->ID) ).'">
					            <figure class="news_thumb">
					              '.$imageTag.'
					          </figure>
					          <aside class="withimage">
					            <div class="caption fullwidth fleft">                  
					                   <span class="resource-type">'.get_the_author_meta('display_name',$author_id).'</span>
					                  <span class="resource-lang">'.$resourcetype.'</span> 
					                  <span class="report-date">'.$date_of_report_publication.'</span>
					                  </div>
					              <h2>'.$item->post_title.'</h2>
					              '.wp_trim_words($item->post_content,45).'
					          </aside>
					          </a>
					          </div>';      
					    }
				 ?>
			</div>
		</div>
	</div>
	<?php // Reset postdata
wp_reset_postdata(); ?>
	<div class="row blog-paginate">
		<div class="col-xs-12" id="paginationWrapper">
			<nav class="fleft fullwidth" data-paged="1">
				<?php
				if (function_exists(custom_pagination)) {
					echo custom_pagination($wp_query->max_num_pages,"",$paged);
				}
				?>
			</nav>
		</div>
	</div>
</div>
</main>
<?php
// Reset main query object
 $wp_query = null; 
  $wp_query = $temp;  // Reset


 ?>
<?php get_footer(); ?>