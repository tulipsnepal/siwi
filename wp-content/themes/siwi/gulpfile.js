var gulp = require('gulp'),
    less = require('gulp-less'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    //jshint = require('gulp-jshint'),
    //imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    // plumber = require('gulp-plumber'),
    mcss = require('gulp'),
    minifyCSS = require('gulp-minify-css');


gulp.task('jsmin', function() {
    gulp.src(['libs/jquery/dist/jquery.min.js', 'libs/bootstrap/dist/js/bootstrap.min.js', 'js/jquery.sidr.min.js', 'libs/bootstrap-select/dist/js/bootstrap-select.min.js', 'js/custom.js', 'js/site.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./assets/js/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'))
});

gulp.task('js', function() {
    gulp.src(['libs/jquery/dist/jquery.min.js', 'libs/bootstrap/dist/js/bootstrap.min.js', 'js/jquery.sidr.min.js', 'libs/bootstrap-select/dist/js/bootstrap-select.min.js', 'js/custom.js', 'js/site.js'])
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./assets/js/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./assets/js/'))
});

var paths = {
    css: ['css/*.css', 'css/less/**/*.*'],
    images: 'client/img/**/*'
};

gulp.task('less', function() {

    gulp.src('./css/less/bootstrap.less')
        // .pipe(plumber())
        .pipe(less())
        //.pipe(rename())
        .pipe(rename("core.css"))
        .pipe(gulp.dest('./css/build/'));

});

gulp.task('styles', function() {
    //gulp.src(['./css/fonts.css', './css/bootstrap.min.css', './css/jquery.sidr.dark.css', './css/build/core.css', './css/custom.css'])
    gulp.src(['./css/fonts.css', './css/build/core.css', './css/jquery.sidr.dark.css', 'libs/bootstrap-select/dist/css/bootstrap-select.css', './css/custom.css'])
        //gulp.src(['./css/fonts.css', './css/build/core.css', './css/jquery.sidr.dark.css', './css/custom.css'])
        // .pipe(plumber())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./assets/css/'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./assets/css/'))
});

//to copy all fonts files from src to dest
gulp.task('copyfont', function() {
    gulp.src(['./fonts/**/*.*'])
        .pipe(gulp.dest('./assets/fonts/'))
});

//to copy all fonts files from src to dest
gulp.task('ci', function() {
    gulp.src(['./images/**/*.*'])
        .pipe(gulp.dest('./assets/images/'))
});

gulp.task('dist', ['jsmin', 'styles', 'less', 'copyfont'], function() {

    // Watch .js files
    gulp.watch(['libs/jquery/dist/jquery.min.js', 'libs/bootstrap/dist/js/bootstrap.min.js', 'js/jquery.sidr.min.js', 'libs/bootstrap-select/dist/js/bootstrap-select.min.js', './js/custom.js', 'js/site.js'], ['jsmin']);

    //watch css file
    gulp.watch(['./css/less/*', './css/fonts.css', './css/core.css', './css/jquery.sidr.dark.css', 'libs/bootstrap-select/dist/css/bootstrap-select.css', './css/custom.css'], ['styles']);
});

gulp.task('default', ['js', 'styles', 'less', 'copyfont'], function() {

    // Watch .js files
    gulp.watch(['libs/jquery/dist/jquery.min.js', 'libs/bootstrap/dist/js/bootstrap.min.js', 'js/jquery.sidr.min.js', 'libs/bootstrap-select/dist/js/bootstrap-select.min.js', './js/custom.js', 'js/site.js'], ['js']);

    //watch css file
    gulp.watch(['./css/less/*', './css/fonts.css', './css/core.css', './css/jquery.sidr.dark.css', 'libs/bootstrap-select/dist/css/bootstrap-select.css', './css/custom.css'], ['styles']);
});