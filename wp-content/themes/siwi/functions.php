<?php

if(is_admin()){
	add_action( 'do_meta_boxes', 'my_do_meta_boxes' );

	function my_do_meta_boxes( $post_type ) {
		
		global $wp_meta_boxes;

		if (in_array($post_type, array('publication','news','project'))) {
			$dimension = ' 350w x 190h';
		}elseif(in_array($post_type, array('page'))){
			$dimension = ' 750w x 424h';			
		}elseif(in_array($post_type, array('slider'))){
			$dimension = ' 1150w x 417h';			
		}else{
			$dimension = '';
		}
		if ( ! current_theme_supports( 'post-thumbnails', $post_type ) || ! post_type_supports( $post_type, 'thumbnail' ) )
		return;

		foreach ( $wp_meta_boxes[ $post_type ] as $context => $priorities )
		foreach ( $priorities as $priority => $meta_boxes )
		foreach ( $meta_boxes as $id => $box )
		if ( 'postimagediv' == $id )
		$wp_meta_boxes[ $post_type ][ $context ][ $priority ][ $id ]['title'] .= $dimension;

		remove_action( 'do_meta_boxes', 'my_do_meta_boxes' );
	}
}


// require_once( 'meta-functions.php' );

function siwi_image_thumb_url( $post_id ) {
    $thumbSmall = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'post-thumbnail' );
    return '<img width="350px" height="191px" alt="" class="img-responsive" src="'.$thumbSmall['0'].'" />';
}

function get_root_parent($page_id) {
global $wpdb;
	$parent = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE post_type='page' AND ID = '$page_id'");
	if ($parent == 0) return $page_id;
	else return get_root_parent($parent);
}

class Custom_Walker_Nav_Menu extends Walker_Nav_Menu {

	var $number = 1;

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    	global $post;
    	 // echo "<pre>";print_r($post);exit;
    	$root_parent = get_root_parent($post->ID);

    	$children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));

        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        if ( $depth == 0 ) { 
        	array_push($classes,'firstlevel');        	
        }

         if ( $depth == 1 ) { 
        	array_push($classes,'secondlevel');        	
        }
        if ( $depth == 2 ) { 
        	array_push($classes,'thirdlevel');        	
        }
        if ( $depth == 3 ) { 
        	array_push($classes,'fourthlevel');        	
        }
        if ( $depth == 4) { 
        	array_push($classes,'fifthlevel');        	
        }

        if ($item->object_id==$root_parent) { //sub page
        	array_push($classes,'current-menu-item');
    	} else if($item->xfn==$post->post_type && !isset($_GET['s'])){ //publication/news
        	array_push($classes,'current-menu-item');
    	} else if($item->xfn==$root_parent){ //publication/news
        	array_push($classes,'current-menu-item');
        }


        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = $class_names ? ' class="dropdown ' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names .'>';

         // add span with number here
         if ( $depth == 0 ) { // remove if statement if depth check is not required
        	 // $output .= '<span>'.$item->xfn.'</span>';
            $this->number++;
        }

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;
        
        $haschildren = get_pages('child_of='.$item->ID);
       

        $item_output .= '<span class="dropdownspan"><a'. $attributes .'>';
        
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

        if (empty($children)) {
	        $item_output .= '</a></span>';
        }else{
	        $item_output .= '<a href="javascript:void(0);" class="dropdown-toggle">&nbsp;</a></span>';
        }
        
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

require_once('cmb-functions.php');
require_once( 'libs/Mobile_Detect.php' );
require_once('libs/aq_resizer.php');

function md_is_mobile() {

    $detect = new Mobile_Detect;

    if( $detect->isMobile() && !$detect->isTablet() )
      return true;
    else
      return false;
  }

/**
 * define ajax url to use in js file
 */
add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() {
?>
<script type="text/javascript">
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
<?php
}

/* SET THEME CONSTANTS */
define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('THEME_NAME', 'SIWI');
define('THEME_SLUG', 'siwi');
define('THEME_VERSION', '1.0.6');
define('THEME_OPTIONS','siwi_settings');
/* SET FOLDER CONSTANTS */
define('JS_URI', THEME_URI. '/js');
define('CSS_URI', THEME_URI. '/css');
define('MINI_URI', THEME_URI. '/assets/');
/* LOAD SCRIPTS AND STYLES */

if(!is_admin()){
	add_action('wp_print_styles', 'siwi_load_styles');
	add_action('wp_print_scripts', 'siwi_load_scripts');
}
function siwi_load_styles() {
	wp_register_style('style', THEME_URI.'/style.css', false, THEME_VERSION, 'screen');
	wp_register_style('layout', CSS_URI.'/layout.css', false, THEME_VERSION, 'screen');
	wp_register_style('responsive', CSS_URI.'/responsive.css', false, THEME_VERSION, 'screen');

	wp_register_style('fontello', CSS_URI.'/fontello.css', false, THEME_VERSION, 'screen');
// wp_register_style( 'droidsans', 'http://fonts.googleapis.com/css?family=Droid+Sans:400,700', false, THEME_VERSION, 'screen' );
	wp_register_style('minifiedcss', MINI_URI.'css/main.min.css', false, THEME_VERSION, 'all');
		// wp_enqueue_style('style');
		// wp_enqueue_style('layout');
		// wp_enqueue_style('responsive');

		// wp_enqueue_style('fontello');
//       wp_enqueue_style('droidsans');
	wp_enqueue_style('minifiedcss');
}
function siwi_load_scripts() {
	wp_register_script( 'slider', JS_URI. '/slides.min.jquery.js', array('jquery'), THEME_VERSION );
	// wp_register_script( 'custom', JS_URI. '/custom.js', array('jquery'), THEME_VERSION );
	wp_register_script( 'selectnav', JS_URI. '/selectnav.min.js', array('jquery'), THEME_VERSION );
	wp_register_script( 'minifiedjs', MINI_URI. 'js/main.min.js' );
			// wp_enqueue_script( 'slider' );
			// wp_enqueue_script( 'custom' );
			// wp_enqueue_script( 'selectnav' );
	// wp_enqueue_script( 'minifiedjs' );
}

/* SUPPORT */
// Add support for WP 2.9 post thumbnails and custom image sizes
if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'featured-single', 455, '' , true ); // single post image
	add_image_size( 'post-thumbnail', 350, 191 , true ); //boxes and widget images
	add_image_size( 'slider-img', 1150, 417, true ); //slider image size
	add_image_size( 'post-img', 448, ''); //custom image size
	add_image_size( 'contact-img', 100, 150); //custom image size
	add_image_size( 'blog-thumbnail', 187, '', true); //custom image size
	add_image_size( 'blog-banner', 456, 101, true); //custom image size

}
add_filter('image_size_names_choose',  'siwi_image_size_labels');
function siwi_image_size_labels($sizes) {
	$sizes['post-img'] = __( 'Post Image');
	return $sizes;
}
//Show all post types on archive templates
function any_ptype_on_tag($request) {
	if(!is_admin()){
		if ( isset($request['tag']) )
			$request['post_type'] = 'any';
	}
	return $request;
}
add_filter('request', 'any_ptype_on_tag');
function any_ptype_on_cate($request) {
	if(!is_admin()){
		if ( isset($request['cat']) )
			$request['post_type'] = 'any';

		if ( isset($request['year']) )
			$request['post_type'] = 'any';
	}
	return $request;
}
add_filter('request', 'any_ptype_on_cate');
// Change Excerpt Length
function siwi_excerpt_length($length) {
	return 30; }
	add_filter('excerpt_length', 'siwi_excerpt_length');
// Change Excerpt [...] to new string : WP2.8+
	function siwi_excerpt_more($excerpt) {
		return str_replace('[...]', '...', $excerpt); }
		add_filter('wp_trim_excerpt', 'siwi_excerpt_more');
		/* LOAD SETTINGS */
		$siwi_options = get_option(THEME_OPTIONS);
// TAGS CLOUD WIDGET MODIFICATION
		function widget_custom_tag_cloud($args) {

// Control number of tags to be displayed - 0 no tags
			$args['number'] = 20;

// Tag font unit px, pt, em
			$args['unit'] = 'px';

// Maximum tag text size
			$args['largest'] = 28;

// Minimum tag text size
			$args['smallest'] = 10;

// Outputs our edited widget
			return $args;
		}
		add_filter( 'widget_tag_cloud_args', 'widget_custom_tag_cloud' );
		/* ARCHIVES WIDGET SHOW ALL POST TYPES */
/*
add_filter( 'getarchives_where' , 'ucc_getarchives_where_filter' , 10 , 2 );
function ucc_getarchives_where_filter( $where , $r ) {

$args = array( 'public' => true , '_builtin' => false ); $output = 'names'; $operator = 'and';
$post_types = get_post_types( $args , $output , $operator ); $post_types = array_merge( $post_types , array( 'post' ) ); $post_types = "'" . implode( "' , '" , $post_types ) . "'";
return str_replace( "post_type = 'post'" , "post_type IN ( $post_types )" , $where );
}
*/
function my_custom_post_type_archive_where($where,$args){
	$post_type  = isset($args['post_type'])  ? $args['post_type']  : 'post';
	$where = "WHERE post_type = '$post_type' AND post_status = 'publish'";
	return $where;
}
add_filter( 'getarchives_where','my_custom_post_type_archive_where',10,2);
/* REGISTER TAGS FOR PAGES */
add_action('init', 'siwi_register_tags_for_pages');
function siwi_register_tags_for_pages(){
	register_taxonomy_for_object_type('post_tag','page');
}
/* NEW TAGS TAXONOMY FOR POSTS */
add_action('init', 'siwi_new_post_tags');
function siwi_new_post_tags(){
	//if(function_exists("unregister_taxonomy_for_object_type") unregister_taxonomy_for_object_type('post_tag', 'post');
	register_taxonomy( 'blog_tag', 'post', array( 'hierarchical' => false, 'label' => __('Blog Tags'), 'query_var' => 'blog_tag' ) );
}
/* INCLUDES */
require_once( 'include/helpers.php' );
require_once( 'include/sidebars.php' );
require_once( 'include/menus.php' );
require_once( 'include/types.php' );
require_once( 'include/widgets.php' );
require_once( 'include/metaboxes.php' );
require_once( 'include/options.php' );
require_once( 'include/shortcodes.php' );
require_once( 'ajax-search.php' );
/* FETCHING SCRIPT */

function left_sidebar_menu(){
	global $post;
	echo '<div id="sub-page-menu" class="widget filterpanel fullwidth fleft">';
	$cv = false;
	if($post->post_type == 'page' ){
		$rootPost = $post;
	}
	$oldRoot = $rootPost;
	$page_array = array();
	$GLOBALS['menu-level'] = 0;
	$GLOBALS['rootPost'] = $rootPost;

	if(!is_search()){
		while(true)
		{
			$pages = get_pages(array('child_of' => $rootPost->ID, 'parent' => $rootPost->ID, 'sort_column' => 'menu_order', 'sort_order' => 'ASC'));
			foreach($pages as $page){
				$page_item['id'] = $page->ID;
				$page_item['level'] = $GLOBALS['menu-level'];
				$page_item['parent'] = $page->post_parent;
				if($page->ID == $oldRoot->ID && $GLOBALS['menu-level'] > 1){
					$page_item['ancestor'] = 1;
				} else {
					$page_item['ancestor'] = 0;
				}
				$page_array[] = $page_item;
			}
			if($rootPost->post_parent == 0){
				$title = $rootPost;
			}
			$oldRoot = $rootPost;
			if($rootPost->post_parent == 0) break;
			$rootPost = get_page($rootPost->post_parent);
			$GLOBALS['menu-level']++;
		}

		echo '<h3 class="area_title panel-title"><a href="'.get_permalink($title->ID).'">'.$title->post_title.'</a></h3>
		<ul class="area_ul list-group fullwidth fleft">';
		}

		foreach($page_array as $page_item){
			if($page_item['level'] == $GLOBALS['menu-level']){
				render_menu($page_item, $page_array, $GLOBALS['menu-level']);
			}
		}

		echo '</ul></div>';
	}

	function post_have_children(){
		global $post;
		$id = $post->ID;

		if ($post->post_parent>0) {			
			return true;
		}

		$children = get_pages('child_of='.$id);
			if(count($children) == 0){
				return false;
			}
			else{
				return true;
			}
	}

	
	function render_menu($page_item, $page_array, $level){
		global $post;
		$pageID = 15;
		$class = 'page_item';
		$page = get_post($page_item['id']);
		$children = get_pages('child_of='.$page_item['id'].'&parent='.$page_item['id']);
		if(count($children) > 0){
			$class .= ' has_children';
		}
		if($page_item['ancestor']) $class .= ' current_page_ancestor';
		if($page_item['id'] == $GLOBALS['rootPost']->ID) $class .= ' current_page_item';
		if($page_item['level'] == $GLOBALS['menu-level']) $class .= ' first_level';

		echo '<li class="'.$class.' list-group-item"><div class="control-label list-group-item"><a href="'.get_permalink($page->ID).'">'.$page->post_title.'</a></div>';
		if($page_item['ancestor'] == 1 || (count($children) > 0 && $page_item['id'] == $post->ID)){
			echo '<ul class="children">';
			foreach($page_array as $new_page_item){
				if($new_page_item['level'] == $level-1){
					render_menu($new_page_item, $page_array, $new_page_item['level']);
				}
			}
			echo '</ul>';
		}
		echo '</li>';
	}
	/* ADD METABOXES AND FUNCTIONS FOR PAGE */
	add_action( 'load-post.php', 'mile_metabox_setup');
	add_action( 'load-post-new.php', 'mile_metabox_setup' );
	function mile_metabox_setup() {
		global $typenow;
		if($typenow == 'page'){
			add_action( 'add_meta_boxes', 'mile_create_sidebars_metabox');
			add_action( 'save_post', 'mile_save_sidebars_metabox', 10, 2 );
		}
	}
	function mile_create_sidebars_metabox(){
		add_meta_box(
					'mile_sidebars',			// Unique ID
				__( 'Sidebar'),		// Title
				'mile_draw_sidebars_metabox',		// Callback function
							'page',					// Admin page (or post type)
							'normal',					// Context
							'default'					// Priority
							);
	}
	function mile_draw_sidebars_metabox( $object, $box ) {
		$sidebars_nonce = wp_nonce_field( basename( __FILE__ ), 'sidebars_nonce' );
		$sidebars_custom = get_post_meta($object->ID,'_sidebars_custom',true);

		global $wp_registered_sidebars;

		if(!empty($wp_registered_sidebars)){
			echo '<select name="sidebars_custom">';
			echo '<option value="0" selected>No Sidebar</option>';
			foreach($wp_registered_sidebars as $sidebar){
				echo '<option value="'.$sidebar['id'].'" '.selected( $sidebar['id'], $sidebars_custom, false ).'>'.$sidebar['name'].'</option>';
			}
			echo '</select>';
		} else {
			echo '<p>'._('No registered sidebars.').'</p>';
		}

	}
	function mile_save_sidebars_metabox( $post_id, $post ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		if ( !isset( $_POST['sidebars_nonce'] ) || !wp_verify_nonce( $_POST['sidebars_nonce'], basename( __FILE__ ) ) )
			return $post_id;

		if($post->post_type == 'page'){

			$post_type = get_post_type_object( $post->post_type );

			if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
				return $post_id;

			$sidebars_custom = isset($_POST['sidebars_custom']) ? $_POST['sidebars_custom'] : '';
			update_post_meta( $post_id, '_sidebars_custom', $sidebars_custom );
		}
	}
	/* ADD RSS SUPPORT FOR CUSTOM POST TYPES */
	function myfeed_request($qv) {
		if (isset($qv['feed']) && !isset($qv['post_type']))
			$qv['post_type'] = get_post_types($args = array(
				'public'   => true,
				'_builtin' => false
				));
		if(isset($qv['post_type']) && is_array($qv['post_type'])){
			array_push($qv['post_type'],'post');
		}
		return $qv;
	}
	add_filter('request', 'myfeed_request');
	function siwi_filter_where($where = ''){

		if(is_post_type_archive('publication') && isset($_POST['pub_s']) && !empty($_POST['pub_s'])){
			$pub_s = esc_attr($_POST['pub_s']);
			$where .= "AND (post_title LIKE '%$pub_s%' OR post_content LIKE '%$pub_s%')";
		}
		return $where;
	}
	add_filter('posts_where', 'siwi_filter_where');
	add_filter('pre_get_posts', 'siwi_pre_get_posts');
	function siwi_pre_get_posts($query){

		/* Dissalow stockholm-water-front-articles to appear in publications search results */
		if(is_post_type_archive('publication') && $query->is_main_query() && isset($_POST['pub_s'])){
			$tax_query = array(
				array(
					'taxonomy' => 'publication_category',
					'terms' => array('stockholm-water-front-articles'),
					'field' => 'slug',
					'operator' => 'NOT IN'
					)
				);

			$query->set('tax_query', $tax_query);
		}
		if((is_home() && $query->get('page_id') != 0) || is_author()){
			$query->set('posts_per_page', 4);
		}
	}
// Add root parent ID to body class
	add_filter('body_class','top_level_parent_id_body_class');
	function top_level_parent_id_body_class($classes) {
		global $wpdb, $post;
		if (is_page()) {
			if ($post->post_parent)	{
				$ancestors=get_post_ancestors($post->ID);
				$root=count($ancestors)-1;
				$parent = $ancestors[$root];
			} else {
				$parent = $post->ID;
			}
			$classes[] = 'root-pageid-' . $parent;
		}
		return $classes;
	}
// Twitter Cards
	add_filter( 'twitter_cards_properties', 'twitter_custom' );

	function twitter_custom( $twitter_card ) {
		if ( is_array( $twitter_card ) ) {
			$twitter_card['creator'] = '@siwi_media';
			$twitter_card['creator:id'] = 120096860;
		}
		return $twitter_card;
	}

/**
 * Custom WordPress Loop With Pagination
 */
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 8;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   *
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'array',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  $html = '';

  if ($paginate_links) {
    $html .='
		<ul class="pagination">';
        foreach ( $paginate_links as $page ) {
                $html .= '<li class="ajax-pagebutton">'.$page.'</li>';
        }
       $html .= '</ul>';
  }

  return $html;

}



/**
 * Returns Array of Term Names for "my_taxonomy"
 */
function getPostTerms($post_id,$taxonomy,$separator=', '){
	$term_list = wp_get_post_terms($post_id, $taxonomy, array("fields" => "names"));
	return implode($separator, $term_list);
}

/**
 * generate checkbox for each search filter
 */
function generateCheckbox($name,$checkbox){
	$html = '';
	foreach ($checkbox as $ck => $cval) {
  	// echo "<pre>";print_r($cval);exit;
		$html.='<div class="checkbox-inline">
		<input class="filter-chkboxes" type="checkbox" name="'.$name.$ck.'" id="'.$name.$ck.'" data-lbl="'.$cval->term_id.'"><label for="'.$name.$ck.'">'.$cval->name.'</label>
	</div> ';
}
return $html;
}


function get_proper_url($url){
	if (false === strpos($url, '://') && !empty($url)) {
		$url = 'http://' . $url;
	}
	return esc_url($url);
}

// in this case we have three ACF Relationship fields set up, they have the names
// acf_rel_1, acf_rel_2, and acf_rel_3 (NOTE: these are the NAMES not the labels)
// ACF stores post IDs in one form or another, so indexing them does us no good,
// we want to index the title of the related post, so we need to extract it

function my_searchwp_custom_fields( $customFieldValue, $customFieldName, $thePost ) {

  // by default we're just going to send the original value back
  $contentToIndex = $customFieldValue;

  // check to see if this is one of the ACF Relationship fields we want to process
  if( in_array( strtolower( $customFieldName ), array( 'short_headline', 'perspective_cf', 'keywords_cf' ,'authors_cf') ) ) {

    // we want to index the titles, not the post IDs, so we'll wipe this out and append our titles to it
    $contentToIndex = '';

    // related posts are stored in an array
    if( is_array( $customFieldValue ) ) {
      foreach( $customFieldValue as $relatedPostData ) {
        if( is_numeric( $relatedPostData ) ) { // if you set the Relationship to store post IDs, it's numeric
          $title = get_the_title( $relatedPostData );
          $contentToIndex .= $title . ' ';
        } else { // it's an array of objects
          $postData = maybe_unserialize( $relatedPostData );
          if( is_array( $postData ) && !empty( $postData ) ) {
            foreach( $postData as $postID ) {
              $title = get_the_title( absint( $postID ) );
              $contentToIndex .= $title . ' ';
            }
          }
        }
      }
    }
  }

  return $contentToIndex;
}

add_filter( 'searchwp_custom_fields', 'my_searchwp_custom_fields', 10, 3 );

function wpse_134409_current_category_class($classes, $item) {
    if (
        is_single()
        && 'category' === $item->object
        && in_array($item->object_id, wp_get_post_categories($GLOBALS['post']->ID))
    )
        $classes[] = 'current-category';

    return $classes;
} // function wpse_134409_current_category_class

add_filter('nav_menu_css_class', 'wpse_134409_current_category_class', 10, 2);