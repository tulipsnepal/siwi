jQuery(document).ready(function($) {

	$('.filter-resources #filter-date').on('change', function() {
		filterResourceLibrary();
	})

	$('.filter-news #filter-date').on('change', function() {
		filterNewsLibrary();
	})

	$('.filter-project #filter-date').on('change', function() {
		filterProjectLibrary();
	})


	$('.filter-resources .filter-chkboxes').on('change', function() {
		filterResourceLibrary();
	});

	$('.filter-news .filter-chkboxes').on('change', function() {
		filterNewsLibrary();
	});

	$('.filter-project .filter-chkboxes').on('change', function() {
		filterProjectLibrary();
	});


	$(".filter-resources #searchsubmit").click(function(e) {
		e.preventDefault();
		filterResourceLibrary();
	});

	$(".filter-news #searchsubmit").click(function(e) {
		e.preventDefault();
		filterNewsLibrary();
	});

	$(".filter-project #searchsubmit").click(function(e) {
		e.preventDefault();
		filterProjectLibrary();
	});

	setPaginationActionsResource();
	setPaginationActionsNews();
	setPaginationActionsProject();

});

function filterResourceLibrary(currentpagenum, pagenum) {

	var searchData = {};

	searchData.date = $('#filter-date option:selected').val();
	searchData.s = $("#s").val();
	searchData.focus_area = [];
	searchData.region = [];
	searchData.language = [];
	searchData.type = [];
	searchData.paged = pagenum || 1;
	searchData.currentpagenum = currentpagenum || 1;

	searchData.keyword = $('#keyword').val();

	$('.filter-resources input[type=checkbox]').each(
		function(index) {
			var input = $(this);
			if (input.is(":checked")) {

				if (input.attr('name').indexOf('focus_area') > -1 && input.attr('data-lbl'))
					searchData.focus_area.push(input.attr('data-lbl'));

				if (input.attr('name').indexOf('region') > -1 && input.attr('data-lbl'))
					searchData.region.push(input.attr('data-lbl'));

				if (input.attr('name').indexOf('language') > -1 && input.attr('data-lbl'))
					searchData.language.push(input.attr('data-lbl'));

				if (input.attr('name').indexOf('type') > -1 && input.attr('data-lbl'))
					searchData.type.push(input.attr('data-lbl'));

			};
		})

	console.log(JSON.stringify(searchData));
	jQuery.ajax({
		type: "post",
		dataType: "json",
		url: ajaxurl,
		data: {
			action: "get_filtered_resources",
			filter: searchData,
		},
		success: function(response) {
			if (response.type == "success") {
				$('.resource-library').html('').html(response.html);
				$('#paginationWrapper').html('').html(response.pagination);

				setPaginationActionsResource();
			} else {
				alert("error occured.")
			}
		}
	})
}

function setPaginationActionsResource() {
	$('.resource-paginate .ajax-pagebutton').click(function(event) {
		event.preventDefault();
		var currentpagenum = parseInt($('.resource-paginate').find('.current').text());
		var pagenum = $(this).find('.page-numbers').text();
		console.log(currentpagenum, pagenum);
		if (pagenum === '«') {
			pagenum = currentpagenum - 1;
		} else if (pagenum === '»') {
			pagenum = currentpagenum + 1;
		}
		pagenum = parseInt(pagenum);
		// console.log(currentpagenum, pagenum);
		filterResourceLibrary(currentpagenum, pagenum);
	});
}

function filterNewsLibrary(currentpagenum, pagenum) {

	var searchData = {};

	searchData.date = $('#filter-date option:selected').val();
	searchData.s = $("#s").val();
	searchData.news_category = [];
	searchData.paged = pagenum || 1;
	searchData.currentpagenum = currentpagenum || 1;

	searchData.keyword = $('#keyword').val();

	$('.filter-news input[type=checkbox]').each(
		function(index) {
			var input = $(this);
			if (input.is(":checked")) {

				if (input.attr('name').indexOf('news_category') > -1 && input.attr('data-lbl'))
					searchData.news_category.push(input.attr('data-lbl'));				

			};
		})

	searchData.keyword = $('#keyword').val();

	console.log(JSON.stringify(searchData));
	jQuery.ajax({
		type: "post",
		dataType: "json",
		url: ajaxurl,
		data: {
			action: "get_filtered_news",
			filter: searchData,
		},
		success: function(response) {
			if (response.type == "success") {
				$('.news-library').html('').html(response.html);
				$('#paginationWrapper').html('').html(response.pagination);

				setPaginationActionsNews();
			} else {
				alert("error occured.")
			}
		}
	})
}

function setPaginationActionsNews() {
	$('.news-paginate .ajax-pagebutton').click(function(event) {
		event.preventDefault();
		var currentpagenum = parseInt($('.news-paginate').find('.current').text());
		var pagenum = $(this).find('.page-numbers').text();
		console.log(currentpagenum, pagenum);
		if (pagenum === '«') {
			pagenum = currentpagenum - 1;
		} else if (pagenum === '»') {
			pagenum = currentpagenum + 1;
		}
		pagenum = parseInt(pagenum);
		// console.log(currentpagenum, pagenum);
		filterNewsLibrary(currentpagenum, pagenum);
	});
}

function filterProjectLibrary(currentpagenum, pagenum) {

	var searchData = {};

	searchData.date = $('#filter-date option:selected').val();
	searchData.s = $("#s").val();
	searchData.project_category = [];
	searchData.paged = pagenum || 1;
	searchData.currentpagenum = currentpagenum || 1;

	searchData.keyword = $('#keyword').val();

	$('.filter-project input[type=checkbox]').each(
		function(index) {
			var input = $(this);
			if (input.is(":checked")) {

				if (input.attr('name').indexOf('project_category') > -1 && input.attr('data-lbl'))
					searchData.project_category.push(input.attr('data-lbl'));				

			};
		})

	searchData.keyword = $('#keyword').val();

	console.log(JSON.stringify(searchData));
	jQuery.ajax({
		type: "post",
		dataType: "json",
		url: ajaxurl,
		data: {
			action: "get_filtered_project",
			filter: searchData,
		},
		success: function(response) {
			if (response.type == "success") {
				$('.project-library').html('').html(response.html);
				$('#paginationWrapper').html('').html(response.pagination);

				setPaginationActionsProject();
			} else {
				alert("error occured.")
			}
		}
	})
}

function setPaginationActionsProject() {
	$('.project-paginate .ajax-pagebutton').click(function(event) {
		event.preventDefault();
		var currentpagenum = parseInt($('.project-paginate').find('.current').text());
		var pagenum = $(this).find('.page-numbers').text();
		console.log(currentpagenum, pagenum);
		if (pagenum === '«') {
			pagenum = currentpagenum - 1;
		} else if (pagenum === '»') {
			pagenum = currentpagenum + 1;
		}
		pagenum = parseInt(pagenum);
		// console.log(currentpagenum, pagenum);
		filterProjectLibrary(currentpagenum, pagenum);
	});
}