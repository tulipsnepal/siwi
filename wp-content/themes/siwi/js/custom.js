var scrolltoheight = 90;
var BrowserDetect = {
    init: function() {
        this.browser = this.searchString(this.dataBrowser) || "Other";
        this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
    },
    inform: function() {
        BrowserDetect.init();
        var browser = BrowserDetect.browser.toLocaleLowerCase();
        if (typeof jQuery == 'function') {
            var classNames = 'is_' + browser + ' ' + browser + '-' + BrowserDetect.version;
            jQuery('body').addClass(classNames);
        }

    },

    searchString: function(data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            this.versionSearchString = data[i].subString;

            if (dataString.indexOf(data[i].subString) != -1) {
                return data[i].identity;
            }
        }
    },

    searchVersion: function(dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },

    dataBrowser: [{
        string: navigator.userAgent,
        subString: "Chrome",
        identity: "Chrome"
    }, {
        string: navigator.userAgent,
        subString: "MSIE",
        identity: "Explorer"
    }, {
        string: navigator.userAgent,
        subString: "Firefox",
        identity: "Firefox"
    }, {
        string: navigator.userAgent,
        subString: "Safari",
        identity: "Safari"
    }, {
        string: navigator.userAgent,
        subString: "Opera",
        identity: "Opera"
    }]

};
BrowserDetect.inform();

/*bootstrap carousel re*/
+ function($) {
    'use strict';

    //this code is for the text only
    var mode = readCookie('mode');
    if (mode === 'image') { // text mode
        $('.textonly-icon').css("background", "#F0EFEE");
        $("#mainblock img,.partnerLogos img").each(function() {
            $(this).attr('src', document.location.origin + '/wgf/wp-content/uploads/2015/07/textonlyimage.png');
        });

    } else { // image mode
        $("#mainblock img,.partnerLogos img").each(function() {
            $(this).attr('src', $(this).attr('data-src'));

        });
    }


    $('.textonly-icon').on('click', function(e) {
        var cookie = readCookie('mode');
        cookie = cookie || 'text';
        if (cookie === 'text') {
            eraseCookie('mode');
            createCookie('mode', 'image', 7);

        } else {

            eraseCookie('mode');
            createCookie('mode', 'text', 7);

        }

        var mode = readCookie('mode');

        e.preventDefault();
        if (mode == "image") {
            $(this).css("background", "#F0EFEE");
            $("#mainblock img,.partnerLogos img").each(function() {
                $(this).attr('src', document.location.origin + '/wgf/wp-content/uploads/2015/07/textonlyimage.png');
            });

        } else {
            location.reload();
        }
    })


    if (!$.fn.carousel) {
        throw new Error("carousel-swipe required bootstrap carousel")
    }

    // CAROUSEL CLASS DEFINITION
    // =========================

    var CarouselSwipe = function(element) {
        this.$element = $(element)
        this.carousel = this.$element.data('bs.carousel')
        this.options = $.extend({}, CarouselSwipe.DEFAULTS, this.carousel.options)
        this.startX =
            this.startY =
            this.startTime =
            this.cycling =
            this.$active =
            this.$items =
            this.$next =
            this.$prev =
            this.dx = null

        this.$element
            .on('touchstart', $.proxy(this.touchstart, this))
            .on('touchmove', $.proxy(this.touchmove, this))
            .on('touchend', $.proxy(this.touchend, this))
    }

    CarouselSwipe.DEFAULTS = {
        swipe: 50 // percent per second
    }

    CarouselSwipe.prototype.touchstart = function(e) {
        if (!this.options.swipe) return;
        var touch = e.originalEvent.touches ? e.originalEvent.touches[0] : e
        this.dx = 0
        this.startX = touch.pageX
        this.startY = touch.pageY
        this.cycling = null
        this.width = this.$element.width()
        this.startTime = e.timeStamp
    }

    CarouselSwipe.prototype.touchmove = function(e) {
        if (!this.options.swipe) return;
        var touch = e.originalEvent.touches ? e.originalEvent.touches[0] : e
        var dx = touch.pageX - this.startX
        var dy = touch.pageY - this.startY
        if (Math.abs(dx) < Math.abs(dy)) return; // vertical scroll

        if (this.cycling === null) {
            this.cycling = !!this.carousel.interval
            this.cycling && this.carousel.pause()
        }

        e.preventDefault()
        this.dx = dx / (this.width || 1) * 100
        this.swipe(this.dx)
    }

    CarouselSwipe.prototype.touchend = function(e) {
        if (!this.options.swipe) return;
        if (!this.$active) return; // nothing moved
        var all = $()
            .add(this.$active).add(this.$prev).add(this.$next)
            .carousel_transition(true)

        var dt = (e.timeStamp - this.startTime) / 1000
        var speed = Math.abs(this.dx / dt) // percent-per-second
        if (this.dx > 40 || (this.dx > 0 && speed > this.options.swipe)) {
            this.carousel.prev()
        } else if (this.dx < -40 || (this.dx < 0 && speed > this.options.swipe)) {
            this.carousel.next();
        } else {
            this.$active
                .one($.support.transition.end, function() {
                    all.removeClass('prev next')
                })
                .emulateTransitionEnd(this.$active.css('transition-duration').slice(0, -1) * 1000)
        }

        all.css('left', '')
        this.cycling && this.carousel.cycle()
        this.$active = null // reset the active element
    }

    CarouselSwipe.prototype.swipe = function(percent) {
        var $active = this.$active || this.getActive()
        if (percent < 0) {
            this.$prev
                .css('left', '')
                .removeClass('prev')
                .carousel_transition(true)
            if (!this.$next.length || this.$next.hasClass('active')) return
            this.$next
                .carousel_transition(false)
                .addClass('next')
                .css('left', (percent + 100) + '%')
        } else {
            this.$next
                .css('left', '')
                .removeClass('next')
                .carousel_transition(true)
            if (!this.$prev.length || this.$prev.hasClass('active')) return
            this.$prev
                .carousel_transition(false)
                .addClass('prev')
                .css('left', (percent - 100) + '%')
        }

        $active
            .carousel_transition(false)
            .css('left', percent + '%')
    }

    CarouselSwipe.prototype.getActive = function() {
        this.$active = this.$element.find('.item.active')
        this.$items = this.$active.parent().children()

        this.$next = this.$active.next()
        if (!this.$next.length && this.options.wrap) {
            this.$next = this.$items.first();
        }

        this.$prev = this.$active.prev()
        if (!this.$prev.length && this.options.wrap) {
            this.$prev = this.$items.last();
        }

        return this.$active;
    }

    // CAROUSEL PLUGIN DEFINITION
    // ==========================

    var old = $.fn.carousel
    $.fn.carousel = function() {
        old.apply(this, arguments);
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('bs.carousel.swipe')
            if (!data) $this.data('bs.carousel.swipe', new CarouselSwipe(this))
        })
    }

    $.extend($.fn.carousel, old);

    $.fn.carousel_transition = function(enable) {
        enable = enable ? '' : 'none';
        return this.each(function() {
            $(this)
                .css('-webkit-transition', enable)
                .css('transition', enable)
        })
    };

}(jQuery);

/*till here*/

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


/*calculating Menu height for scrolling to top*/
function calcHeight() {
    var windowWidth = jQuery(window).width();
    if (windowWidth > 800) {
        scrolltoheight = jQuery('header').height();
    } else {
        scrolltoheight = jQuery("#mobile-header").outerHeight();
    }
}

/*! Copyright 2012, Ben Lin (http://dreamerslab.com/)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Version: 1.0.16
 *
 * Requires: jQuery >= 1.2.3
 */
;
(function($) {
    $.fn.addBack = $.fn.addBack || $.fn.andSelf;

    $.fn.extend({

        actual: function(method, options) {
            // check if the jQuery method exist
            if (!this[method]) {
                throw '$.actual => The jQuery method "' + method + '" you called does not exist';
            }

            var defaults = {
                absolute: false,
                clone: false,
                includeMargin: false
            };

            var configs = $.extend(defaults, options);

            var $target = this.eq(0);
            var fix, restore;

            if (configs.clone === true) {
                fix = function() {
                    var style = 'position: absolute !important; top: -1000 !important; ';

                    // this is useful with css3pie
                    $target = $target.
                    clone().
                    attr('style', style).
                    appendTo('body');
                };

                restore = function() {
                    // remove DOM element after getting the width
                    $target.remove();
                };
            } else {
                var tmp = [];
                var style = '';
                var $hidden;

                fix = function() {
                    // get all hidden parents
                    $hidden = $target.parents().addBack().filter(':hidden');
                    style += 'visibility: hidden !important; display: block !important; ';

                    if (configs.absolute === true) style += 'position: absolute !important; ';

                    // save the origin style props
                    // set the hidden el css to be got the actual value later
                    $hidden.each(function() {
                        // Save original style. If no style was set, attr() returns undefined
                        var $this = $(this);
                        var thisStyle = $this.attr('style');

                        tmp.push(thisStyle);
                        // Retain as much of the original style as possible, if there is one
                        $this.attr('style', thisStyle ? thisStyle + ';' + style : style);
                    });
                };

                restore = function() {
                    // restore origin style values
                    $hidden.each(function(i) {
                        var $this = $(this);
                        var _tmp = tmp[i];

                        if (_tmp === undefined) {
                            $this.removeAttr('style');
                        } else {
                            $this.attr('style', _tmp);
                        }
                    });
                };
            }

            fix();
            // get the actual value with user specific methed
            // it can be 'width', 'height', 'outerWidth', 'innerWidth'... etc
            // configs.includeMargin only works for 'outerWidth' and 'outerHeight'
            var actual = /(outer)/.test(method) ?
                $target[method](configs.includeMargin) :
                $target[method]();

            restore();
            // IMPORTANT, this plugin only return the value of the first element
            return actual;
        }
    });
})(jQuery);


function get_menu_width() {

    jQuery('ul.dropdown-menu.sub-menu').each(function() {
        var total_width1 = 0;
        var total_width = 0;
        jQuery(this).find('.firstlevel').each(function() {
            var current_width = 0;
            var margedl = 0;
            var margedr = 0;
            var padde = 0;
            current_width = jQuery(this).outerWidth();
            margedl = parseInt(jQuery(this).css('margin-left'));
            margedr = parseInt(jQuery(this).css('margin-right'));
            total_width = current_width + margedl + margedr + 2;
            total_width1 += total_width;
            // console.log('margin-left:',margedl,'margin-right:',margedr,total_width1);
            jQuery(this).parent("ul").css('width', total_width1);
        });
        total_width1 = 0;

    });
}

function mobilemenu() {
    jQuery('li.sidr-class-activated').parents('li.sidr-class-firstlevel,li.sidr-class-dropdown').addClass("opened");
    jQuery('a.activated').parents('li.firstlevel,li.dropdown').addClass("opened");
    /*rightmenu*/
    jQuery('.right_menu .activated').parents('.right_menu .secondlevel').addClass("opened");
    jQuery('a#responsive-menu-button').click(function() {
        if (jQuery(this).hasClass('opened')) {
            jQuery(this).removeClass('opened');
        } else {
            jQuery(this).addClass('opened');
        }
    });



    // jQuery(document).on('load','.goog-te-combo',function() {
    //    jQuery('.goog-te-combo').selectpicker();
    //    });


    jQuery(document).on('click', 'a.sidr-class-dropdown-toggle,span.sidr-class-firstleveltext,a.dropdown-toggle,span.firstleveltext', function() {
        if (jQuery(this).parent().parent('li').hasClass('opened')) {
            jQuery(this).parent().parent('li').removeClass('opened');
        } else {
            jQuery(this).parent().parent('li').addClass('opened');
        }
    });

    jQuery('li.menu-item-has-children > span, li.hasnextlevel > span').click(function() {
        if (jQuery(this).parent('li').hasClass('activated')) {
            jQuery(this).parent('li').removeClass('activated');
        } else {
            jQuery(this).parent('li').addClass('activated');
        }
    });
}


jQuery(window).load(function() {
    addMargintobody();
    calculateHeight();
    get_menu_width();
    mobilemenu();
    // calculateHeight();
    //makesecondlineequal();
    calcHeight();
    responsiveImages();
});
// logoCentered();
jQuery(window).resize(function() {
    // logoCentered();
    calculateHeight();
    addMargintobody();
    get_menu_width();
    mobilemenu();
    // calculateHeight();
    //makesecondlineequal();
    calcHeight();
    responsiveImages();
});

jQuery('header img').load(function() {
    // logoCentered();
});
jQuery("a.searchIcon").click(function() {
    if (jQuery("#searchform").hasClass('activated')) {
        jQuery("#searchform").removeClass('activated');
    } else {
        jQuery("#searchform").addClass('activated')
    }
});
jQuery(document).ready(function() {


    //calling function which wil convert label to placeholder
    addRemoveFocusOnInputs();
    //tabbedstructure
    tabbedStructure();

    jQuery("a.scrollTop").click(function() {
        scrollToAnchor('body');
    });


    // jQuery("#myCarousel").carousel({
    //     interval: 0,
    //     pause: false
    // });
    // $(function() {
    jQuery('.carousel').each(function() {
        jQuery(this).carousel({
            interval: 5000,
            pause: 'hover',
            swipe: 30
        });
    });

    //for newsletter subscribe and unsubscribe
    jQuery('.forlabel2').hide();
    jQuery('.label1').click(function() {
        jQuery('.forlabel1').show();
        jQuery('.forlabel2').hide();
        jQuery('.label2').removeClass("checked");

    });
    jQuery('.label2').click(function() {
        jQuery('.forlabel2').show();
        jQuery('.forlabel1').hide();
        jQuery('.label1').removeClass("checked");
    });
});


// function addMargintobody() {
//     var wWidth = jQuery(window).width()
//     var heightHeader = jQuery(".header").innerHeight();
//     jQuery("#mainblock").css("margin-top", heightHeader + "px");
//     }
// }
function logoCentered() {
    /*for making logo centerd*/
    jQuery(".logo").css("height", "auto");
    var LogoBar = jQuery(".fixedrow").height();
    jQuery(".logo").css("height", LogoBar + "px");

    var LogoHeight = jQuery(".logo img").height();
    jQuery(".logo a").css({
        "margin-top": (-LogoHeight / 2) + "px",
        "top": "50%"
    });

}

function addMargintobody() {
    var heightHeader = jQuery(".header").height();
    var heightHeaderWpadmin = heightHeader + jQuery("#wpadminbar").height();
    // jQuery("#mainblock").css("margin-top", heightHeader + "px");
    addTop();

}

/*iframe is detected and wrapped with iframe_section div*/
jQuery('iframe').wrap('<div class="iframe_section">');

/*add Top value to header if there is adminbar*/
function addTop() {
    var hTop = jQuery('#wpadminbar').height();
    // jQuery('header').css('top', hTop + 'px');
    if (jQuery('header').hasClass("change_bg")) {
        jQuery('header nav').css('top', hTop + 'px');
    } else {
        jQuery('header nav').css('top', '0px');
    }
}

function scrollToAnchor(aid, speed) {
    speed = speed || 'slow';
    var aTag = jQuery(aid);

    var top = (aTag.offset().top);
    jQuery('html,body').animate({
        scrollTop: top
    }, speed);
}
/*making first two words wrapped by strong tag*/
// var first_line = $(".preamble").each(function() {
//     $(this).html(function(i, html) {
//         return html.replace(/([0-9a-zA-ZäöåÄÖÅ:]+\s[0-9a-zA-ZäöåÄÖÅ:]+)/, '<strong>$1</strong>')
//     });
// });
/*initiating sider menu*/
jQuery('#responsive-menu-button').sidr({
    name: 'sidr-main',
    source: '#navbar'
});


jQuery("a#responsive-menu-button").click(function() {

})

jQuery(document).ready(function() {
    $('.selectpicker').selectpicker();
    $('input').customInput();
    var heightHeader = jQuery('header .container').height();
    var navHeights = jQuery('header .navbar').height();
    jQuery(window).bind('scroll', function() {
        var navHeight = heightHeader;
        if (jQuery(window).scrollTop() > heightHeader) {
            jQuery('#header.fixed').addClass('change_bg').css("margin-bottom",navHeights +'px');
            addTop();
            // jQuery('#mobile-header,#searchform').addClass('fixed');
        } else {
            jQuery('#header.fixed').removeClass('change_bg').css("margin-bottom",'0px');
            addTop();
            // jQuery('#mobile-header,#searchform').removeClass('fixed');
        }
    }).scroll();

    var heightHeadernotfixed = jQuery('#header .logo').height();
    jQuery(window).bind('scroll', function() {
        var navHeightnotfixed = heightHeadernotfixed;
        if (jQuery(window).scrollTop() > navHeightnotfixed) {
            jQuery('#header,#mobile-header,#searchform').addClass('fixed').parents(".fixed").css("padding-bottom", jQuery('#mobile-header').outerHeight() + "px");
        } else {
            jQuery('#header,#mobile-header,#searchform').removeClass('fixed').parents(".fixed").css("padding-bottom", "0px");
        }
    });

    //tabbed structure
    var tabContainers = jQuery('.belownavigation');
    jQuery('.controlled a').click(function() {
        // tabContainers.hide().removeClass("activated");
        if (tabContainers.hide().filter(this.hash).hasClass("activated")) {

            tabContainers.hide().filter(this.hash).removeClass("activated").fadeOut();
        } else {
            //alert('hello');
            setTimeout(function() {
                $("#s").focus();
            }, 1);

            tabContainers.removeClass("activated").fadeOut();
            tabContainers.hide().filter(this.hash).addClass("activated").fadeIn();
        }
        return false;

    });

    //equalheight
    // calculateHeight();
});

function calculateHeight() {
    /*for carousel*/
    jQuery(".carousel-inner .item").css("height", "auto");
    jQuery(".carousel .carousel-inner").css("height", "auto");
    var height_prev = 0;
    // jQuery(".carousel").each(function(){
    jQuery(".carousel .carousel-inner .item").each(function() {
        var current_height = jQuery(this).outerHeight();
        if (current_height > height_prev) {
            height_prev = current_height;
        } else {
            height_prev = height_prev;
        }
    });
    // jQuery(.carousel .carousel-inner .item").css("height", height_prev);
    jQuery(".carousel .carousel-inner .item").css("height", height_prev);
    /*for carousel*/

    jQuery(".boxrepeater .col-sm-6 .shaded").css("height", "auto");
    jQuery(".boxrepeater").each(function() {
        var rheight_prev = null;
        jQuery(".col-sm-6").each(function() {
            var rcurrent_height = jQuery(this).find('.shaded').height();
            if (rcurrent_height > rheight_prev) {
                rheight_prev = rcurrent_height;
            } else {
                rheight_prev = rheight_prev;
            }
        });
        jQuery(".boxrepeater .col-sm-6 .shaded").css("height", rheight_prev);
    });
    // });

    var cheight_prev = null;
    jQuery(".contakts h2.h3").css("height", "auto");
    jQuery(".contakts").each(function() {
        var ccurrent_height = jQuery(this).find("h2.h3").height();
        if (ccurrent_height > cheight_prev) {
            cheight_prev = ccurrent_height;
        } else {
            cheight_prev = cheight_prev;
        }
    });
    jQuery(".contakts").find("h2.h3").css("height", cheight_prev);
}



/*for using label as the placeholder*/
function addRemoveFocusOnInputs() {
    $('.form-control').each(function() {
        $text = $.trim($(this).val());
        if ($text == "") {
            $(this).removeClass("focused");
        } else {
            $(this).addClass("focused");
        }
    })
}

$(".form-control").blur(function() {
    $text = $.trim($(this).val());
    if ($text == "") {
        $(this).removeClass("focused");
    } else {
        $(this).addClass("focused");
    }
});
$(".form-control").focus(function() {
    $text = $.trim($(this).val());
    if ($text == "") {
        $(this).removeClass("focused");
    } else {
        $(this).addClass("focused");
    }
});


/*customiszed form elements with background iamge*/
jQuery.fn.customInput = function() {
    $('.filter-resources,.filter-news').each(function(i) {
        if ($(this).is('[type=checkbox],[type=radio]')) {
            var input = $(this);

            // get the associated label using the input's id
            var label = $('label[for=' + input.attr('id') + ']');
            console.log(label)

            //get type, for classname suffix
            var inputType = (input.is('[type=checkbox]')) ? 'checkbox' : 'radio';

            // wrap the input + label in a div
            $('').insertBefore(input).append(input, label);

            // find all inputs in this set using the shared name attribute
            var allInputs = $('input[name=' + input.attr('name') + ']');

            // necessary for browsers that don't support the :hover pseudo class on labels
            label.hover(
                function() {
                    $(this).addClass('hover');
                    if (inputType == 'checkbox' && input.is(':checked')) {
                        $(this).addClass('checkedHover');
                    }
                },
                function() {
                    $(this).removeClass('hover checkedHover');
                }
            );

            //bind custom event, trigger it, bind click,focus,blur events
            input.bind('updateState', function() {
                    if (input.is(':checked')) {
                        if (input.is(':radio')) {
                            allInputs.each(function() {
                                $('label[for=' + $(this).attr('id') + ']').removeClass('checked');
                            });
                        };
                        label.addClass('checked');
                    } else {
                        label.removeClass('checked checkedHover checkedFocus');
                    }

                })
                .trigger('updateState')
                .click(function() {
                    $(this).trigger('updateState');
                })
                .focus(function() {
                    label.addClass('focus');
                    if (inputType == 'checkbox' && input.is(':checked')) {
                        $(this).addClass('checkedFocus');
                    }
                })
                .blur(function() {
                    label.removeClass('focus checkedFocus');
                });
        }
    });
};


//map script
jQuery(document).ready(function($) {
    (function($) {

        /*
         *  render_map
         *
         *  This function will render a Google Map onto the selected jQuery element
         *
         *  @type    function
         *  @date    8/11/2013
         *  @since   4.3.0
         *
         *  @param   $el (jQuery element)
         *  @return  n/a
         */

        function render_map($el) {

            // var
            var $markers = $el.find('.marker');

            // vars
            var args = {
                zoom: 5,
                mapTypeControl: false,
                center: new google.maps.LatLng(57.701955, 11.982260),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // create map
            var map = new google.maps.Map($el[0], args);

            // add a markers reference
            map.markers = [];

            // add markers
            $markers.each(function() {
                if ($(this).attr('data-lat') != '' && $(this).attr('data-lng') != '')
                    add_marker($(this), map);
            });

            // center map
            center_map(map);

        }

        /*
         *  add_marker
         *
         *  This function will add a marker to the selected Google Map
         *
         *  @type    function
         *  @date    8/11/2013
         *  @since   4.3.0
         *
         *  @param   $marker (jQuery element)
         *  @param   map (Google Map object)
         *  @return  n/a
         */
        var infowindows = [];

        function closeAllInfoWindows() {
            for (var i = 0; i < infowindows.length; i++) {
                infowindows[i].close();
            }
        }

        function add_marker($marker, map) {

            // var
            var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
            var mapiconsrc = $marker.attr('data-map');

            // create marker
            var marker = new google.maps.Marker({
                position: latlng,
                icon: mapiconsrc,
                map: map
            });

            // add to array
            map.markers.push(marker);

            // if marker contains HTML, add it to an infoWindow
            if ($marker.html()) {
                // create info window
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });
                infowindows.push(infowindow);
                // show info window when marker is clicked
                google.maps.event.addListener(marker, 'click', function() {

                    closeAllInfoWindows();

                    infowindow.open(map, marker);

                });
            }

        }

        /*
         *  center_map
         *
         *  This function will center the map, showing all markers attached to this map
         *
         *  @type    function
         *  @date    8/11/2013
         *  @since   4.3.0
         *
         *  @param   map (Google Map object)
         *  @return  n/a
         */

        function center_map(map) {

            // vars
            var bounds = new google.maps.LatLngBounds();

            // loop through all markers and create bounds
            $.each(map.markers, function(i, marker) {

                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

                bounds.extend(latlng);

            });

            // only 1 marker?
            if (map.markers.length == 1) {
                // set center of map
                map.setCenter(bounds.getCenter());
                map.setZoom(5);
            } else {
                // fit to bounds
                map.fitBounds(bounds);
                //map.setCenter( bounds.getCenter() );
                //map.setZoom( 5 );
            }

        }

    })(jQuery);
});

/* for homepage two column menu showhide */
$('#menutoggler1').click(function() {
    //colcount = $(this).attr('data-colcount');
    if ($('.hidden-menu-1').is(":visible")) {
        $('.hidden-menu-1').removeClass('contentFadeIn');
        $('.hidden-menu-1').addClass('contentFadeOut').delay(400).slideUp();
    } else {
        $('.hidden-menu-1').slideDown(600, function() {
            $('.hidden-menu-1').removeClass('contentFadeOut');
            $('.hidden-menu-1').addClass('contentFadeIn');
        });
    }
});
$('#menutoggler0').click(function() {
    //colcount = $(this).attr('data-colcount');
    if ($('.hidden-menu-0').is(":visible")) {
        $('.hidden-menu-0').removeClass('contentFadeIn');
        $('.hidden-menu-0').addClass('contentFadeOut').delay(400).slideUp();
    } else {
        $('.hidden-menu-0').slideDown(600, function() {
            $('.hidden-menu-0').removeClass('contentFadeOut');
            $('.hidden-menu-0').addClass('contentFadeIn');
        });
    }
});

function responsiveImages() {
    var width = $(window).width();
    $('img.customresponsive').each(function() {
        var small = $(this).attr('data-small');
        var med = $(this).attr('data-medium');
        var large = $(this).attr('data-large');
        if (width <= 480) {
            $(this).attr('src', small);
        } else if (width <= 767) {
            $(this).attr('src', med);
        } else {
            $(this).attr('src', large);
        }
    });
}

function tabbedStructure() {
    var tabContainers = $('.tab-pane');

    $('ul.nav-tabs li a').click(function() {
        tabContainers.hide().filter(this.hash).show();
        $('ul.nav-tabs li').removeClass('active');
        $(this).parent().addClass('active');
        return false;
    }).filter('[href=#tab1]').click();
}

/* FOR INTERACTIVE MAP */
$('.map-region-content').hide();
$('.map-region').click(function() {
    $('.map-region').attr('class', 'map-region');
    $('.map-region-content').hide();

    $(this).attr('class', 'map-region active-region');

    $('#' + $(this).attr('id') + '_Contents').show();
    // scrollToAnchor('.regional_title');
});
$('a.closeThis').click(function() {
    $(this).parents('.map-region-content').hide();
    $(".map-region").attr('class', '');
});


var $ = jQuery.noConflict();

jQuery(document).ready(function($) {



    // selectnav('menu-topmenu', {

    //   label: 'Tap to choose',

    //   nested: true,

    //   indent: '-'

    // });



    // selectnav('menu-footermenu', {

    //   label: 'Tap to choose',

    //   nested: true,

    //   indent: '-'

    // });



    // SLIDER HOME PAGE

    // if($('#slides').length > 0){

    // $("#slides").slides({

    //     generateNextPrev: true,

    //     pagination: true,

    //     play: 7000

    // });

    // }



    // MAIN MENU

    // $('ul.main-menu > li > a').hover(function(){

    //     var self = $(this);



    //     $('.main-menu li a').removeClass('active');

    //     self.addClass('active');



    //     $('.main-menu li ul.sub-menu').hide();

    //     self.next().show();



    // },

    // function () {

    //     $('.main-menu li a').removeClass('active');

    //     self.next().hide();



    // });

    //OUTSIDE CLICK

    // $(document).mouseup(function (e){

    //     var container = $(".main-menu li ul.sub-menu");



    //     if (container.has(e.target).length === 0){

    //         container.hide();

    //         $('.main-menu li a').removeClass('active');

    //     }

    // });



    // TABS

    // $('ul.tabs_nav').find('li a').click(function(e){

    //     e.preventDefault();

    //     var self = $(this);

    //     var self_attr = $(this).attr('data-rel');

    //     $('.tab_content div').removeClass('active');

    //     $('.tab_content div[data-rel='+self_attr+']').addClass('active');

    //     $('ul.tabs_nav').find('li a').parent().removeClass('active');

    //     self.parent().addClass('active');

    // });
});