var siwi_shortcode;
var siwi_shortcode_content;
(function() {
    tinymce.create('tinymce.plugins.siwi_shortcodes', {
        init : function(ed, url) {
            ed.addButton('siwi_shortcodes_button', {
                title : 'SIWI Shortcodes',
                image : '../wp-content/themes/siwi/images/shortcodes.png',
                onclick : function() {
                    
                    siwi_shortcode = ed.selection;
                    siwi_shortcode_content = ed.selection.getContent();
                    
										var width = jQuery(window).width(), H = jQuery(window).height(), W = ( 720 < width ) ? 720 : width;
                    	W = W - 80;
                    	H = H - 84;
										
										/*
										var shortcodes_loaded = jQuery("#siwi_shortcodes_holder").length;
										
										if (shortcodes_loaded){
										
												tb_show( 'SIWI Shortcodes', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=siwi_shortcodes' );
										
										} else {
												
												jQuery("#content_toolbargroup").append('<div id="siwi_shortcodes_holder" style="display: none;"><div id="siwi_shortcodes"></div></div>');
												
												jQuery.get('admin-ajax.php?action=siwi_generate_shortcodes_ui', function(data) {
  													jQuery('#siwi_shortcodes').html(data);
  													tb_show( 'SIWI Shortcodes', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=siwi_shortcodes' );
												});
										}
										*/
										
                    tb_show( 'SIWI Shortcodes', 'admin-ajax.php?action=siwi_generate_shortcodes_ui&width=' + W + '&height=' + H);
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('siwi_shortcodes', tinymce.plugins.siwi_shortcodes);
})();