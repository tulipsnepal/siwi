<?php get_header(); ?>
<main id="mainblock" role="main" class="resource single-page main-page content-wrapper">
	<div class="container">
		<div class="row resource-list relativebox">
	<?php
	$class = $post->post_type == 'post' ? ' blog' : '';
	?>
	<div class="content_holder<?php echo $class; ?>">
		<div class="col-md-4 col-sm-5 col-xs-12 pull-right">
		<?php
		$class = "";
		switch($post->post_type){
			case 'post': get_template_part('boxes/sidebar','blog-left'); $class = " main_blog"; break;
			default: get_template_part('boxes/sidebar','single-left'); break;
		} ?>
		</div>
		<div class="col-md-8 col-sm-7 col-xs-12">
		<div class="main_single <?php echo $class; ?>">
			
			<?php switch($post->post_type){
				case 'post': get_template_part('boxes/single-post','loop'); break;
				default: get_template_part('boxes/single','loop'); break;
			} ?>
			<?php  ?>
			
		</div>
		</div>
	</div>
	<div class="col-xs-12">
	<?php get_template_part('boxes/sidebar','single-right'); ?>
	</div>
</div>

</div>
</main>
<?php get_footer(); ?>