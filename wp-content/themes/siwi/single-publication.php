<?php get_header();
$post = get_post();
// $resource_type_item = $acf['resource_type_cf']['value']->name;
$resource_type_item = get_field('resource_type_cf',$post->ID)->name;
// $resource_language_items = $acf['resource_language_cf']['value'];
$resource_language_items = get_field('resource_language_cf',$post->ID);
$authors = getPostTerms($post->ID,'author_categories');
$date_of_report_publication_cf = get_field('date_of_report_publication',$post->ID);
$date_of_report_publication= date('F Y',strtotime($date_of_report_publication_cf));
// $publisher_items = $acf['publisher_cf']['value'];
$publisher_items = get_field('publisher_cf',$post->ID);
// $resource_pdfs = $acf['resource_pdf']['value'];
$resource_pdfs = get_field('resource_pdf',$post->ID);
// $lessons_learned = $acf['lessons_learned']['value'];
$lessons_learned = get_field('lessons_learned',$post->ID);
// $summary = $acf['summary']['value'];
$summary = get_field('summary',$post->ID);
// $citation = $acf['citation']['value'];
$citation = get_field('citation',$post->ID);
$featured_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
// $pdfLinkField =  $acf['link_to_online_pdf_reader']['value'];
$pdfLinkField = get_field('link_to_online_pdf_reader',$post->ID);
$pdfLink = get_proper_url($pdfLinkField);
// $partners_items = $acf['partners_cf']['value'];
$partners_items = get_field('partners_cf',$post->ID);
// $part_of_a_program_items = $acf['part_of_a_program']['value'];
// $part_of_a_program_items = get_post_meta($post->ID,'siwi_demo_metabox',true);
$part_of_a_program_items = get_post_meta($post->ID,'_siwi_demo_multicheckbox',true);
// echo "<pre>";print_r($part_of_a_program_items);exit;
// $keywords_items = $acf['keywords_cf']['value'];
$keywords_items = get_field('keywords_cf',$post->ID);
// $perspective_cf = $acf['perspective_cf']['value']->name;
$perspective_cf = get_field('perspective_cf',$post->ID)->name;
// $regions = $acf['regions']['value'];
$regions = get_field('regions',$post->ID);
// $focus_area_items = $acf['topic_cf']['value'];
$focus_area_items = get_field('topic_cf',$post->ID);
?>
<main id="mainblock" role="main" class="single mobiledesign resource resource-single-page content-wrapper">
<div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<?php if ($post->post_title): ?>
			<h1 class="h1">
			<?php echo $post->post_title; ?>
			</h1>
			<?php endif ?>
			<div class="quickinfo caption">
				<?php if ($resource_type_item): ?>
				<span class="label label-warning resource-type"><?php echo $resource_type_item; ?></span>
				<?php endif ?>
				<?php if ($resource_language_items): ?>
				<span class="resource-lang">
				<?php foreach ($resource_language_items as $key => $item) {
					echo $key>0 ? '/ ': '';
					echo $item->name;
				} ?>
				</span>
				<?php endif ?>
				<?php if ($authors): ?>
				<span class="resource-author">Authors: <?php echo $authors; ?></span>
				<br />
				<?php endif ?>
				<?php if ($date_of_report_publication): ?>
				<span class="report-date">Published: <?php echo $date_of_report_publication; ?></span>
				<?php endif ?>
				<?php if ($publisher_items): ?>
				<span class="resource-publisher">Publisher:
				<?php foreach ($publisher_items as $key => $item) {
					echo $key>0 ? ', ': '';
					echo $item->name;
				} ?>
				</span>
				<?php endif ?>
			</div>
			<div class="withsomegap">
				<figure class="featured-image">
					<img src="<?php echo $featured_image; ?>" class="img-responsive" alt="" />
				</figure>
				<?php if ($resource_pdfs): ?>
				<?php foreach ($resource_pdfs as $key => $item):
					$pdf_button_name = (!empty($item['button_name'])) ? $item['button_name']: 'Download pdf';
				?>
				<div class="list-group-item">
					<a class="btn btn-yellow btn-lg" href="<?php echo $item['pdf_file']; ?>" download><?php echo $pdf_button_name; ?></a>
				</div>
				<?php endforeach ?>
				<?php endif ?>
			</div>
			<?php if ($post->post_content): ?>
			<article class="preamble fullwidth fleft">
				<?php echo $post->post_content; ?>
			</article>
			<?php endif ?>
			<?php if ($lessons_learned): ?>
			<artilcle class="jumbotron fullwidth fleft" id="jumbo">
				<h2>Key Lessons learned</h2>
				<?php echo $lessons_learned; ?>
			</artilcle>
			<?php endif ?>
			<article class="summarybox fullwidth fleft">
				<?php foreach ($summary as $key => $item) {
				if ($item['content']) { ?>
				<h2>Summary</h2>
				<div class="media-body">
					<div class="media-left fleft">
						<img src="<?php echo $item['image']; ?>" class="img-responsive" alt="" />
					</div>
					<?php echo $item['content']; ?>
				</div>
				<?php }
				} ?>
				<?php if ($citation): ?>
				<cite> <?php echo $citation; ?> </cite>
				<?php endif ?>
			</article>

			<div class="filterpanel listing fullwidth fleft visible-xs">
				<?php if (md_is_mobile()): ?>
				<?php include(locate_template('boxes/filter-resource-panel.php')); ?>
				<?php endif ?>
			</div>

			
			<div class="after_post fullwidth fleft">
					<!-- <a href="#" class="download_file">Download file</a> -->
					<?php include(locate_template('boxes/share-article.php')); ?>
				</div>
			<?php include(locate_template('boxes/filter-resources-recommended.php')); ?>
		</div>

		<div class="col-md-3 col-sm-4 col-xs-12 hidden-xs">
			<div class="filterpanel listing fullwidth fleft">
				<?php if (!md_is_mobile()): ?>
				<?php include(locate_template('boxes/filter-resource-panel.php')); ?>
				<?php endif ?>
			</div>
		</div>

	</div>
	</main>
	<?php get_footer(); ?>