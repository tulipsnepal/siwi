<?php get_header();

$post = get_post();
// $acf= get_field_objects($post->ID);
// May 13, 2015
$date_of_report_publication= date('M d, Y',strtotime($post->post_date));
// echo "<pre>";print_r($post);exit;

// $resource_type_item = $acf['resource_type_cf']['value']->name;
$resource_type_item = get_field('news_resource_type_cf',$post->ID)->name;

$featured_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

$news_type = get_the_terms($post->ID,'news_category');
				
$resourcetype = '';

foreach ($news_type as $key => $item1) {
	$keyname = $key>0 ? ', ': '';
	$itemname =  $item1->name;					
	$resourcetype .= $keyname.$itemname;
}

?>

<main id="mainblock" role="main" class="single mobiledesign news news-single-page content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-12">
				<?php if ($post->post_title): ?>
					<h1 class="h1">
					<?php echo $post->post_title; ?>
					</h1>
				<?php endif ?>


				<div class="quickinfo caption fullwidth fleft">
					<span class="resource-type">
							<?php echo $resourcetype;?>
					</span>
					<span class="report-date">Published: <?php echo $date_of_report_publication; ?></span>
				</div>


				<?php if ($featured_image): ?>
					<div class="impact_figure fullwidth fleft">
					<figure class="featured-image">
						<img src="<?php echo $featured_image; ?>" class="img-responsive" alt="" />
					</figure>
					</div>
				<?php endif ?>

				<?php if ($post->post_content): ?>
					<article class="preamble fullwidth fleft">
						<?php echo $post->post_content; ?>
					</article>
				<?php endif ?>

				<div class="fullwidth sharebox fleft">
				<div class="after_post fullwidth fleft">
				<?php include(locate_template('boxes/share-article.php')); ?>

				</div>

			</div>
	</div>

	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="fullwidth fleft">
				<?php include(locate_template('boxes/sidebar-single-news.php')); ?>
			</div>
	</div>

</main>


<?php get_footer(); ?>