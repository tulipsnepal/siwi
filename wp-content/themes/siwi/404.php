<?php get_header(); ?>
<main id="mainblock" role="main" class="content-wrapper">
<div class="container">
	<div class="row">
		
		<?php //get_template_part('boxes/sidebar','single-left'); ?>
		
		<div class="main_single col-xs-12">
			
			<div class="entry-content">
				
				<h1>404: Page not found</h1>
				<h2>Sorry but the page you are looking for has not been found.</h2>
				
			</div>
			
		</div>
	</div>
	<?php //get_template_part('boxes/sidebar','single-right'); ?>
</div>
</main>
<?php get_footer(); ?>