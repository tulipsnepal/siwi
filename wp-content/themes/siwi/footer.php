<!-- </div> -->
<!-- CLOSE #WRAP -->
<div class="prefooter fullwidth fleft">
	<div class="container">
		<div class="row">
		<div class="fullwidth fleft">
			<div class="col-xs-6 col-sm-3 col-md-3">
			<a href="http://www.worldwaterweek.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/assets/images/siwi-worldwaterweek.png" alt="" class="img-responsive"></a>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-3">
			<a href="http://www.swedishwaterhouse.se/en/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/assets/images/siwi-swedishwaterhouse.png" alt="" class="img-responsive"></a>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-4">
			<a href="http://www.watergovernance.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/assets/images/wgflogo.png" alt="" class="img-responsive"></a>
			</div>
			<div class="col-xs-6 col-sm-3 col-md-2">
			<a href="http://www.internationalwatercooperation.org/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/assets/images/icwc.png" alt="" class="img-responsive"></a>
			</div>
			</div>
		</div>
	</div>
</div>
<footer id="footer" class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<h5><?php echo get_field('footer_headline_1','option') ?></h5>
				<p><?php echo get_field('footer_description_1','option') ?></p>
			</div>
			<div class="hidden-sm hidden-xs col-md-3">
				<h5 class="footer_title">
					<?php echo get_field('footer_headline_2','option') ?>
				</h5>
					<?php if ( has_nav_menu( 'primary-menu' ) ) : ?>
						<?php $args = array(
							'depth'        => 2,
							'theme_location' => 'footer-menu',
							'menu_class' => 'footer-menu footer_navigation',
							'container' => ''
							);?>
							<?php wp_nav_menu( $args ); ?>
						<?php endif; ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<h5><?php echo get_field('footer_headline_3','option') ?></h5>
					<?php echo get_field('footer_description_3','option') ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3">
					<h5><?php echo get_field('footer_headline_4','option') ?></h5>
					<p><?php echo get_field('footer_description_4','option') ?></p>
					<div class="tipsafooter">
						<span class="sharef">
							<a target="_blank" href="<?php echo get_field('facebook','option') ?>" class="facebook">&nbsp;</a>
							<a target="_blank" href="<?php echo get_field('twitter','option') ?>" class="twitter">&nbsp;</a>
							<!-- <a target="_blank" href="https://plus.google.com/" class="gplus">&nbsp;</a> -->
							<a target="_blank" href="<?php echo get_field('linkedin','option') ?>" class="linkedin">&nbsp;</a>
						</span>
					</div>
				</div>
				<div class="col-xs-12 caption">
					<p><?php echo get_field('copyright_text','option') ?></p>
					<a href="javascript:void(0);" class="scrollTop"><?php echo get_field('back_to_top','option') ?></a></span>
				</div>
			</div>
		</div>
	</div>
</footer>
</div><!-- mobile wrapper closed-->
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/main.min.js"></script>
<!--[if lt IE 10]>
  <script src="<?php bloginfo('template_directory'); ?>/assets/js/addons/ie-bootstrap-carousel.js"></script>
 <![endif]-->
 <!--[if lt IE 9]>
	<script src="<?php bloginfo('template_directory'); ?>/assets/js/addons/respond.min.js"></script>
<![endif]-->
</body>
</html>