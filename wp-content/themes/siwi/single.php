<?php get_header();

$post = get_post();

$date_of_report_publication= date('M d, Y',strtotime($post->post_date));

$featured_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$news_type = get_the_terms(get_the_ID(),'category');
$resourcetype = '';

foreach ($news_type as $key => $item1) {
  $keyname = $key>0 ? ', ': '';
  $itemname =  $item1->name;          
  $resourcetype .= $keyname.$itemname;
}

$author_id=$post->post_author;

?>

<main id="mainblock" role="main" class="single mobiledesign news news-single-page content-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-12">
				<?php if ($post->post_title): ?>
					<h1 class="h1">
					<?php echo $post->post_title; ?>
					</h1>
				<?php endif ?>


				<div class="quickinfo caption fullwidth fleft">
					 <div class="caption fullwidth fleft">                  
		                    <span class="resource-type"><?php echo get_the_author_meta('display_name',$author_id); ?></span>
		                  <span class="resource-lang"><?php echo $resourcetype; ?></span> 
		                  <span class="report-date"><?php  echo $date_of_report_publication ?></span>
		                  </div>
					<span class="report-date">Published: <?php echo $date_of_report_publication; ?></span>
				</div>


				<?php if ($featured_image): ?>
					<div class="impact_figure fullwidth fleft">
					<figure class="featured-image">
						<img src="<?php echo $featured_image; ?>" class="img-responsive" alt="" />
					</figure>
					</div>
				<?php endif ?>

				<?php if ($post->post_content): ?>
					<article class="preamble fullwidth fleft">
						<?php echo $post->post_content; ?>
					</article>
				<?php endif ?>

			<div class="fullwidth sharebox fleft">				
				<div class="after_post fullwidth fleft">
				<?php include(locate_template('boxes/share-article.php')); ?>
				</div>
			</div>


	</div>

	<div class="col-md-3 col-sm-4 col-xs-12">
		<div class="fullwidth fleft">
			<?php
			get_template_part('boxes/filter','blog-desktop');
			get_template_part('boxes/sidebar','blog-right');
			?>			
			</div>
	</div>

</main>

<?php get_footer(); ?>