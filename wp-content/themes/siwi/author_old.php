<?php get_header(); ?>



<div class="content">



<div class="content_holder blog">

	

	<?php get_template_part('boxes/sidebar','blog-left'); ?>

	

	<div class="main_blog">

	
		<?php switch($post->post_type){

			case 'post': get_template_part('boxes/blog-post','loop'); break;

			default: get_template_part('boxes/single','loop'); break;

		} ?>

		<?php  ?>

	

	</div>
	

	<?php if(function_exists('wp_pagenavi')) {

	    wp_pagenavi(array('options' => array('prev_text' => 'prev', 'next_text' => 'next')));

	} ?>



</div>



<?php get_template_part('boxes/sidebar','author-right'); ?>



</div>



<?php get_footer(); ?>