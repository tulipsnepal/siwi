<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js lt-ie9" lang="en"><![endif]-->
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<title><?php bloginfo('name'); ?></title>
	<meta name="description" content="Intelliweb">
	<meta name="author" content="Intelliweb">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="mobilewrapper">
			<header id="header" class="header">
				<div class="container">
					<div class="row fixedrow">
						<div class="logo col-xs-6 col-sm-4">
							<a class="" href="<?php echo home_url(); ?>">
								<img src="<?php bloginfo('template_directory'); ?>/assets/images/logo@2x.png" height="103" width="141" alt="SIWI" class="img-responsive">
							</a>
						</div>
						<div class="col-xs-6 col-sm-8 pull-right">
							<div class="topHeader fright hidden-xs">
								<form class="navbar-form searchBar fright" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ; ?>">
									<button type="submit" class="btn btn-default">&nbsp;</button>
									<div class="form-group inputg">
										<input id="searchText" name="s" class="form-control" value="<?php the_search_query(); ?>" placeholder="Search" type="search">
										<label for="searchText">&nbsp;</label>
									</div>
								</form>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<nav class="navbar fullwidth fleft">
						<div class="container">
							<!-- Brand and toggle get grouped for better mobile display -->
							<a id="responsive-menu-button" href="#sidr-main" class="buttonclass">
								<span type="button" role="button" aria-label="Toggle Navigation" class="lines-button x fleft">
									<span class="lines"></span>
								</span>
								<span class="meny">Meny</span>
							</a>
							<a class="navbar-brand hidden" href="">SIWI</a>
							<div class="searchBox visible-xs">
								<a class="searchIcon" href="javascript:void(0);">&nbsp;</a>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="collapse navbar-collapse" id="navbar0">
								</div>
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="navbar">
									<?php if ( has_nav_menu( 'primary-menu' ) ) : ?>
										<?php $args = array(
											'depth'        => 5,
											'theme_location' => 'primary-menu',
											'menu_class' => 'main-menu nav navbar-nav fleft',
											'container' => '',
											// This one is the important part:
  											'walker' => new Custom_Walker_Nav_Menu
											);?>
											<?php wp_nav_menu( $args ); ?>
										<?php endif; ?>
										<div class="logos hidethis fright">
											<a class="" href="<?php echo home_url(); ?>">
												<img src="<?php bloginfo('template_directory'); ?>/assets/images/logo@2x.png" height="206" width="282" alt="Water Governance Facility Logo" class="img-responsive">
											</a>
										</div>
									</div>
								</div><!-- /.navbar-collapse -->
							</div>
						</nav>
						<div id="searchform" class="belownavigation hidethis">
							<div class="col-xs-12 sok">
								<form class="navbar-form searchBar fleft" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ) ; ?>">
									<div class="form-group inputg">
										<input type="search" id="searchTexts" name="s" value="<?php the_search_query(); ?>" class="form-control" placeholder="">
										<label for="searchTexts">SIWI</label>
									</div>
									<button type="submit" class="btn btn-default">&nbsp;</button>
								</form>
							</div>
						</div>
					</div>
				</header>