<?php
/**
 * Template Name: Publication Template
 *
 * @package SIWI
 */
?>
<?php
get_header();
$post = get_post(11401);
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

if (isset($_GET['keyword']) && $_GET['keyword']) {
	$custom_args = array(
		'post_type'     =>  'publication',
		'post_status'   =>  array('publish'),
		'posts_per_page' => get_field('publications_limit','option'),
		'meta_key' => 'date_of_report_publication', //name of custom field
		'orderby' => 'meta_value_num',
		'order' => 'desc',
		'paged' => $paged,
       	'tax_query' => array(
       		'relation' => 'AND',
	        array(
	            'taxonomy' => 'keyword_categories', //or tag or custom taxonomy
	            'field' => 'slug',
	            'terms' => array($_GET['keyword'])
	        )
	    )
	);
}else{
	$custom_args = array(
	'post_type'     =>  'publication',
	'post_status'   =>  array('publish'),
	'posts_per_page' => get_field('publications_limit','option'),
	'meta_key' => 'date_of_report_publication', //name of custom field
	'orderby' => 'meta_value_num',
	'order' => 'desc',
	'paged' => $paged
	);
}
// echo "<pre>";print_r($custom_args);exit;
$custom_query = new WP_Query( $custom_args );
$items = $custom_query->posts;
?>
<main id="mainblock" role="main" class="resource main-page content-wrapper">
	<div class="container">
		<div class="row resource-list relativebox">
			<div class="col-md-9 col-sm-7 col-xs-12 ">
				<div class="row">
					<div class="col-xs-12">
						<h1 class="h1">
							<?php echo $post->post_title; ?>
						</h1>
					</div>
					<div class="post-content col-sm-12 col-xs-12 col-md-11">
						<div class="preamble"><?php echo $post->post_content; ?></div>
					</div>
				</div>
			</div>
			<?php if (!md_is_mobile()): ?>
				<div class="col-md-3 col-sm-5 col-xs-12 filter-resources absolutebox">
					<div class="filterpanel samebox ">
						<div class="form-group fullwidth fleft">
							<select name="filter-date" id="filter-date" class="form-control selectpicker">
								<option value="desc" selected="selected">Sort by date</option>
								<option value="alphabetical">Sort by alphabet</option>
							</select>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
		<div class="row">
			<?php if (!md_is_mobile()):
			get_template_part('boxes/filter','resource-desktop');
			endif ?>
			<div class="col-md-9 col-sm-7 col-xs-12">
				<?php if (md_is_mobile()):
				get_template_part('boxes/filter','resource-mobile');
				endif ?>
				<div class="main_single">
					<div class="row resource-library">
						<?php
						foreach ($items as $key => $item) {
							echo '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">';
							$resource_type_acf = get_field('resource_type_cf',$item->ID)->name;
							$resource_language_items =  get_field('resource_language_cf',$item->ID);
							$resource_language_arr = array();
							foreach ($resource_language_items as $key => $lang_item) {
								array_push($resource_language_arr, $lang_item->name);
							}
							$language_acf = implode('/ ', $resource_language_arr);
							$date_of_report_publication = date('Y',strtotime(get_field('date_of_report_publication',$item->ID)));
							
							$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

							$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
							$image = aq_resize( $img_url, 250, 166 , true,true,true);	
							$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

							echo '<a href="'.esc_url( post_permalink($item->ID) ).'">
							<figure>
								'.$imageTag.'
							</figure>
							<aside class="withimage caption fullwidth fleft">
								<span class="resource-type">'.$resource_type_acf.'</span>
								<span class="resource-lang">'.$language_acf.'</span>
								<span class="report-date">'.$date_of_report_publication.'</span>
								<h4>'.$item->short_headline.'</h4>
							</aside>
						</a>';
						echo '</div>';
					}
					?>
				</div>
			</div>
				<div class="row resource-paginate">
					<div class="col-xs-12" id="paginationWrapper">
						<nav class="fleft fullwidth" data-paged="1">
						<?php
						if (function_exists(custom_pagination)) {
							echo custom_pagination($custom_query->max_num_pages,"",$paged);
						}
						 ?>
						 </nav>
					</div>
				</div>
				<input type="hidden" id="keyword" name="keyword" value="<?php echo $_GET['keyword']; ?>" >
				<div class="clear"></div>
			<?php wp_reset_postdata(); ?>
		</div>
	</main>
	<?php get_footer(); ?>