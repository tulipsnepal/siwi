<?php
/**
* Template Name: News Template
*
* @package SIWI
*/
?>
<?php
get_header();
$post = get_post(11426);
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$custom_args = array(
	'post_type'     =>  'news',
	'post_status'   =>  array('publish'),
	'posts_per_page' => get_field('news_limit','option'),
	'orderby' => 'date',
	'order' => 'desc',
	'paged' => $paged
	);
$custom_query = new WP_Query( $custom_args );
$items = $custom_query->posts;
?>
<main id="mainblock" role="main" class="news-page newslistpage content-wrapper">
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="h1">
			<?php echo $post->post_title; ?>
			</h1>
		</div>
	</div>
	<div class="row relativebox">
		<div class="col-md-9 col-sm-7 col-xs-12">
			<div class="row">
				<div class="post-content col-sm-12 col-xs-12">
					<?php echo $post->post_content; ?>
				</div>
			</div>
		</div>
		<?php if (!md_is_mobile()): ?>
		<div class="col-md-3 col-sm-5 col-xs-12 filter-resources absolutebox">
			<div class="filterpanel samebox ">
				<div class="form-group fullwidth fleft">
					<select name="filter-date" id="filter-date" class="selectpicker">
						<option value="desc" selected="selected">Sort by date</option>
						<option value="alphabetical">Sort by alphabet</option>
					</select>
				</div>
			</div>
		</div>			
		<?php endif ?>		
	</div>
	<div class="row news-list">

	<?php if (!md_is_mobile()):
			get_template_part('boxes/filter','news-desktop');
			endif ?>
		<div class="col-md-9 col-sm-7 col-xs-12">

		<?php if (md_is_mobile()):
				get_template_part('boxes/filter','news-mobile');
				endif ?>
			<div class="news-library">

				<?php foreach ($items as $key => $item){
					$date_of_report_publication = date('M d, Y',strtotime($item->post_date));

					$img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

							$img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
							
							$image = aq_resize( $img_url, 250, 166 , true,true,true);	
							$imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';   

					$news_type = get_the_terms($item->ID,'news_category');
					
					$resourcetype = '';

					foreach ($news_type as $key => $item1) {
						$keyname = $key>0 ? ', ': '';
						$itemname =  $item1->name;					
						$resourcetype .= $keyname.$itemname;
					}

					echo '<div class="news_repeater fleft">
								<a href="'.esc_url( post_permalink($item->ID) ).'">
							<figure class="news_thumb">
								'.$imageTag.'
						</figure>
						<aside class="withimage">
							<div class="caption fullwidth fleft">
										<span class="resource-type">
													'.$resourcetype.'
											</span>
											<span class="report-date">'.$date_of_report_publication.'</span>
										</div>
								<h2>'.$item->post_title.'</h2>
								'.$item->post_excerpt.'
						</aside>
						</a>
						</div>';
				} ?>
			</div>
		</div>
	</div>
	<div class="row news-paginate">
		<div class="col-xs-12" id="paginationWrapper">
			<nav class="fleft fullwidth" data-paged="1">
				<?php
				if (function_exists(custom_pagination)) {
					echo custom_pagination($custom_query->max_num_pages,"",$paged);
				}
				?>
			</nav>
		</div>
	</div>
	<input type="hidden" id="keyword" name="keyword" value="<?php echo $_GET['keyword']; ?>" >
</div>
</main>
<?php get_footer(); ?>