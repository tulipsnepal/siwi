<?php
get_header();
?>

<main id="mainblock" role="main" class="news-page newslistpage content-wrapper">
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="h1">
			<?php
				if (function_exists('is_tag') && is_tag()) {
					echo 'Feed of Tag Archive for &quot;'.$tag.'&quot; - ';
				} elseif (is_archive()) {
					echo 'Feed of Archive - '; wp_title('');
				} elseif (is_search()) {
					echo 'Search for &quot;'.wp_specialchars($s).'&quot; - ';
				} elseif (!(is_404()) && (is_single()) || (is_page())) {
					wp_title(''); echo ' - ';
				} elseif (is_404()) {
					echo 'Not Found - ';
				}
				?>
			</h1>
		</div>
	</div>
	<div class="row news-list">		
	
		<div class="col-md-9 col-sm-7 col-xs-12">

			<div class="news-library">
				<?php
					if($post->post_type == 'post'){
						get_template_part('boxes/blog-post','loop');
					} else {
						get_template_part('boxes/archive','loop');
					}
					?>
				
			</div>

				<div class="row resource-paginate">
					<div class="col-xs-12">
						<?php if(function_exists('wp_pagenavi')) {?>
						<nav id="paginationWrapper" class="fleft fullwidth">
							<ul class="pagination" data-paged='1'>
							<?php
									if($post->post_type == 'post'){
										wp_pagenavi(array('options' => array('prev_text' => 'prev', 'next_text' => 'next')));
									} else {
										wp_pagenavi();
									}
									?>
								</ul>
							</nav>
						<?php } ?>
					</div>
				</div>
				<div class="clear"></div>

				
		</div>
		<div class="col-md-3 col-sm-5 col-xs-12 filter-blog pull-right">
		<?php
			get_template_part('boxes/filter','blog-desktop');
			get_template_part('boxes/sidebar','author-right');
			// get_template_part('boxes/sidebar','blog-right');
			?>
		</div>

	</div>
</div>
</main>
<?php get_footer(); ?>