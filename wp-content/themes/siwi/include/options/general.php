<?php

/* SIWI GENERAL OPTIONS */



if(isset($_POST['save_siwi_options'])){



	$new = array();

	$new['banner_url'] = isset($_POST['banner_url']) ? esc_attr($_POST['banner_url']) : '';

	$new['slider_title'] = isset($_POST['slider_title']) ? esc_attr($_POST['slider_title']) : '';

	$new['slider_desc'] = isset($_POST['slider_desc']) ? esc_attr($_POST['slider_desc']) : '';

	$new['first_headline'] = isset($_POST['first_headline']) ? esc_attr($_POST['first_headline']) : '';

	$new['second_headline'] = isset($_POST['second_headline']) ? esc_attr($_POST['second_headline']) : '';

	$new['third_headline'] = isset($_POST['third_headline']) ? esc_attr($_POST['third_headline']) : '';

	$new['footer_headline'] = isset($_POST['footer_headline']) ? esc_attr($_POST['footer_headline']) : '';

	$new['news_headline'] = isset($_POST['news_headline']) ? esc_attr($_POST['news_headline']) : '';

	$new['projects_headline'] = isset($_POST['projects_headline']) ? esc_attr($_POST['projects_headline']) : '';

	$new['publications_headline'] = isset($_POST['publications_headline']) ? esc_attr($_POST['publications_headline']) : '';

	$new['publications_limit'] = isset($_POST['publications_limit']) ? esc_attr($_POST['publications_limit']) : '';



	//Update options

	siwi_update_options($new);

}

//Get options

$banner_url = siwi_get_option('banner_url');

$footer_headline1 = siwi_get_option('footer_headline1');
$footer_description1 = siwi_get_option('footer_description1');

$footer_headline2 = siwi_get_option('footer_headline2');
$footer_headline3 = siwi_get_option('footer_headline3');

$footer_headline4 = siwi_get_option('footer_headline4');
$footer_description4 = siwi_get_option('footer_description4');

$first_headline = siwi_get_option('first_headline');

$second_headline = siwi_get_option('second_headline');

$third_headline = siwi_get_option('third_headline');

$footer_headline = siwi_get_option('footer_headline');

$news_headline = siwi_get_option('news_headline');

$projects_headline = siwi_get_option('projects_headline');

$publications_headline = siwi_get_option('publications_headline');

$publications_limit = siwi_get_option('publications_limit');

?>





<div class="wrap">

	<?php screen_icon('options-general'); ?>

	<h2>SIWI Options</h2>

	<?php if(isset($_POST['save_siwi_options'])): ?>

	<div id="message" class="updated"><p>Options saved.</p></div>

	<?php endif; ?>

	<form method="POST" action="">

		<table class="form-table">
			<tr valign="top">

					<td><h3>Blog banner</h3></td>

			</tr>

			<tr valign="top">

				<th scope="row"><label>Image URL:</label></th>

				<td><input type="text" name="banner_url" value="<?php echo $banner_url; ?>" class="regular-text" /></td>

			</tr>


			<tr valign="top">

					<td><h3>Footer</h3></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Footer headline 1</label></th>

					<td><input type="text" name="footer_headline1" value="<?php echo $footer_headline1; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Footer Description 1</label></th>

					<td><textarea name="footer_description1" cols="55" rows="4"><?php echo $footer_description1; ?></textarea></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Footer headline 2</label></th>

					<td><input type="text" name="footer_headline2" value="<?php echo $footer_headline2; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Footer headline 3</label></th>

					<td><input type="text" name="footer_headline3" value="<?php echo $footer_headline3; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Footer headline 4</label></th>

					<td><input type="text" name="footer_headline4" value="<?php echo $footer_headline4; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Footer Description 4</label></th>

					<td><textarea name="footer_description4" cols="55" rows="4"><?php echo $footer_description4; ?></textarea></td>

				</tr>


				<tr valign="top">

					<td><h3>Home page</h3></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>First headline</label></th>

					<td><input type="text" name="first_headline" value="<?php echo $first_headline; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Second headline</label></th>

					<td><input type="text" name="second_headline" value="<?php echo $second_headline; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Third headline</label></th>

					<td><input type="text" name="third_headline" value="<?php echo $third_headline; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<td><h3>Page tabs</h3></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>News headline</label></th>

					<td><input type="text" name="news_headline" value="<?php echo $news_headline; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Projects headline</label></th>

					<td><input type="text" name="projects_headline" value="<?php echo $projects_headline; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>Publications headline</label></th>

					<td><input type="text" name="publications_headline" value="<?php echo $publications_headline; ?>" class="regular-text" /></td>

				</tr>

				<tr valign="top">

					<td><h3>Pagination</h3></td>

				</tr>

				<tr valign="top">

					<th scope="row"><label>No of pages to show for publication</label></th>

					<td><input type="number" name="publications_limit" value="<?php echo $publications_limit; ?>" class="regular-text" /></td>

				</tr>


				<tr>

					<td><input type="submit" value="Save Options" name="save_siwi_options" class="button-primary"/><td>

				</tr>

		</table>

	</form>

</div>