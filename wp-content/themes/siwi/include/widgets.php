<?php



 /* INCLUDE WIDGET CLASSES */

 require_once('widgets/background_color.php');

 require_once('widgets/image_headline.php');

 require_once('widgets/complex_image_text.php');

 require_once('widgets/share.php');

 require_once('widgets/twitter.php');

 require_once('widgets/flickr.php');

 require_once('widgets/map.php');

require_once('widgets/person_widget.php');

require_once('widgets/blog_authors.php');

require_once('widgets/siwi_custom_menu.php');

 

 /* REGISTER WIDGETS */

	function register_siwi_widgets(){

		register_widget('Background_Color_Widget');

		register_widget('Image_Headline_Widget');

		register_widget('Complex_Image_Text_Widget');

		register_widget('Custom_Share_Widget');

		register_widget('Custom_Twitter_Widget');

		register_widget('Custom_Flickr_Widget');

		register_widget('Map_Widget');

		register_widget('Person_Widget');

		register_widget('Blog_Authors_Widget');

		register_widget('SIWI_Nav_Menu_Widget');

	}

	

 add_action('widgets_init', 'register_siwi_widgets');



?>