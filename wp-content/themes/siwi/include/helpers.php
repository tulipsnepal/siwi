<?php
/* HELPER FUNCTIONS FOR SIWI THEME */
/* Debug Log Function */
if(!function_exists('siwi_log')):
	function siwi_log($mixed){
		
		if (is_array($mixed)) {
			$mixed = print_r($mixed, 1);
		} else if (is_object($mixed)) {
			ob_start();
			var_dump($mixed);
			$mixed = ob_get_clean();
		}
		
		$handle = fopen(THEME_DIR . '/log', 'a');
		fwrite($handle, $mixed . PHP_EOL);
		fclose($handle);
	}
	endif;
	/* Get Setting */
	if(!function_exists('siwi_get_option')):
		function siwi_get_option($option_name = false){
			global $siwi_options;
			return isset($siwi_options[$option_name]) ? $siwi_options[$option_name] : false;
		}
		endif;
		/* Update Setting */
		if(!function_exists('siwi_update_option')):
			function siwi_update_option($option_name, $option_value){
				global $siwi_options;
				
				if(!empty($option_name) && !empty($option_value)){
					$siwi_options[$option_name] = $option_value;
					update_option(THEME_OPTIONS,$siwi_options);
				}
			}
			endif;
			/* Update Settings */
			if(!function_exists('siwi_update_options')):
				function siwi_update_options($args = array()){
					global $siwi_options;
					if(!empty($args)){
						foreach($args as $option_name => $option_value){
							if(!empty($option_name) && !empty($option_value)){
								$siwi_options[$option_name] = $option_value;
							}
						}
						update_option(THEME_OPTIONS,$siwi_options);
					}
				}
				endif;
				/* Get taxonomies links */
				if(!function_exists('siwi_the_taxonomy')):
					function siwi_the_taxonomy ($taxonomy, $separator = '') {
						global $post;
						$terms = wp_get_object_terms($post->ID, $taxonomy);
						$term_output = array();
						foreach ( $terms as $term ) {
							$link = get_term_link((int)$term->term_id, $term->taxonomy);
							$term_output[] = '<span class="label label-warning resource-type"><a href="'.$link.'">'.$term->name.'</a></span>';
						}
						echo implode($separator,$term_output);
					}
					endif;
					/* First image from posts */
					if(!function_exists('siwi_catch_image()')):
						function siwi_catch_image(){
							global $post, $posts;
							$first_img = '';
							ob_start();
							ob_end_clean();
							$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
							$first_img = $matches[1][0];
							return $first_img;
						}
						endif;
						?>