<?php 

if(isset($_GET['fetch_publications'])){
	add_action('init','siwi_fetch_publications');
	
}

function siwi_fetch_publications(){
	
	//echo "FETCHING TEST...";
	set_time_limit(300);
	include_once('simple_html_dom.php');
	
	$i = absint($_GET['fetch_publications']);
	$t = ($i+10);
	$ps = array();
	
	$url = 'http://www.siwi.org/sa/node.asp?node=52&skip='.$i;
	$main_html = file_get_html($url);
	
	
	foreach($main_html->find('td a') as $link){
				
				//Do not double links
				if(!empty($link->plaintext) && !empty($link->href)){
					
					//Create single page url
					$single_url = 'http://www.siwi.org/'.$link->href;
					
					//Get single page html
					$single_html = file_get_html($single_url);
					
					//Get title
					foreach($single_html->find('h1') as $h1){
						$title = $h1->plaintext;
						$h1->innertext  = '';
						break;
					}
					
					
					//Get content
					foreach($single_html->find('div#content div#left') as $desc){
						$content = $desc->innertext;
						$content = strip_tags($content,'<p><h2><h3><h4><i><b><br><br/><br /><strong>');
						$content = str_replace("&lt;&lt; Back","",$content);
						$content = preg_replace('!\s+!', ' ', $content);
						$content = force_balance_tags($content);
						break;
					}
				
				//Get thumbnail
				$no_img = '/plugins/Resources/Images/no_image.jpg';
				foreach($single_html->find('div#rightColumn img') as $img){
						if($img->src != $no_img){
							$thumb_url = 'http://www.siwi.org'.$img->src;
						} else {
							$thumb_url = '';
						}
						break;
				}
				
				//Get title
				foreach($single_html->find('div#rightColumn a') as $pdf){
						$pdf_url = 'http://www.siwi.org'.$pdf->href;
						break;
					}
					
					siwi_log($title);
					siwi_log(' ');
					siwi_log($content);
					siwi_log(' ');
					siwi_log($thumb_url);
					siwi_log($pdf_url);
					siwi_log(' ');
					
					//Prepare data to insert into post
					$p = array();
					$p['post_title'] = addslashes(utf8_encode($title));
					$p['post_content'] = utf8_encode($content);
					$p['img'] = $thumb_url;
					$p['pdf'] = $pdf_url;
					
					$ps[] = $p;
					
					
				}
				
				
	}
	
	
	
	//$i += 10;
	
 //end while

$ps = array_reverse($ps);
				
				//DO INSERT
				siwi_old_to_wp($ps);
	
}

function is_valid_url($url){
	return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
}

function siwi_post_exists($post_title){
	global $wpdb;
	$query = "SELECT `post_title` FROM `$wpdb->posts` WHERE `post_title` = '$post_title'";
	$result = $wpdb->get_results($wpdb->prepare($query));
	return !empty($result) ? true : false;
}

//Insert
function siwi_old_to_wp($ps = array()){
			
			if(empty($ps)){
				return false;
			}
				
			for($i=0; $i < count($ps); $i++){
			
			$p = $ps[$i];
			
			if(!siwi_post_exists($p['post_title']) || 1){
			
			
			//Set common params
			$p['post_type'] = 'publication';
			$p['post_status'] = 'publish';
		
			// Insert post into the database
	  	$post_id = wp_insert_post($p);
	  	if($post_id){
	  		
	  		//Insert pdf link
	  		if(isset($p['pdf']) && !empty($p['pdf']) && is_valid_url($p['pdf'])){
	    		update_post_meta($post_id, '_download_link', $p['pdf']);
	    	}
	    	
	    	//Insert thumbnail
	    	if(isset($p['img']) && !empty($p['img']) && is_valid_url($p['img'])){
					include_once(ABSPATH.'wp-admin/includes/file.php');
					include_once(ABSPATH . 'wp-admin/includes/image.php');
					$upload = wp_upload_dir();
					
						$tmp_file = download_url($p['img']);
						$file_name = basename($p['img']);
						$attach_upload['url']  = $upload['url'] . '/' . $file_name;
            $attach_upload['path'] = $upload['path'] . '/' . $file_name;
            $renamed               = @rename($tmp_file, $attach_upload['path']);
						$atta_ext = end(explode('.', $file_name));
            $attachment = array(
                        'post_title' => $file_name,
                        'post_content' => '',
                        'post_type' => 'attachment',
                        'post_parent' => $post_id,
                        'post_mime_type' => 'image/' . $atta_ext,
                        'guid' => $attach_upload['url']
                    );      
             $attach_id = wp_insert_attachment($attachment, $attach_upload['path'],$post_id);
             wp_update_attachment_metadata($attach_id, wp_generate_attachment_metadata($attach_id, $attach_upload['path']));
             
             set_post_thumbnail($post_id,$attach_id);    
	    	}
	   }
 }
 
 sleep(2);

} //end foreach
  	
}
?>