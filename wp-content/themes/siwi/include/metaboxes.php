<?php



/* ADD METABOXES AND FUNCTIONS FOR PAGE */



add_action( 'load-post.php', 'siwi_metabox_setup');

add_action( 'load-post-new.php', 'siwi_metabox_setup' );



function siwi_metabox_setup() {

	global $typenow;

	if($typenow == 'page'){

		add_action( 'add_meta_boxes', 'siwi_add_metaboxes');

		add_action( 'save_post', 'siwi_save_metaboxes', 10, 2 );

	}

}



function siwi_add_metaboxes(){

add_meta_box(

		'tabs',			// Unique ID

		__( 'Tabs'),		// Title

		'siwi_draw_tab_metabox',		// Callback function

		'page',					// Admin page (or post type)

		'side',					// Context

		'default'					// Priority

	);

	

	add_meta_box(

		'contact',			// Unique ID

		__( 'Contact'),		// Title

		'siwi_draw_contact_metabox',		// Callback function

		'page',					// Admin page (or post type)

		'side',					// Context

		'default'					// Priority

	);

}



function siwi_draw_tab_metabox( $object, $box ) { 

	$cat_tabs_nonce = wp_nonce_field( basename( __FILE__ ), 'siwi_cat_tabs_nonce' );

	

	$cat_tabs = get_post_meta($object->ID,'siwi_cat_tabs',true);

	

	$news_cat = isset($cat_tabs['news_cat']) ? $cat_tabs['news_cat'] : '';

	$project_cat = isset($cat_tabs['project_cat']) ? $cat_tabs['project_cat'] : '';

	$publication_cat = isset($cat_tabs['publication_cat']) ? $cat_tabs['publication_cat'] : '';

	$num_feeds = isset($cat_tabs['num_feeds']) ? $cat_tabs['num_feeds'] : 3;



	

	

	$cat_args = array(	

		'taxonomy'	=> 'news_category',

		'hide_empty'=> 0

	);



	$terms = get_categories($cat_args);

	

	if(!empty($terms)){

		echo '<label>'.__('News Category').':</label><br/>';

		echo '<select name="news_cat" style="width:200px;">';

		echo '<option value="0" selected/>  None</option>';

		foreach ($terms as $term) {

			$selected = $term->term_id == $news_cat ? 'selected' : '';	

			echo '<option value="'.$term->term_id.'" '.$selected.'/>  '.$term->name.'</option>';

		}

		echo '</select><br/><br/>';

	}

	

	$cat_args = array(	

		'taxonomy'	=> 'project_category',

		'hide_empty'=> 0

	);



	$terms = get_categories($cat_args);

	if(!empty($terms)){

		echo '<label>'.__('Project Category').':</label><br/>';

		echo '<select name="project_cat" style="width:200px;">';

		echo '<option value="0" selected/>  None</option>';

		foreach ($terms as $term) {

			$selected = $term->term_id == $project_cat ? 'selected' : '';	

			echo '<option value="'.$term->term_id.'" '.$selected.'/>  '.$term->name.'</option>';

		}

		echo '</select><br/><br/>';

	}

	

	$cat_args = array(	

		'taxonomy'	=> 'publication_category',

		'hide_empty'=> 0

	);



	$terms = get_categories($cat_args);

	

	if(!empty($terms)){

		echo '<label>'.__('Publication Category').':</label><br/>';

		echo '<select name="publication_cat" style="width:200px;">';

		echo '<option value="0" selected/>  None</option>';

		foreach ($terms as $term) {

			$selected = $term->term_id == $publication_cat ? 'selected' : '';	

			echo '<option value="'.$term->term_id.'" '.$selected.'/>  '.$term->name.'</option>';

		}

		echo '</select><br/><br/>';

	}

	

	echo '<label>Max number of feeds: </label> <input name="num_feeds" type="text" value="'.$num_feeds.'" style="width: 30px;"/>';

}



function siwi_draw_contact_metabox($object, $box){

	 wp_nonce_field( __FILE__ , 'siwi_contact_nonce' );

	   $defaults = array();

	   $siwi_meta = get_post_meta($object->ID,'_siwi_contact_meta',true);	   

	   $siwi_meta = wp_parse_args( (array) $siwi_meta, $defaults );

	   

	   $contact_args = array(

	   	'post_type' => 'siwi_contact',

	   	'posts_per_page' => -1,

	   	'orderby' => 'title',

	   	'order' => 'ASC'

	   );

     $contacts = get_posts ( $contact_args );

			?>

	   	<ul>

	   	<?php foreach($contacts as $contact) :

	   		$checked = in_array($contact->ID, $siwi_meta) ? 'checked': '';

	   	?>

	 			<li><input type="checkbox" name="siwi[contact][]" value="<?php echo $contact->ID; ?>" <?php echo $checked; ?>/> <?php echo $contact->post_title; ?> </li>

	   	<?php endforeach; ?>

	  </ul>

	  <?php

}



function siwi_save_metaboxes( $post_id, $post ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )

    return $post_id;

    

	if ( !isset( $_POST['siwi_cat_tabs_nonce'] ) || !wp_verify_nonce( $_POST['siwi_cat_tabs_nonce'], basename( __FILE__ ) ) )

		return $post_id;

	

	if($post->post_type == 'page'){

	

		$post_type = get_post_type_object( $post->post_type );

		

		if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )

			return $post_id;

		

		$cat_tabs = array();

		$cat_tabs['news_cat'] = isset($_POST['news_cat']) ? $_POST['news_cat'] : '';

		$cat_tabs['project_cat'] = isset($_POST['project_cat']) ? $_POST['project_cat'] : '';

		$cat_tabs['publication_cat'] = isset($_POST['publication_cat']) ? $_POST['publication_cat'] : '';

		$cat_tabs['num_feeds'] = isset($_POST['num_feeds']) ? absint($_POST['num_feeds']) : 3;

		

		update_post_meta( $post_id, 'siwi_cat_tabs', $cat_tabs );

		

		$contact = isset($_POST['siwi']['contact']) ? $_POST['siwi']['contact'] : array();

		update_post_meta( $post_id, '_siwi_contact_meta', $contact );

	}



}





function siwi_cat_tabs($post_id = false){ 

	

	if(empty($post_id))

		return false;

		//return array();

	$cat_tabs = get_post_meta($post_id,'siwi_cat_tabs',true);

	$news_cat = isset($cat_tabs['news_cat']) ? $cat_tabs['news_cat'] : 0;

	$project_cat = isset($cat_tabs['project_cat']) ? $cat_tabs['project_cat'] : 0;

	$publication_cat = isset($cat_tabs['publication_cat']) ? $cat_tabs['publication_cat'] : 0;

	$num_feeds = isset($cat_tabs['num_feeds']) ? $cat_tabs['num_feeds'] : 3;

	

	if(!$news_cat && !$project_cat && !$project_cat)

		return false;

		

	$return = array();

						

	$cats = array(

						  'news_category' => $news_cat,

							'project_category' => $project_cat,

							'publication_category' => $publication_cat  

						);

						

	$headlines = array(

						  'news_category' => siwi_get_option('news_headline'),

							'project_category' => siwi_get_option('projects_headline'),

							'publication_category' => siwi_get_option('publications_headline')  

						);

						

	foreach($cats as $cat => $id){

			if($id){

				$tax_query = array(

				array(

					'taxonomy' => $cat,

					'field' => 'term_id',

					'terms' => $id,

					'hierarchical' => false

				)

			);

			

			$args = array( 

			'posts_per_page' => $num_feeds,

			'tax_query' => $tax_query);

			$loop = new WP_Query( $args );

			

			while( $loop->have_posts() ) {	$loop->the_post();

				$new = array();

				$new['title'] = get_the_title();

				$new['content'] = wp_trim_words( get_the_content(), 17, '...' );

				$new['permalink'] = get_permalink();

				$new['date'] = get_the_date('F j, Y');

				$return[$headlines[$cat]][] = $new;

			}

			

		}

	}

	

	return $return;

}


/*
 * Authors extra fields
 */

add_action( 'show_user_profile', 'siwi_extra_profile_fields' );
add_action( 'edit_user_profile', 'siwi_extra_profile_fields' );

function siwi_extra_profile_fields( $user ) { ?>

	<h3>Extra profile information</h3>

	<table class="form-table">

		<tr>
			<th><label for="user_phone">Phone number</label></th>

			<td>
				<input type="text" name="user_phone" id="user_phone" value="<?php echo esc_attr( get_the_author_meta( 'user_phone', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Enter phone number</span>
			</td>
		</tr>
		<tr>
			<th><label for="user_title">Title</label></th>

			<td>
				<input type="text" name="user_title" id="user_title" value="<?php echo esc_attr( get_the_author_meta( 'user_title', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Enter title</span>
			</td>
		</tr>

	</table>
<?php }

add_action( 'personal_options_update', 'siwi_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'siwi_save_extra_profile_fields' );

function siwi_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_usermeta( $user_id, 'user_phone', $_POST['user_phone'] );
	update_usermeta( $user_id, 'user_title', $_POST['user_title'] );
}

/*
 * Post hashtags
 */

function add_siwi_hashtag_metabox(){
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	
	add_meta_box('siwi_hashtag', 'Hash tag', 'siwi_display_hashtag_metabox', 'post', 'side', 'default');
}
function siwi_display_hashtag_metabox($post){
    wp_nonce_field(plugin_basename( __FILE__ ), 'siwi_nonce');
    
    $tag_value = get_post_meta($post->ID, '_siwi_hashtag_tag', true);
    echo '<p>
    <label for="siwi_hashtag_tag">Tag (including #): </label><br>
    <input id="siwi_hashtag_tag" name="siwi_hashtag_tag" value="'.$tag_value.'"></p>';
}

function siwi_save_hashtag_metabox($post_id){
    if(!current_user_can('edit_posts')) return;
     
    if(!isset($_POST['siwi_nonce']) || !wp_verify_nonce($_POST['siwi_nonce'], plugin_basename(__FILE__))) return;
    
    if(isset($_POST['siwi_hashtag_tag'])){
	    $category_data = sanitize_text_field($_POST['siwi_hashtag_tag']);
	    update_post_meta($post_id, '_siwi_hashtag_tag', $category_data);
    }
}

add_action('add_meta_boxes', 'add_siwi_hashtag_metabox');
add_action('save_post', 'siwi_save_hashtag_metabox');


?>