<?php
/* REGISTER MENUS */
add_action( 'init', 'register_siwi_menus' );

function register_siwi_menus() {
	register_nav_menus(
		array(
			'primary-menu' => __( 'Primary Menu' ),
			'footer-menu' => __( 'Footer Menu' )
		)
	);
}
?>