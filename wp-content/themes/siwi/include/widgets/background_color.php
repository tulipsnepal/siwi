<?php
/* CUSTOM BACKGROUND COLOR WIDGET */
class Background_Color_Widget extends WP_Widget {

	function Background_Color_Widget() {
		$widget_ops = array( 'classname' => 'widget_grey one_third', 'description' => __( 'Background color widget with title and text.' ) );
		$control_ops = array( 'id_base' => 'widget_grey' );
		$this->WP_Widget('widget_grey', __('SIWI Background Color Widget'), $widget_ops, $control_ops);
		add_action( 'admin_init', array( $this, 'admin_setup' ) );
	}
	
	function admin_setup() {
		global $pagenow;
		if ( 'widgets.php' == $pagenow ) {
			wp_enqueue_style('farbtastic');
			wp_enqueue_script('farbtastic');
		}
	}
	function widget($args, $instance) {
		extract($args);
		echo $before_widget;
		echo '<div style="background-color:'.$instance['background'].';">';
		echo $before_title . $instance['title'] . $after_title;
		echo '<p>'.$instance['text'].'</p>';
		echo '</div>';
		echo $after_widget;
	}
	function update($new_instance, $old_instance) {;
		return $new_instance;
	}
	
	function form($instance) { 
		$defaults = array( 'text' => '', 'title' => '', 'background' => '#fff');
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<script type="text/javascript">
			//<![CDATA[
				jQuery(document).ready(function()
				{
					// colorpicker field
					jQuery('.cw-color-picker').each(function(){
						var $this = jQuery(this),
							id = $this.attr('rel');

						$this.farbtastic('#' + id);
					});
				});
				
		jQuery(document).ajaxSuccess(function(e, xhr, settings) {
			
			// Run the farbtastic code again
							jQuery('.cw-color-picker').each(function(){
									var $this = jQuery(this),
										id = $this.attr('rel');
			
									$this.farbtastic('#' + id);
								});
					//}
		});
			//]]>   
		  </script>	
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:'); ?></label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e('Text:'); ?></label>
			<textarea id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"  class="widefat"><?php echo $instance['text']; ?></textarea>
		</p>
		<p>
	  <label for="<?php echo $this->get_field_id('background'); ?>"><?php _e('Background Color:'); ?></label> 
	  <input class="widefat" id="<?php echo $this->get_field_id('background'); ?>" name="<?php echo $this->get_field_name('background'); ?>" type="text" value="<?php echo $instance['background']; ?>"/>
		<div class="cw-color-picker" rel="<?php echo $this->get_field_id('background'); ?>"></div>
	</p>
	<?php
	} 
}
?>