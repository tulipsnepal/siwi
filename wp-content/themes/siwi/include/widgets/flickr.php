<?php

/* CUSTOM FLICKER WIDGET */
class Custom_Flickr_Widget extends WP_Widget {
	
	function Custom_Flickr_Widget() {
		$widget_ops = array( 'classname' => 'flickr_widget one_third', 'description' => __( 'Flickr widget to show photos.' ) );
		$control_ops = array( 'id_base' => 'flickr_widget' );
		$this->WP_Widget('flickr_widget', __('SIWI Flickr Widget'), $widget_ops, $control_ops);
	}
	
	/**
	 * Displays the widget contents.
	 */
	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;

		$photos = $this->get_photos( $instance['rss'], $instance['count']);
		
		if(!empty($photos)){
			echo '<ul class="flickr">';
			foreach ( $photos as $photo ) {
				echo '<li><a href="'.$photo['img_url'].'" title="'.$photo['title'].'" target="_blank"><img src="'.$photo['img_src'].'"></a></li>';
			}
			echo '</ul>';
		}
		echo $args['after_widget'];
	}

	
	function get_photos( $rss, $count = 8) {
		if(empty($rss))
			return false;
		
	$transient_key = md5( 'siwi_flickr_cache_' . $rss . $count);
	$cached = get_transient( $transient_key );
	if (!empty($cached))
			return $cached;
		
			$rss = fetch_feed($rss);
			if (!is_wp_error( $rss ) ) { 
    	$maxitems = $rss->get_item_quantity($count); 
    	$rss_items = $rss->get_items(0, $maxitems);
    	foreach ( $rss_items as $item ) {
    		$temp = array();
    		$temp['img_url'] = esc_url( $item->get_permalink() );
    		$temp['title'] = esc_html( $item->get_title() );
    		$content =  $item->get_content();
    		preg_match_all("/<IMG.+?SRC=[\"']([^\"']+)/si",$content,$sub,PREG_SET_ORDER);
				$photo_url = str_replace( "_m.jpg", "_t.jpg", $sub[0][1] );
    		$temp['img_src'] = esc_url($photo_url);
    		$output[] = $temp;
			}
		}
		
		set_transient( $transient_key, $output, apply_filters( 'siwi_flickr_widget_cache_timeout', 3600 ) );
		return $output;
	}

	function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['rss'] = strip_tags( $new_instance['rss'] );
		$instance['count'] = absint( $new_instance['count'] );
		return $new_instance;
	}

	
	function form( $instance ) {
		$defaults = array( 'title' => '', 'rss' => '', 'count' => 8);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'rss' ); ?>"><?php _e( 'Flickr RSS url:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'rss' ); ?>" name="<?php echo $this->get_field_name( 'rss' ); ?>" type="text" value="<?php echo esc_attr( $instance['rss'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'count' ); ?>"><?php _e( 'Number of photos:' ); ?></label><br />
			<input type="text" value="<?php echo esc_attr( $instance['count'] ); ?>" id="<?php echo $this->get_field_id( 'count' ); ?>" name="<?php echo $this->get_field_name( 'count' ); ?>" />
		</p>

		<?php
	}
}
?>