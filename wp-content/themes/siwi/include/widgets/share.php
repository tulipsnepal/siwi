<?php
/* CUSTOM SHARE WIDGET */
class Custom_Share_Widget extends WP_Widget {

	function Custom_Share_Widget() {
		$widget_ops = array( 'classname' => 'custom_share_widget', 'description' => __( "Custom Share widget." ) );
		$this->WP_Widget('custom_share_widget', __('SIWI Share Widget'), $widget_ops);
	}
	function widget($args, $instance) {
		extract($args);
		echo $before_widget;
	?>
	<!-- Lockerz Share BEGIN -->
	<div class="a2a_kit a2a_default_style">
	<span class="share_this">Share this page</span>

	<div class="sharez">
		<a class="icons facebook a2a_button_facebook"></a>
		<a class="icons email a2a_button_email"></a>
		<a class="icons twitter a2a_button_twitter"></a>
		<a class="icons share a2a_dd" href="http://www.addtoany.com/share_save"></a>
	</div>

	</div>
	<script type="text/javascript" src="http://static.addtoany.com/menu/page.js"></script>
	<!-- Lockerz Share END -->
		<?php
		echo $after_widget;
	}
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		
		
		return $new_instance;
	}
	function form($instance) {
	
	} 
}
?>