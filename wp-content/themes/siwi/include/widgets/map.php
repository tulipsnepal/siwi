<?php
/* CUSTOM MAP WIDGET */
class Map_Widget extends WP_Widget {

	function Map_Widget() {
		$widget_ops = array( 'classname' => 'map_widget one_third', 'description' => __( 'Map widget with headline, text and link.' ) );
		$control_ops = array( 'id_base' => 'map_widget');
		$this->WP_Widget('map_widget', __('SIWI Map Widget'), $widget_ops, $control_ops);
	}
	
	
	function widget($args, $instance) {
		extract($args);
		
		echo $before_widget; ?>
		<div class="gmap" id="<?php echo $this->get_field_id( 'map_id' ) ?>" style="display: block;background: red;">
		</div>
		<?php
		echo $before_title . esc_attr($instance['headline']) . $after_title;
		echo '<p>'.esc_attr($instance['text']).'</p>';
		echo '<a href="'.esc_url($instance['link']).'" target="'.esc_attr($instance['linktarget']).'">Link</a>';
		echo $after_widget;
		$this->draw_map('map_'.$this->number,$this->get_field_id( 'map_id' ), $instance['map_center'], $instance['map_zoom'], false);
	}
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['headline'] = strip_tags($new_instance['headline']);
		$instance['text'] = strip_tags($new_instance['text']);
		$instance['link'] = esc_url($new_instance['link']);
		$instance['linktarget'] = esc_attr($new_instance['linktarget']);
		$instance['map_center'] = esc_attr($new_instance['map_center']);
		$instance['map_zoom'] = absint($new_instance['map_zoom']);
		return $instance;
	}
	
	function form($instance) { 
		$defaults = array( 
			'headline' => '', 
			'text' => '', 
			'linktarget' => '_self',
			'link' => 'http://', 
			'map_center' => '0,0',
			'map_zoom' => 5 
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>	
		<script type="text/javascript">
			//<![CDATA[
				jQuery(document).ready(function(){
					
					jQuery('#<?php echo $this->get_field_id( 'save_map' ); ?>').click(function(e){
					e.preventDefault();
					var latlong = new google.maps.LatLng();
					latlong = <?php echo 'map_'.$this->number; ?>.getCenter();
					var lat = latlong.lat();
					var long = latlong.lng();
					jQuery('#<?php echo $this->get_field_id( 'map_center' ); ?>').val(lat + ',' + long);
					jQuery('#<?php echo $this->get_field_id( 'map_zoom' ); ?>').val(<?php echo 'map_'.$this->number; ?>.getZoom());
					//alert("Current view set!");
					});
					
				});
				
				jQuery(document).ajaxSuccess(function(e, xhr, settings){
					jQuery('#<?php echo $this->get_field_id( 'save_map' ); ?>').click(function(e){
					e.preventDefault();
					var latlong = new google.maps.LatLng();
					latlong = <?php echo 'map_'.$this->number; ?>.getCenter();
					var lat = latlong.lat();
					var long = latlong.lng();
					jQuery('#<?php echo $this->get_field_id( 'map_center' ); ?>').val(lat + ',' + long);
					jQuery('#<?php echo $this->get_field_id( 'map_zoom' ); ?>').val(<?php echo 'map_'.$this->number; ?>.getZoom());
					//alert("Current view set!");
					});
				});
				
			//]]>   
		  </script>
		  <?php 
		  	$this->draw_map('map_'.$this->number,$this->get_field_id( 'map_id' ), $instance['map_center'], $instance['map_zoom'], true);
		  ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'headline' ); ?>"><?php _e('Headline:'); ?></label>
			<input id="<?php echo $this->get_field_id( 'headline' ); ?>" name="<?php echo $this->get_field_name( 'headline' ); ?>" value="<?php echo esc_attr($instance['headline']); ?>" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e('Text:'); ?></label>
			<textarea id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>"  class="widefat"><?php echo esc_attr($instance['text']); ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e('Link:'); ?></label>
			<input id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" value="<?php echo esc_attr($instance['link']); ?>" class="widefat" />
		</p>
			<label for="<?php echo $this->get_field_id('linktarget'); ?>"><?php _e('Target:'); ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name('linktarget'); ?>" id="<?php echo $this->get_field_id('linktarget'); ?>">
				<option value="_self"<?php selected( $instance['linktarget'], '_self' ); ?>><?php _e('Same Window'); ?></option>
				<option value="_blank"<?php selected( $instance['linktarget'], '_blank' ); ?>><?php _e('Open New Window'); ?></option>
			</select>
		</p>
		<p>
			<div id="<?php echo $this->get_field_id( 'map_id' ); ?>" style="width:200px; height:200px;">
			</div>
		</p>
		<p>
			<a id="<?php echo $this->get_field_id( 'save_map' ); ?>" class="button-secondary" href="#">Save map view</a>
		</p>
		<input type="hidden" id="<?php echo $this->get_field_id( 'map_center' ); ?>" name="<?php echo $this->get_field_name( 'map_center' ); ?>" value="<?php echo esc_attr($instance['map_center']); ?>"/>
		<input type="hidden" id="<?php echo $this->get_field_id( 'map_zoom' ); ?>" name="<?php echo $this->get_field_name( 'map_zoom' ); ?>" value="<?php echo esc_attr($instance['map_zoom']); ?>"/>
	
		
		<?php
	} 
	
	function draw_map($map_id, $element_id, $map_center, $map_zoom = 5, $controls = false){
		if($map_center){
			$map_center = explode(',', trim($map_center));
		} else {
			$map_center = array(0,0);
		}
	 	
	$zoomcontrol = $controls ? '' : ', zoomControl: false';
	echo '<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=true"></script>';
	echo '<script type="text/javascript">
	var '.$map_id.';
	  function initialize_'.$map_id.'() {
    var latlng = new google.maps.LatLng('.$map_center[0].', '.$map_center[1].');
    var myOptions = {
      zoom: '.$map_zoom.',
      center: latlng,
      mapTypeId: google.maps.MapTypeId.TERRAIN,
      mapTypeControl: false,
      streetViewControl: false'.
	  	$zoomcontrol.',
	  overviewMapControl: false,
	  overviewMapControlOptions: {opened: true}
    };
    '.$map_id.' = new google.maps.Map(document.getElementById("'.$element_id.'"), myOptions);
    ';  
	echo 'return '.$map_id.'; } jQuery(document).ready(function(){ initialize_'.$map_id.'(); }); jQuery(document).ajaxSuccess(function(e, xhr, settings){ initialize_'.$map_id.'(); });</script>';
	} 
}
		  
?>