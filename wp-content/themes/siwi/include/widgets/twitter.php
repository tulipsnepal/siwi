<?php
/* CUSTOM SHARE WIDGET */
class Custom_Twitter_Widget extends WP_Widget {

	function Custom_Twitter_Widget() {
		$widget_ops = array( 'classname' => 'custom_twitter_widget one_third col-xs-12 col-sm-12 col-md-4', 'description' => __( "Custom Twitter widget." ) );
		$this->WP_Widget('custom_twitter_widget', __('SIWI Twitter Widget'), $widget_ops);
	}

	function widget($args, $instance) {
		extract($args);
			echo $before_widget;		
		?>
		<h3 class="hidden">Twitter<a href="http://twitter.com/<?php echo $instance["url"]; ?>">Follow @<?php echo $instance["url"]; ?></a></h3>

<a class="twitter-timeline"  href="https://twitter.com/siwi_media" height="260" data-widget-id="430338241867161600">Tweets by @siwi_media</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>



		<div class="under_widget hidden">
		<h3>We’re on: </h3>
		<a href="<?php echo $instance['facebook']; ?>" title="Facebook"><i class="icon-facebook"></i></a>
		<a href="http://twitter.com/<?php echo $instance['url']; ?>" title="Twitter"><i class="icon-twitter"></i></a>
		<a href="<?php echo $instance['youtube']; ?>" title="Youtube"><i class="icon-youtube"></i></a>
		<a href="<?php echo $instance['flickr']; ?>" title="Flickr"><i class="icon-flickr"></i></a>
		<span>- Follow us!</span>
		</div>

		<?php

		echo $after_widget;
	}


	
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		return $new_instance;
	}	
	
	function form($instance) {
		echo '<div id="Puff-admin-panel">';
		echo '<p><label for="' . $this->get_field_id("url") .'">Twitter username:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("url") . '" '; 
		echo 'id="' . $this->get_field_id("url") . '" ';
		echo 'value="' . $instance["url"] . '"></p>';
		echo '<p><label for="' . $this->get_field_id("facebook") .'">Facebook link:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("facebook") . '" '; 
		echo 'id="' . $this->get_field_id("facebook") . '" ';
		echo 'value="' . $instance["facebook"] . '"></p>';
		echo '<p><label for="' . $this->get_field_id("youtube") .'">Youtube link:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("youtube") . '" '; 
		echo 'id="' . $this->get_field_id("youtube") . '" ';
		echo 'value="' . $instance["youtube"] . '"></p>';
		echo '<p><label for="' . $this->get_field_id("flickr") .'">Flickr link:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("flickr") . '" '; 
		echo 'id="' . $this->get_field_id("flickr") . '" ';
		echo 'value="' . $instance["flickr"] . '"></p>';
		echo '</div>';
	} 
}
?>