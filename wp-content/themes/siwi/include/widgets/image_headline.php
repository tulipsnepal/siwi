<?php
/* CUSTOM IMAGE WITH HEADLINE WIDGET */
class Image_Headline_Widget extends WP_Widget {
	function Image_Headline_Widget() {
		$widget_ops = array( 'classname' => 'widget_sp_image one_third borderwhite col-xs-12 col-sm-6 col-md-4', 'description' => __( 'Widget with image, headline and custom link.') );
		$control_ops = array( 'id_base' => 'widget_sp_image' );
		$this->WP_Widget('widget_sp_image', __('SIWI Image Headline Widget'), $widget_ops, $control_ops);
		add_action( 'admin_init', array( $this, 'admin_setup' ) );
	}
	function admin_setup() {
		global $pagenow;
		if ( 'widgets.php' == $pagenow ) {
			wp_enqueue_style( 'thickbox' );
			wp_enqueue_script( 'siwi-image-widget', JS_URI.'/image-widget.js', array('thickbox'), false, true );
			add_action( 'admin_head-widgets.php', array( $this, 'admin_head' ) );
		}
		elseif ( 'media-upload.php' == $pagenow || 'async-upload.php' == $pagenow ) {
			wp_enqueue_script( 'siwi-image-widget-fix-uploader', JS_URI.'/image-widget-upload-fixer.js', array('jquery'), false, true );
			add_filter( 'image_send_to_editor', array( $this,'image_send_to_editor'), 1, 8 );
			add_filter( 'gettext', array( $this, 'replace_text_in_thickbox' ), 1, 3 );
			add_filter( 'media_upload_tabs', array( $this, 'media_upload_tabs' ) );
			add_filter( 'image_widget_image_url', array( $this, 'https_cleanup' ) );
			add_filter('image_size_names_choose',  array( $this,'widget_image_size'));
		}
		$this->fix_async_upload_image();
	}
	function fix_async_upload_image() {
		if(isset($_REQUEST['attachment_id'])) {
			$id = (int) $_REQUEST['attachment_id'];
			$GLOBALS['post'] = get_post( $id );
		}
	}
	function get_image_url( $id, $size = false) {
		/**/
		// Get attachment and resize but return attachment path (needs to return url)
		$img_url = wp_get_attachment_image_src($id, $size);
		return (!empty($img_url) && isset($img_url[0])) ? $img_url[0] : wp_get_attachment_url( $id );
		
	}
	function is_sp_widget_context() {
		if ( isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'],$this->id_base) !== false ) {
			return true;
		} elseif ( isset($_REQUEST['_wp_http_referer']) && strpos($_REQUEST['_wp_http_referer'],$this->id_base) !== false ) {
			return true;
		} elseif ( isset($_REQUEST['widget_id']) && strpos($_REQUEST['widget_id'],$this->id_base) !== false ) {
			return true;
		}
		return false;
	}
	
	function replace_text_in_thickbox($translated_text, $source_text, $domain) {
		if ( $this->is_sp_widget_context() ) {
			if ('Insert into Post' == $source_text) {
				return __('Insert Into Widget');
			}
		}
		return $translated_text;
	}
	
	function image_send_to_editor( $html, $id, $caption, $title, $align, $url, $size, $alt = '' ) {
		// Normally, media uploader return an HTML string (in this case, typically a complete image tag surrounded by a caption).
		// Don't change that; instead, send custom javascript variables back to opener.
		// Check that this is for the widget. Shouldn't hurt anything if it runs, but let's do it needlessly.
		if ( $this->is_sp_widget_context() ) {
			if ($alt=='') $alt = $title;
			?>
			<script type="text/javascript">
	// send image variables back to opener
	var win = window.dialogArguments || opener || parent || top;
	win.IW_html = '<?php echo addslashes($html); ?>';
	win.IW_img_id = '<?php echo $id; ?>';
	win.IW_alt = '<?php echo addslashes($alt); ?>';
	win.IW_caption = '<?php echo addslashes($caption); ?>';
	win.IW_title = '<?php echo addslashes($title); ?>';
	win.IW_align = '<?php echo esc_attr($align); ?>';
	win.IW_url = '<?php echo esc_url($url); ?>';
	win.IW_size = '<?php echo esc_attr($size); ?>';
</script>
<?php
}
return $html;
}

function media_upload_tabs($tabs) {
	if ( $this->is_sp_widget_context() ) {
		unset($tabs['type_url']);
	}
	return $tabs;
}
function widget( $args, $instance ) {
	extract( $args );
	extract( $instance );
	if ( !empty( $imageurl ) ) {
		$headline = apply_filters( 'widget_title', empty( $headline ) ? '' : $headline );
		$description = apply_filters( 'widget_text', $description, $args, $instance );
		$imageurl = apply_filters( 'image_widget_image_url', esc_url( $imageurl ), $args, $instance );
		if ( $link ) {
			$link = apply_filters( 'image_widget_image_link', esc_url( $link ), $args, $instance );
			$linktarget = apply_filters( 'image_widget_image_link_target', esc_attr( $linktarget ), $args, $instance );
		}
		echo $before_widget;
		if ( !empty( $imageurl ) ) {
			if ( $link ) {
				// echo '<a class="'.$this->widget_options['classname'].'-image-link" href="'.$link.'" target="'.$linktarget.'">';
				echo '<a class="fullwidth fleft" href="'.$link.'" target="'.$linktarget.'">';
			}
			if ( $imageurl ) {
				echo '<figure class="news_thumb"><img src="'.$imageurl.'" alt="'.$headline.'" class="img-responsive" /></figure>';
			}
			if ( !empty( $headline ) ) { echo $before_title . $headline . $after_title; }
			if ( $link ) { echo '</a>'; }
		}
		echo $after_widget;
	}
}
function update( $new_instance, $old_instance ) {
	$instance = $old_instance;
	$instance['headline'] = strip_tags($new_instance['headline']);
	$instance['link'] = esc_url($new_instance['link']);
	$instance['image'] = $new_instance['image'];
	$instance['imageurl'] = $this->get_image_url($new_instance['image'],$new_instance['imagesize']);
	$instance['imagesize'] = $new_instance['imagesize'];
	$instance['linktarget'] = $new_instance['linktarget'];
	return $instance;
}
function form( $instance ) {
	$instance = wp_parse_args( (array) $instance, array(
		'headline' => '',
		'link' => '',
		'linktarget' => '',
		'image' => '',
		'imageurl' => '',
		'imagesize' => ''
		) ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('headline'); ?>"><?php _e('Headline:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('headline'); ?>" name="<?php echo $this->get_field_name('headline'); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['headline'])); ?>" />
		</p>
		<p><label for="<?php echo $this->get_field_id('image'); ?>"><?php _e('Image:'); ?></label>
			<?php
	$media_upload_iframe_src = "media-upload.php?type=image&post_id=0&widget_id=".$this->id; //NOTE #1: the widget id is added here to allow uploader to only return array if this is used with image widget so that all other uploads are not harmed.
	$image_upload_iframe_src = apply_filters('image_upload_iframe_src', "$media_upload_iframe_src");
	$image_title = __(($instance['image'] ? 'Change Image' : 'Add Image'));
	?><br />
	<a href="<?php echo $image_upload_iframe_src; ?>&TB_iframe=true" id="add_image-<?php echo $this->get_field_id('image'); ?>" class="thickbox-image-widget" title='<?php echo $image_title; ?>' onClick="imageWidget.setActiveWidget('<?php echo $this->id; ?>');return false;" style="text-decoration:none"><img src='images/media-button-image.gif' alt='<?php echo $image_title; ?>' align="absmiddle" /> <?php echo $image_title; ?></a>
	<div id="display-<?php echo $this->get_field_id('image'); ?>"><?php
		if ($instance['imageurl']) {
			echo '<img src="'.$instance['imageurl'].'" alt="'.$instance['headline'].'" style="width:100%" />';
		}


		?></div>
		<br />
		<input id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>" type="hidden" value="<?php echo $instance['image']; ?>" />
	</p>

	<p>
		<label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Link:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo esc_attr(strip_tags($instance['link'])); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id('linktarget'); ?>"><?php _e('Target:'); ?></label>
		<select class="widefat" name="<?php echo $this->get_field_name('linktarget'); ?>" id="<?php echo $this->get_field_id('linktarget'); ?>">
			<option value="_self"<?php selected( $instance['linktarget'], '_self' ); ?>><?php _e('Same Window'); ?></option>
			<option value="_blank"<?php selected( $instance['linktarget'], '_blank' ); ?>><?php _e('Open New Window'); ?></option>
		</select>
	</p>
	<input id="<?php echo $this->get_field_id('imagesize'); ?>" name="<?php echo $this->get_field_name('imagesize'); ?>" type="hidden" value="<?php echo esc_attr(strip_tags($instance['imagesize'])); ?>" />
	<?php
}
function admin_head() {
	?>
	<style type="text/css">
		.aligncenter {
			display: block;
			margin-left: auto;
			margin-right: auto;
		}
	</style>
	<?php
}
function https_cleanup( $imageurl = '' ) {
	if( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ) {
		$imageurl = str_replace('http://', 'https://', $imageurl);
	} else {
		$imageurl = str_replace('https://', 'http://', $imageurl);
	}
	return $imageurl;
}

function widget_image_size($sizes) {
	$sizes['post-thumbnail'] = __( 'Widget Image');
	return $sizes;
}

}
?>