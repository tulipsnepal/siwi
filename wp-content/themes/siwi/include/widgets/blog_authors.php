<?php
/* CUSTOM SHARE WIDGET */
class Blog_Authors_Widget extends WP_Widget {

	function Blog_Authors_Widget() {
		$widget_ops = array( 'classname' => 'blog_authors_widget', 'description' => __( "SIWI Blog Authors" ) );
		$this->WP_Widget('siwi_blog_authors', __('SIWI Blog Authors'), $widget_ops);
	}

	function widget($args, $instance) {
		extract($args);
			echo $before_widget;	
			echo $before_title . '<a href="'.$instance['title_link'].'">'.$instance['title'].'</a>'.$after_title;
		?>
		<h3 class="cat_title"><?php echo $instance['cat_title']; ?></h3>
		<ul class="blog_categories">
			<?php wp_list_categories(array('title_li' => '')); ?>
		</ul>
		<?php

		echo $after_widget;
	}


	
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		return $new_instance;
	}	
	
	function form($instance) {
		echo '<p><label for="' . $this->get_field_id("title") .'">Title:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("title") . '" '; 
		echo 'id="' . $this->get_field_id("title") . '" ';
		echo 'value="' . $instance["title"] . '"></p>';
		echo '<p><label for="' . $this->get_field_id("title_link") .'">Title link:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("title_link") . '" '; 
		echo 'id="' . $this->get_field_id("title_link") . '" ';
		echo 'value="' . $instance["title_link"] . '"></p>';
		echo '<p><label for="' . $this->get_field_id("cat_title") .'">Categories title:</label>';
		echo '<input type="text" class="widefat" ';
		echo 'name="' . $this->get_field_name("cat_title") . '" '; 
		echo 'id="' . $this->get_field_id("cat_title") . '" ';
		echo 'value="' . $instance["cat_title"] . '"></p>';
	} 
}
?>