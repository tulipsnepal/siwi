<?php



/* CUSTOM POST TYPE - PUBLICATION */

function create_post_type_publication() {



  $labels = array(

              'name' => __( 'Publications'),

              'singular_name' => __( 'Publication'),

              'add_new' => __('Add New'),

              'add_new_item' => __('Add New Publication'),

              'edit_item' => __('Edit Publication'),

              'new_item' => __('New Publication'),

              'view_item' => __('View Publication'),

              'search_items' => __('Search Publication'),

              'not_found' => __('No publications found'),

              'not_found_in_trash' => __('No publications found in Trash'),

              'parent_item_colon' => ''

            );



  $args = array(

            'labels' => $labels,

            'public' => true,

            'show_ui' => true,

            'query_var' => true,

            'can_export' => true,

            'rewrite' => array('slug'=>'publication'),

            'capability_type' => 'post',

            'hierarchical' => false,

            'menu_position' => 34,

            // 'has_archive' => 'publications',

            'supports' => array('title','editor','thumbnail','page-attributes'),

            // 'taxonomies' => array('post_tag'),

            'register_meta_box_cb' => 'add_publication_metaboxes'

          );



  register_post_type( 'publication' , $args );

  /*register_taxonomy( 'publication_category', 'publication', array( 'hierarchical' => true, 'label' => __('Publication Categories'), 'query_var' => 'publication_category' ) );*/



}



add_action('init', 'create_post_type_publication');







function add_publication_metaboxes() {

  add_meta_box(

    'download_link',

    __('Download Link (PDF)'),

    'download_link_box',

    'publication',

    'normal',

    'default'

  );

}







function download_link_box($object, $box) {

  wp_nonce_field( plugin_basename( __FILE__ ), 'publication_nonce' );

  $download_link = get_post_meta($object->ID,'_download_link',true);

  echo '<p><input type="text" id="download_link" name="download_link" value="'.esc_url($download_link).'" style="width:100%;" /></p>';

}



add_action( 'save_post', 'save_publication_postdata', 10 ,2);



function save_publication_postdata($post_id, $post ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )

    return;

	if(isset($_POST['publication_nonce'])){

  	if ( !wp_verify_nonce( $_POST['publication_nonce'], plugin_basename( __FILE__ ) ) )

    	return;

  }



  if($post->post_type == 'publication') {

  	$post_type = get_post_type_object( $post->post_type );

  	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )

    return $post_id;



  	$download_link = isset($_POST['download_link']) ? esc_url($_POST['download_link']) : '';



  	update_post_meta($post_id, '_download_link', $download_link);

	}

}



?>