<?php

/*-----------------------------------------------------------------------------------*/

/*	Create Post Type - Contact

/*-----------------------------------------------------------------------------------*/



add_action('init', 'siwi_create_post_type_contact');



function siwi_create_post_type_contact() {



  $labels = array(

              'name' => __( 'Contacts', THEME_SLUG ),

              'singular_name' => __( 'Contact', THEME_SLUG ),

              'add_new' => __('Add New', THEME_SLUG ),

              'add_new_item' => __('Add New Contact Item', THEME_SLUG ),

              'edit_item' => __('Edit Contact Item', THEME_SLUG ),

              'new_item' => __('New Contact Item', THEME_SLUG ),

              'view_item' => __('View Contact Item', THEME_SLUG ),

              'search_items' => __('Search Contact', THEME_SLUG ),

              'not_found' => __('No Items Found', THEME_SLUG ),

              'not_found_in_trash' => __('No Items Found In Trash',THEME_SLUG),

              'parent_item_colon' => '',

            );



  $args = array(

            'labels' => $labels,

            'public' => true,

            'show_ui' => true,

            'query_var' => 'siwi_contact',

            'can_export' => true,

            'capability_type' => 'post',

            'hierarchical' => false,

            'menu_position' => 34,

            'show_in_nav_menus' => true,

            'supports' => array('title','thumbnail'),

            'register_meta_box_cb' => 'siwi_contact_metaboxes'

          );



		/* Register Contact */

  	register_post_type( 'siwi_contact' , $args);



}





/*-----------------------------------------------------------------------------------*/

/*	Create Contact Metaboxes

/*-----------------------------------------------------------------------------------*/



if(!function_exists('siwi_contact_metaboxes')) :

	function siwi_contact_metaboxes() {



	  /* Custom URL metabox */

	  add_meta_box(

	    'siwi_contact_details',

	    __('Details',THEME_SLUG),

	    'siwi_contact_details_metabox',

	    'siwi_contact',

	    'normal',

	    'default'

	  );





	}

endif;



/* Create Contact Subtitle Metabox */



if(!function_exists('siwi_contact_details_metabox')) :

	function siwi_contact_details_metabox($object, $box) {

	   wp_nonce_field( __FILE__ , 'siwi_contact_nonce' );

	   $defaults = array('phone' => '', 'company' => '', 'position' => '', 'email' => '');

	   $siwi_meta = get_post_meta($object->ID,'_siwi_meta',true);

	   $siwi_meta = wp_parse_args( (array) $siwi_meta, $defaults );

	  ?>

	  <p>

	  	<label><?php _e('Phone',THEME_SLUG); ?>:</label>

	  	<input type="text" name="siwi[phone]" class="widefat" value="<?php echo $siwi_meta['phone']; ?>"/>

		</p>

		<p>

	  	<label><?php _e('Company',THEME_SLUG); ?>:</label>

	  	<input type="text" name="siwi[company]" class="widefat" value="<?php echo $siwi_meta['company']; ?>"/>

		</p>

		<p>

	  	<label><?php _e('Position',THEME_SLUG); ?>:</label>

	  	<input type="text" name="siwi[position]" class="widefat" value="<?php echo $siwi_meta['position']; ?>"/>

		</p>

		<p>

	  	<label><?php _e('Email',THEME_SLUG); ?>:</label>

	  	<input type="text" name="siwi[email]" class="widefat" value="<?php echo $siwi_meta['email']; ?>"/>

		</p>



	  <?php

	}

endif;









/* Save Contact Post Meta */



add_action( 'save_post', 'siwi_save_contact_postmeta', 10 , 2);



	function siwi_save_contact_postmeta($post_id, $post ) {

	  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )

	    return;



		if(isset($_POST['siwi_contact_nonce'])){

	  	if ( !wp_verify_nonce( $_POST['siwi_contact_nonce'], __FILE__  ) )

	    	return;

	  }



	  if($post->post_type == 'siwi_contact' && isset($_POST['siwi'])) {

	  	$post_type = get_post_type_object( $post->post_type );

	  	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )

	    return $post_id;



	  	$email = isset($_POST['siwi']['email']) ? strip_tags($_POST['siwi']['email']) : '';

	  	$phone = isset($_POST['siwi']['phone']) ? strip_tags($_POST['siwi']['phone']) : '';

	  	$company = isset($_POST['siwi']['company']) ? strip_tags($_POST['siwi']['company']) : '';

	  	$position = isset($_POST['siwi']['position']) ? strip_tags($_POST['siwi']['position']) : '';



			$siwi_meta = array(

				'phone' => $phone,

				'company' => $company,

				'position' => $position,

				'email' => $email

			);



	  	update_post_meta($post_id, '_siwi_meta', $siwi_meta);

		}

}



?>