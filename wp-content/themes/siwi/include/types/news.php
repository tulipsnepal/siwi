<?php
/* CUSTOM POST TYPE - NEWS */
function create_post_type_news() {
  $labels = array(
    'name' => __( 'News'),
    'singular_name' => __( 'News'),
    'add_new' => __('Add New'),
    'add_new_item' => __('Add New'),
    'edit_item' => __('Edit News'),
    'new_item' => __('New'),
    'view_item' => __('View News'),
    'search_items' => __('Search News'),
    'not_found' => __('No news found'),
    'not_found_in_trash' => __('No news found in Trash'),
    'parent_item_colon' => ''
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'show_ui' => true,
    'query_var' => true,
    'can_export' => true,
    // 'has_archive' => 'news',
    'rewrite' => array('slug'=>'news','with_front' => true),
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 32,
    'taxonomies' => array('post_tag'),
    'supports' => array('title','editor','thumbnail','page-attributes', 'excerpt')
    );
  register_post_type( 'news' , $args );

  register_taxonomy( 'news_category', 'news', array( 'hierarchical' => true, 'label' => __('News Categories'), 'query_var' => 'news_category' ) );
}
add_action('init', 'create_post_type_news');
?>