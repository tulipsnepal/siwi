<?php



/* CUSTOM POST TYPE - PROJECT */

function create_post_type_project() {



  $labels = array(

              'name' => __( 'Projects'),

              'singular_name' => __( 'Project'),

              'add_new' => __('Add New'),

              'add_new_item' => __('Add New Project'),

              'edit_item' => __('Edit Project'),

              'new_item' => __('New Project'),

              'view_item' => __('View Project'),

              'search_items' => __('Search Project'),

              'not_found' => __('No projects found'),

              'not_found_in_trash' => __('No projects found in Trash'),

              'parent_item_colon' => ''

            );



  $args = array(

            'labels' => $labels,

            'public' => true,

            'show_ui' => true,

            'query_var' => true,

            'can_export' => true,

            'rewrite' => array('slug'=>'project'),

            // 'has_archive' => 'projects',

            'capability_type' => 'post',

            'hierarchical' => false,

            'menu_position' => 33,

            'taxonomies' => array('post_tag'),

            'supports' => array('title','editor','thumbnail','page-attributes')

          );



  register_post_type( 'project' , $args );

  register_taxonomy( 'project_category', 'project', array( 'hierarchical' => true, 'label' => __('Project Categories'), 'query_var' => 'project_category' ) );



}



add_action('init', 'create_post_type_project');



?>