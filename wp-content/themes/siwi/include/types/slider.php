<?php

/* CUSTOM POST TYPE - SLIDER */
function create_post_type_slider() {

  $labels = array(
              'name' => __( 'Slider'),
              'singular_name' => __( 'Slider'),
              'add_new' => __('Add New'),
              'add_new_item' => __('Add New Slider'),
              'edit_item' => __('Edit Slider'),
              'new_item' => __('New Slider'),
              'view_item' => __('View Slider'),
              'search_items' => __('Search Slider'),
              'not_found' => __('No sliders found'),
              'not_found_in_trash' => __('No sliders found in Trash'),
              'parent_item_colon' => ''
            );

  $args = array(
            'labels' => $labels,
            'public' => true,
            'show_ui' => true,
            'query_var' => false,
            'can_export' => true,
            'rewrite' => array('slug'=>'slider'),
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => 31,
            'supports' => array('title','editor','thumbnail','page-attributes'),
            'register_meta_box_cb' => 'add_slider_metaboxes'
          );

  register_post_type( 'slider' , $args );
}

add_action('init', 'create_post_type_slider');



function add_slider_metaboxes() {
  add_meta_box(
    'slider_link',
    __('Link'),
    'slider_link_box',
    'slider',
    'normal',
    'default'
  );
}



function slider_link_box($object, $box) {
  wp_nonce_field( plugin_basename( __FILE__ ), 'slider_nonce' );
  $slider_link = get_post_meta($object->ID,'_slider_link',true);
  echo '<p><input type="text" id="slider_link" name="slider_link" value="'.esc_url($slider_link).'" style="width:100%;" /></p>';
}

add_action( 'save_post', 'save_slider_postdata', 10 ,2);

function save_slider_postdata($post_id, $post ) {
  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    return;
	if(isset($_POST['slider_nonce'])){
  	if ( !wp_verify_nonce( $_POST['slider_nonce'], plugin_basename( __FILE__ ) ) )
    	return;
  }
  
  if($post->post_type == 'slider') {
  	$post_type = get_post_type_object( $post->post_type );

  	if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  	$slider_link = isset($_POST['slider_link']) ? esc_url($_POST['slider_link']) : '';

  	update_post_meta($post_id, '_slider_link', $slider_link);
	}
}

function get_slider_data() {
  $args = array(
            'post_type' => 'slider',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'post_status' => 'publish',
            'showposts' => -1
          );

  $items = query_posts($args);
  if (!empty($items)) {
    foreach($items as $ind => $item) {
      $slider_link = get_post_meta($item->ID,'_slider_link',true);
      $items[$ind]->link_href = $slider_link;
      $image = array();
      if (has_post_thumbnail( $item->ID ) ) {
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'slider-img' );
      }
      $items[$ind]->img_src = isset($image[0]) ? $image[0] : '';
    }
  }
  return $items;
}

?>