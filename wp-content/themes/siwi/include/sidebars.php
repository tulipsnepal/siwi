<?php
 // REGISTER SIDEBARS 
add_action( 'widgets_init', 'siwi_register_sidebars' );
function siwi_register_sidebars() {

	if(function_exists('register_sidebar')){

		register_sidebar(
			array(
				'id' => 'home_primary',
				'name' => __( 'Home Primary Sidebar' ),
				'description' => __( 'Home page first row sidebar.' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<aside class="box_inside withimage"><h5 class="widget-title">',
				'after_title' => '</h5></aside>'
				)
			);

		register_sidebar(
			array(
				'id' => 'home_secondary',
				'name' => __( 'Home Secondary Sidebar' ),
				'description' => __( 'Home page second row sidebar.' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<aside class="box_inside withimage"><h5 class="widget-title">',
				'after_title' => '</h5></aside>'
				)
			);
		register_sidebar(
			array(
				'id' => 'single_post',
				'name' => __( 'Single Post Sidebar' ),
				'description' => __( 'This is single post sidebar.' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);
		register_sidebar(
			array(
				'id' => 'single_page',
				'name' => __( 'Single Page Sidebar' ),
				'description' => __( 'This is single page sidebar.' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);

		register_sidebar(
			array(
				'id' => 'archive',
				'name' => __( 'Archive Sidebar' ),
				'description' => __( 'This is archive sidebar.'),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);
		register_sidebar(
			array(
				'id' => 'projects',
				'name' => __( 'SIWI Projects' ),
				'description' => __( 'This is projects sidebar.'),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);
		register_sidebar(
			array(
				'id' => 'publications',
				'name' => __( 'SIWI Publications' ),
				'description' => __( 'This is publications sidebar.'),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);
		register_sidebar(
			array(
				'id' => 'all-authors',
				'name' => __( 'Blog right sidebar' ),
				'description' => __( 'Right sidebar on "All authors" page'),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);
		register_sidebar(
			array(
				'id' => 'blog-left',
				'name' => __( 'Blog left sidebar' ),
				'description' => __( 'Left sidebar on all blog and post pages'),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
				)
			);

	}
}
?>