<form id="siwi_shortcode_projects">
	<table class="form-table">
		<tbody>
			<tr>
		 		<th><h3><?php _e('Project Options',THEME_SLUG); ?></h3></th><td>&nbsp;</td>
			</tr>
			<tr>
				<th>
					<?php _e('Title',THEME_SLUG); ?>:
				</th>
				<td><input type="text" name="title" value="<?php _e('My Project',THEME_SLUG); ?>" class="widefat"/></td>
		</tr>
		 <tr>
		 	<th><?php _e('Content',THEME_SLUG); ?>:</th>
		 	<td><textarea id="content_text" type="text" name="content" value="" class="widefat" style="height:250px;" /></td>
		 </tr>
		 <tr>
				<th><input type="submit" class="button-primary" value="<?php _e('Insert Project Box',THEME_SLUG); ?>" class="widefat"></th> 
				<td>&nbsp;</td>
		</tr>
	</tbody>
	</table>
</form>

<script type="text/javascript">
	/* <![CDATA[ */
  (function($) {
    	$('#siwi_shortcode_projects').submit(function(e) {
    			e.preventDefault();
    			tb_remove();
    			var title = $(this).find('input[name="title"]').val();
    			var project_content = $(this).find('textarea[name="content"]').val();
 				txt = '<p>' + project_content.split('\n').join('</p><p>') + '</p>';
    			var content = '[project title="'+title+'"]'+txt+'[/project]';
    			siwi_shortcode.setContent(content);
			});

	})(jQuery);
	/* ]]> */
</script>