<?php 
/*-----------------------------------------------------------------------------------*/
/*	Theme Shortcodes Handler
/*-----------------------------------------------------------------------------------*/
	 
	 /* Register Shortcodes */
	 
	 add_action('init', 'siwi_register_shortcodes' );

   function siwi_register_shortcodes(){
		  
			    add_shortcode( 'project', 'siwi_project_sc');
		}
		 
		/* Column shortcodes */
		function siwi_project_sc( $atts, $content = '', $tag ) {
		    extract( shortcode_atts(  array('title' => ''), $atts ) );
		    $output =  '';
		    if(!empty($title)){
		    	$output = '<div class="titles page projects"><h3 class="title_lined smaller">'.strip_tags($title).'</h3><div class="title_line smaller"></div></div>';
		    }
		    
		   	$output .= '<div class="project_box"><div class="project_box_inside">' . do_shortcode( $content ) . '</div></div>';
		   	
		    return $output;
		}
		
	/* Add shortcodes UI */
	
	add_action( 'admin_init', 'siwi_shortcodes');
    
  function siwi_shortcodes() {
   	
        if ( current_user_can( 'edit_posts' ) && current_user_can( 'edit_pages' ) ) {
            add_filter( 'mce_buttons', 'siwi_register_shortcode_buttons' );
            add_filter( 'mce_external_plugins', 'siwi_register_shortcode_plugin');
        }
   }
   
   /* Register shortcodes button */
   function siwi_register_shortcode_buttons( $buttons ) {
    	  array_push( $buttons, 'siwi_shortcodes_button');
        return $buttons;
   }
   
   /* Register shortcodes plugin */ 
   function siwi_register_shortcode_plugin( $plugins ) {
        $plugins['siwi_shortcodes'] = JS_URI . '/shortcodes.js';
        return $plugins;
   }
  
  /* Generate shortcodes UI */
  add_action('wp_ajax_siwi_generate_shortcodes_ui', 'siwi_generate_shortcodes_ui');
  
  function siwi_generate_shortcodes_ui(){
  
  $sections = array(
  	array('name' => 'projects', 'title' => __('Project Box',THEME_SLUG))
  ); ?>
  

  <div id="siwi_tabs">
  	<ul>
  	<?php foreach($sections as $section) : ?>
			<li><a data-nav="<?php echo $section['name'];?>" href="#"><?php echo $section['title'];?></a></li>
  	<?php endforeach; ?>
  	</ul>
  </div>
  
  <div id="siwi_tabs_sections">
  <?php foreach($sections as $section) : ?>
		<div id="tabs-<?php echo $section['name'];?>" class="hidable wrap" style="display: none;">
		<?php include(THEME_DIR.'/include/shortcodes/template_'.$section['name'].'.php'); ?>
		</div>
  <?php endforeach; ?>
	</div>
	
	<script type="text/javascript">
	/* <![CDATA[ */
	(function($) {
	    	$('#siwi_tabs a').click(function(e) {
	    		e.preventDefault();
	    		siwi_tabs_switch($(this));
			 });
			 
			 siwi_tabs_switch($('#siwi_tabs a').first());
			 
			 function siwi_tabs_switch(obj){
			 	$('#siwi_tabs_sections .hidable').hide();
	    	$('#siwi_tabs_sections #tabs-'+ obj.attr('data-nav')).show();
	    	$('#siwi_tabs li').removeClass('current');
	    	obj.parent().addClass('current');
	     }
	     
	})(jQuery);
	/* ]]> */
	</script>
  <?php die();
}

  
?>