<?php
/**
* Template Name: Search Template
*
* @package SIWI
*/
?>
<?php
get_header();
$params = array();
$params['s']    =   trim($_GET['s']);
// $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
// $params['paged'] = $paged;
/*$custom_query = new WP_Query($params);
$total_results = $custom_query->found_posts;
$posts = $custom_query->posts;*/
?>
<main id="mainblock" role="main" class="news-page newslistpage searchlist content-wrapper">
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<p class="h3" style="color:#999; font-weight: 400; display: inline-block; margin-right: 15px;">Search Results for</p>
			<h1 class="h1" style="display:inline-block">
			<?php the_search_query(); ?>
			</h1>
		</div>
	</div>
	<?php if (!empty($posts)): ?>
	
	<div class="row news-list">
		<div class="col-md-9 col-sm-7 col-xs-12 ">
			<?php while ( have_posts() ) { the_post();?>
			<div class="news_repeater fleft">
				<a href="<?php echo the_permalink(); ?>">
					<figure class="news_thumb">
						<?php if (has_post_thumbnail()): ?>
						<?php the_post_thumbnail('post-img',array('class'=>'img-responsive')); ?>
						<?php endif ?>
					</figure>
					<aside class="withimage">
						<h2><?php echo the_title(); ?></h2>
						<?php echo wp_trim_words( get_the_content(), 20 ); ?>
					</aside>
				</a>
				
			</div>
			<?php } ?>
		</div>
		</div>
		<div class="row search-paginate">
			<div class="col-xs-12 " id="paginationWrapper">
				<nav class="fleft fullwidth" data-paged="1">
					<?php if(function_exists('wp_pagenavi')) {
					wp_pagenavi();
					} ?>
				</nav>
			</div>
		</div>
	<div class="clear"></div>
	<?php else: ?>
	<h1>No Results Found.</h1>
	<?php endif ?>
</div>
</main>
<?php get_footer(); ?>