<?php

add_action("wp_ajax_get_filtered_resources", "get_filtered_resources");
add_action("wp_ajax_nopriv_get_filtered_resources", "get_filtered_resources");

function get_filtered_resources() {

  global $wpdb;

  $filter = $_REQUEST['filter'];

  $params = array();

  if ($filter['date']=='desc') {
    $params = array(
      'post_type'     =>  'publication',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('publications_limit','option'),
      'meta_key' => 'date_of_report_publication', //name of custom field
      'orderby' => 'meta_value_num',
      'order' => 'desc'
    );
  }else{
    $params = array(
      'post_type'     =>  'publication',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('publications_limit','option'),
      'meta_key'      => 'short_headline',
      'orderby'     => 'meta_value',
      'order'       => 'asc'
    );
  }

  if($filter['paged']){
   global $paged;
    $params['paged'] = $filter['paged'];
    $paged = $filter['paged'];
  }

  $tax_query = array();

    if (!empty($filter['focus_area'])) {
    $tax_query[] = array(
        'taxonomy' => 'topic_categories',
              'field' => 'id',
              'terms' => $filter['focus_area']
        );
    }

    if (!empty($filter['region'])) {
      $tax_query[] = array(
          'taxonomy' => 'region_categories',
              'field' => 'id',
              'terms' => $filter['region']
        );
    }

    if (!empty($filter['language'])) {
      $tax_query[] = array(
          'taxonomy' => 'resource_language_categories',
              'field' => 'id',
              'terms' => $filter['language']
        );
    }

    if (!empty($filter['type'])) {
      $tax_query[] = array(
          'taxonomy' => 'resource_type_categories',
              'field' => 'id',
              'terms' => $filter['type']
       );
    }

    if (!empty($filter['keyword'])) {
        $tax_query[] = array(
              'taxonomy' => 'keyword_categories', //or tag or custom taxonomy
              'field' => 'slug',
              'terms' => array($filter['keyword'])
          );
    }


  if( count($tax_query) > 0){
    $tax_query['relation'] = 'AND';
    $params['tax_query'] =  $tax_query;
  }

  if (!empty($filter['s'])) {
    $params['s']    =   trim($filter['s']);
/*  remove_filter('the_posts', 'relevanssi_query');
    remove_filter('posts_request', 'relevanssi_prevent_default_request',9);
    remove_filter('query_vars', 'relevanssi_query_vars');
*/

//     add_filter('the_posts', 'relevanssi_query');
// add_filter('posts_request', 'relevanssi_prevent_default_request', 10, 2 );
    require_once locate_template('/libs/search-everything/search-everything.php');
  }

   $temp = $query; 
 $query = null; 
 $query = new WP_Query($params); 

$query->query($params); 

 // $query->set('posts_per_page', 1);
 $query->query($query->query_vars);


  $search_results = $query->posts;
  $count_x_results = $query->found_posts;
  $post_count = $query->post_count;

 /* echo "<pre>";print_r($params);echo "<pre>";
  echo "<pre>";print_r($query->request);echo "<pre>";
  echo "<pre>";print_r($search_results);echo "<pre>";*/

  $response = '';
  if (count($search_results)>0) {
    foreach ($search_results as $item) {
      $resource_type_acf = get_field('resource_type_cf',$item->ID)->name;

      $resource_language_items =  get_field('resource_language_cf',$item->ID);

      $resource_language_arr = array();
      foreach ($resource_language_items as $key => $lang_item) {
        array_push($resource_language_arr, $lang_item->name);
      }

      $language_acf = implode('/ ', $resource_language_arr);

      $date_of_report_publication = date('Y',strtotime(get_field('date_of_report_publication',$item->ID)));

      $img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

      $img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
      
      $image = aq_resize( $img_url, 250, 166 , true,true,true); 
      $imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

        $response .= '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">
                <a href="'.esc_url( post_permalink($item->ID) ).'">
                  <figure>
                    '.$imageTag.'
                  </figure>
                  <aside class="withimage caption fullwidth fleft">
                  <span class="resource-type">'.$resource_type_acf.'</span>
                  <span class="resource-lang">'.$language_acf.'</span>
                  <span class="report-date">'.$date_of_report_publication.'</span>
                  <h4>'.$item->short_headline.'</h4>
                </aside>
                </a>
              </div>';
    }

    $pagination_nav = '';

    if(isset($filter['paged'])){

       if (function_exists(custom_pagination)) {
            $pagination_nav .= '<nav class="fleft fullwidth"">';
             $pagination_nav .=  custom_pagination($query->max_num_pages,"",$paged);
             $pagination_nav .= '</nav>';
            }
      }

  }else{
    $response .= '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">
                    <div class="alert alert-danger" role="alert">No search results</div>
                  </div>';
  }

  wp_reset_query();
  // $result['params'] = $params;
  // $result['request'] = $query->request;

   if($response === false) {
    $result['type'] = "error";
   } else {
    $result['type'] = "success";
    $result['html'] = $response;
    $result['pagination'] = $pagination_nav;

   }

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $result = json_encode($result);
    echo $result;
   }
   else {
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}

// === News Ajax Search and pagination 

add_action("wp_ajax_get_filtered_news", "get_filtered_news");
add_action("wp_ajax_nopriv_get_filtered_news", "get_filtered_news");

function get_filtered_news() {

  global $wpdb;

  $filter = $_REQUEST['filter'];

  $params = array();

  if ($filter['date']=='desc') {
    $params = array(
      'post_type'     =>  'news',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('news_limit','option'),
      'orderby' => 'date',
      'order' => 'desc'
    );
  }else{
    $params = array(
      'post_type'     =>  'news',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('news_limit','option'),
      'orderby'     => 'title',
      'order'       => 'asc'
    );
  }

  if($filter['paged']){
   global $paged;
    $params['paged'] = $filter['paged'];
    $paged = $filter['paged'];
  } 

  $tax_query = array();

    if (!empty($filter['news_category'])) {
    $tax_query[] = array(
        'taxonomy' => 'news_category',
              'field' => 'id',
              'terms' => $filter['news_category']
        );
    }

    if (!empty($filter['keyword'])) {
        $tax_query[] = array(
              'taxonomy' => 'keyword_categories', //or tag or custom taxonomy
              'field' => 'slug',
              'terms' => array($filter['keyword'])
          );
    }


  if( count($tax_query) > 0){
    $tax_query['relation'] = 'AND';
    $params['tax_query'] =  $tax_query;
  }

  if (!empty($filter['s'])) {
    $params['s']    =   trim($filter['s']);
    require_once locate_template('/libs/search-everything/search-everything.php');
  }

  $query =   new WP_Query($params);
  $search_results = $query->posts;
  $count_x_results = $query->found_posts;
  $post_count = $query->post_count;

/*  echo "<pre>";print_r($params);echo "<pre>";
  echo "<pre>";print_r($query->request);echo "<pre>";
  echo "<pre>";print_r($search_results);echo "<pre>";*/

  $response = '';
  if (count($search_results)>0) {
    foreach ($search_results as $item) {
      $date_of_report_publication = date('M d, Y',strtotime($item->post_date));

     $img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

      $img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
      
      $image = aq_resize( $img_url, 250, 166 , true,true,true); 
      $imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';

        $news_type = get_the_terms($item->ID,'news_category');
        
        $resourcetype = '';

        foreach ($news_type as $key => $item1) {
          $keyname = $key>0 ? '  ': '';
          $itemname =  $item1->name;          
          $resourcetype .= $keyname.$itemname;
        }

        $response .= '<div class="news_repeater fleft">
              <a href="'.esc_url( post_permalink($item->ID) ).'">
            <figure class="news_thumb">
             '.$imageTag.'
          </figure>
          <aside class="withimage">
            <div class="caption fullwidth fleft">
                  <span class="resource-type">
                        '.$resourcetype.'
                    </span>
                    <span class="report-date">'.$date_of_report_publication.'</span>
                  </div>
              <h2>'.$item->post_title.'</h2>
              '.$item->post_excerpt.'
          </aside>
          </a>
          </div>';      
    }

    $pagination_nav = '';

    if(isset($filter['paged'])){

       if (function_exists(custom_pagination)) {
            $pagination_nav .= '<nav class="fleft fullwidth"">';
             $pagination_nav .=  custom_pagination($query->max_num_pages,"",$paged);
             $pagination_nav .= '</nav>';
            }
      }

  }else{
    $response .= '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">
                    <div class="alert alert-danger" role="alert">No search results</div>
                  </div>';
  }

  wp_reset_query();
  // $result['params'] = $params;
  // $result['request'] = $query->request;

   if($response === false) {
    $result['type'] = "error";
   } else {
    $result['type'] = "success";
    $result['html'] = $response;
    $result['pagination'] = $pagination_nav;

   }

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $result = json_encode($result);
    echo $result;
   }
   else {
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}

// === Project Ajax Search and pagination 

add_action("wp_ajax_get_filtered_project", "get_filtered_project");
add_action("wp_ajax_nopriv_get_filtered_project", "get_filtered_project");

function get_filtered_project() {

  global $wpdb;

  $filter = $_REQUEST['filter'];

  $params = array();

  if ($filter['date']=='desc') {
    $params = array(
      'post_type'     =>  'project',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('news_limit','option'),
      'orderby' => 'date',
      'order' => 'desc'
    );
  }else{
    $params = array(
      'post_type'     =>  'project',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('news_limit','option'),
      'orderby'     => 'title',
      'order'       => 'asc'
    );
  }

  if($filter['paged']){
   global $paged;
    $params['paged'] = $filter['paged'];
    $paged = $filter['paged'];
  } 

  $tax_query = array();

    if (!empty($filter['project_category'])) {
    $tax_query[] = array(
        'taxonomy' => 'project_category',
              'field' => 'id',
              'terms' => $filter['project_category']
        );
    }

    if (!empty($filter['keyword'])) {
        $tax_query[] = array(
              'taxonomy' => 'keyword_categories', //or tag or custom taxonomy
              'field' => 'slug',
              'terms' => array($filter['keyword'])
          );
    }


  if( count($tax_query) > 0){
    $tax_query['relation'] = 'AND';
    $params['tax_query'] =  $tax_query;
  }

  if (!empty($filter['s'])) {
    $params['s']    =   trim($filter['s']);
    require_once locate_template('/libs/search-everything/search-everything.php');
  }

  $query =   new WP_Query($params);
  $search_results = $query->posts;
  $count_x_results = $query->found_posts;
  $post_count = $query->post_count;

/*  echo "<pre>";print_r($params);echo "<pre>";
  echo "<pre>";print_r($query->request);echo "<pre>";
  echo "<pre>";print_r($search_results);echo "<pre>";*/

  $response = '';
  if (count($search_results)>0) {
    foreach ($search_results as $item) {
      $date_of_report_publication = date('M d, Y',strtotime($item->post_date));

     $img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

      $img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
      
      $image = aq_resize( $img_url, 250, 166 , true,true,true); 
      $imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';
      
        $news_type = get_the_terms($item->ID,'project_category');
        $resourcetype = '';

        foreach ($news_type as $key => $item1) {
          $keyname = $key>0 ? ', ': '';
          $itemname =  $item1->name;          
          $resourcetype .= $keyname.$itemname;
        }

        $response .= '<div class="news_repeater fleft">
              <a href="'.esc_url( post_permalink($item->ID) ).'">
            <figure class="news_thumb">
              '.$imageTag.'
          </figure>
          <aside class="withimage">
            <div class="caption fullwidth fleft">
                  <span class="resource-type">
                        '.$resourcetype.'
                    </span>
                    <span class="report-date">'.$date_of_report_publication.'</span>
                  </div>
              <h2>'.$item->post_title.'</h2>
              '.$item->post_excerpt.'
          </aside>
          </a>
          </div>';      
    }

    $pagination_nav = '';

    if(isset($filter['paged'])){

       if (function_exists(custom_pagination)) {
            $pagination_nav .= '<nav class="fleft fullwidth"">';
             $pagination_nav .=  custom_pagination($query->max_num_pages,"",$paged);
             $pagination_nav .= '</nav>';
            }
      }

  }else{
    $response .= '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">
                    <div class="alert alert-danger" role="alert">No search results</div>
                  </div>';
  }

  wp_reset_query();
  // $result['params'] = $params;
  // $result['request'] = $query->request;

   if($response === false) {
    $result['type'] = "error";
   } else {
    $result['type'] = "success";
    $result['html'] = $response;
    $result['pagination'] = $pagination_nav;

   }

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $result = json_encode($result);
    echo $result;
   }
   else {
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}

// === Project Ajax Search and pagination 

add_action("wp_ajax_get_filtered_blog", "get_filtered_blog");
add_action("wp_ajax_nopriv_get_filtered_blog", "get_filtered_blog");

function get_filtered_blog() {

  global $wpdb;

  $filter = $_REQUEST['filter'];

  $params = array();

  if ($filter['date']=='desc') {
    $params = array(
      'post_type'     =>  'post',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('news_limit','option'),
      'orderby' => 'date',
      'order' => 'desc'
    );
  }else{
    $params = array(
      'post_type'     =>  'post',
      'post_status'   =>  array('publish'),
      'posts_per_page' => get_field('news_limit','option'),
      'orderby'     => 'title',
      'order'       => 'asc'
    );
  }

  if($filter['paged']){
   global $paged;
    $params['paged'] = $filter['paged'];
    $paged = $filter['paged'];
  } 

  $tax_query = array();

    if (!empty($filter['category'])) {
    $tax_query[] = array(
        'taxonomy' => 'category',
              'field' => 'id',
              'terms' => $filter['category']
        );
    }

    if (!empty($filter['keyword'])) {
        $tax_query[] = array(
              'taxonomy' => 'keyword_categories', //or tag or custom taxonomy
              'field' => 'slug',
              'terms' => array($filter['keyword'])
          );
    }


  if( count($tax_query) > 0){
    $tax_query['relation'] = 'AND';
    $params['tax_query'] =  $tax_query;
  }

  if (!empty($filter['s'])) {
    $params['s']    =   trim($filter['s']);
    require_once locate_template('/libs/search-everything/search-everything.php');
  }

  $query =   new WP_Query($params);
  $search_results = $query->posts;
  $count_x_results = $query->found_posts;
  $post_count = $query->post_count;

/*  echo "<pre>";print_r($params);echo "<pre>";
  echo "<pre>";print_r($query->request);echo "<pre>";
  echo "<pre>";print_r($search_results);echo "<pre>";*/

  $response = '';
  if (count($search_results)>0) {
    foreach ($search_results as $item) {
      $date_of_report_publication = date('M d, Y',strtotime($item->post_date));

     $img_url = wp_get_attachment_url(get_post_thumbnail_id($item->ID),'full');

      $img_url = (!empty($img_url)) ? $img_url: get_field('fallback_image','option');
      
      $image = aq_resize( $img_url, 250, 166 , true,true,true); 
      $imageTag = '<img src="'.$image.'" class="img-responsive" alt="">';
      
        $news_type = get_the_terms($item->ID,'category');
        $resourcetype = '';

        foreach ($news_type as $key => $item1) {
          $keyname = $key>0 ? ', ': '';
          $itemname =  $item1->name;          
          $resourcetype .= $keyname.$itemname;
        }

        $author_id=$item->post_author;

        $response .= '<div class="news_repeater fleft">
              <a href="'.esc_url( post_permalink($item->ID) ).'">
            <figure class="news_thumb">
              '.$imageTag.'
          </figure>
          <aside class="withimage">
            <div class="caption fullwidth fleft">                  
                   <span class="resource-type">'.get_the_author_meta('display_name',$author_id).'</span>
                  <span class="resource-lang">'.$resourcetype.'</span> 
                  <span class="report-date">'.$date_of_report_publication.'</span>
                  </div>
              <h2>'.$item->post_title.'</h2>
              '.wp_trim_words($item->post_content,45).'
          </aside>
          </a>
          </div>';      
    }

    $pagination_nav = '';

    if(isset($filter['paged'])){

       if (function_exists(custom_pagination)) {
            $pagination_nav .= '<nav class="fleft fullwidth"">';
             $pagination_nav .=  custom_pagination($query->max_num_pages,"",$paged);
             $pagination_nav .= '</nav>';
            }
      }

  }else{
    $response .= '<div class="posts_archive col-md-4 col-sm-6 col-xs-6 librarylist">
                    <div class="alert alert-danger" role="alert">No search results</div>
                  </div>';
  }

  wp_reset_query();
  // $result['params'] = $params;
  // $result['request'] = $query->request;

   if($response === false) {
    $result['type'] = "error";
   } else {
    $result['type'] = "success";
    $result['html'] = $response;
    $result['pagination'] = $pagination_nav;

   }

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $result = json_encode($result);
    echo $result;
   }
   else {
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}