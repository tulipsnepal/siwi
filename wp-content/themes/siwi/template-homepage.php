<?php
/*
* Template Name: Home
*/
get_header(); ?>
<main id="mainblock" role="main" class="homepage content-wrapper">
	<div class="container">
		<?php get_template_part('boxes/home','slider'); ?>
		<div class="row repeated-row">
		<div class="col-xs-12 news">
				<?php get_template_part('boxes/news','area'); ?>
			</div>
		</div>
		<div class="row repeated-row">
			<div class="col-xs-12">
				<div class="recommended_reading fullwidth fleft">
					<?php get_template_part('boxes/resources','area'); ?>
				</div>
			</div>
		</div>
		<div class="row repeated-row">
			<div class="col-xs-12">
				<div class="recommended_reading fullwidth fleft">
					<?php get_template_part('boxes/social','area'); ?>
				</div>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>