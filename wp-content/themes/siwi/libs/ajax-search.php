<?php
add_action("wp_ajax_get_filtered_resources", "get_filtered_resources");
add_action("wp_ajax_nopriv_get_filtered_resources", "get_filtered_resources");

function get_filtered_resources() {

  global $wpdb;

  $default_options = get_fields('en_options');

  $filter = $_REQUEST['filter'];

  $params = array();

  if ($filter['date']=='desc') {
    $params = array(
      'post_type'     =>  'resources',
      'post_status'   =>  array('publish'),
      'posts_per_page' => $default_options['number_of_resources'],
      'meta_key' => 'date_of_report_publication', //name of custom field
      'orderby' => 'meta_value_num',
      'order' => 'desc'
    );
  }else{
    $params = array(
      'post_type'     =>  'resources',
      'post_status'   =>  array('publish'),
      'posts_per_page' => $default_options['number_of_resources'],
      'meta_key'      => 'short_headline',
      'orderby'     => 'meta_value',
      'order'       => 'asc'
    );
  }

  if($filter['paged']){
    $params['paged'] = $filter['paged'];
     global $paged;
    $paged = $filter['paged'];
  }

  $tax_query = array();

  if (!empty($filter['focus_area'])) {
    $tax_query[] = array(
        'taxonomy' => 'focus_area',
            'field' => 'id',
            'terms' => $filter['focus_area']
      );
  }

  if (!empty($filter['region'])) {
    $tax_query[] = array(
        'taxonomy' => 'regions',
            'field' => 'id',
            'terms' => $filter['region']
      );
  }

  if (!empty($filter['language'])) {
    $tax_query[] = array(
        'taxonomy' => 'resource_language',
            'field' => 'id',
            'terms' => $filter['language']
      );
  }

  if (!empty($filter['type'])) {
    $tax_query[] = array(
        'taxonomy' => 'resource_type',
            'field' => 'id',
            'terms' => $filter['type']
     );
  }

  if( count($tax_query) > 0){
    $tax_query['relation'] = 'AND';
    $params['tax_query'] =  $tax_query;
  }

  if (!empty($filter['s'])) {
    $params['s']    =   trim($filter['s']);
    require_once locate_template('/libs/search-everything/search-everything.php');
  }

  $query =   new WP_Query($params);
  $search_results = $query->posts;
  $count_x_results = $query->found_posts;
  $post_count = $query->post_count;

  wp_reset_query();
  /* your args have to be the defualt query */
  query_posts($params);
  /* make sure you've got query_posts in your .php file */
  $pagination = Timber::get_pagination();
  wp_reset_query();

  $response = '';
  $response .= '<div class="resource-divs">';
  if (count($search_results)>0) {
    foreach ($search_results as $item) {
      $resource_type_acf = get_field('resource_type_cf',$item->ID)->name;

      $resource_language_items =  get_field('resource_language_cf',$item->ID);

      $resource_language_arr = array();
      foreach ($resource_language_items as $key => $lang_item) {
        array_push($resource_language_arr, $lang_item->name);
      }

      $language_acf = implode('/ ', $resource_language_arr);

      $date_of_report_publication = date('Y',strtotime(get_field('date_of_report_publication',$item->ID)));

        $response .= '<div class="col-md-4 col-sm-6 col-xs-12 librarylist">
                <a href="'.$item->post_name.'">
                  <figure>
                    <img src="" data-src="'.wp_get_attachment_url(get_post_thumbnail_id($item->ID)).'" class="img-responsive" alt="">
                  </figure>
                  <div class="caption fullwidth fleft">
                    <span class="resource-type">'.$resource_type_acf.'</span>
                    <span class="resource-lang">'.$language_acf.'</span>
                    <span class="report-date">'.$date_of_report_publication.'</span>
                    <h4><a href="'.$item->post_name.'">'.$item->short_headline.'</a></h4>
                  </div>
                </a>
              </div>';
    }

    $response .= '<div class="resource-divs">';

    if($pagination){
        $pagination_nav .= '
                        <ul class="pagination" data-paged="'.$filter['paged'].'">';
                        if($pagination['prev']){
                              $pagination_nav .= '<li class="first">
                              <button class="ajax-pagebutton" data-paged="prev" aria-label="Previous">'.icl_translate('front_texts','Previous','Previous').'</button>
                              </li>
                              ';
                        }
                           foreach ($pagination['pages'] as $page)
                           {
                            if($page['name'] == $filter['paged']){
                              $cls = 'active';
                            }else{
                              $cls ='';
                            }

                             $pagination_nav .='<li class="'.$cls.'">
                               <button class="ajax-pagebutton" data-paged="'.$page['name'].'" >'.$page['name'].'</button>
                               </li>';

                            }

                        if($pagination['next']){
                          $pagination_nav .='<li class="last"><button class="ajax-pagebutton" data-paged="next" aria-label="Next">'.icl_translate('front_texts','Next','Next').'</button></li>';
                        }

              $pagination_nav .='</ul>';
      }else{
        $pagination_nav = $pagination;
    }

  }else{
    $response .= '<div class="row"><div class="col-xs-12 librarylist">
                    <div class="alert alert-danger" role="alert">'.icl_translate('front_texts','No search results','No search results').'</div>
                  </div></div>';
  }

  // echo "<pre>";print_r($params);echo "<pre>";
  // echo "<pre>";print_r($query->request);echo "<pre>";
  // echo "<pre>";print_r($search_results);echo "<pre>";

  // $result['params'] = $params;
  // $result['request'] = $query->request;

   if($response === false) {
    $result['type'] = "error";
   } else {
    $result['type'] = "success";
    $result['html'] = $response;
    $result['pagination'] = $pagination_nav;

   }

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $result = json_encode($result);
    echo $result;
   }
   else {
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}
//=========================== AJAX FILTER FOR NEWS
add_action("wp_ajax_get_filtered_news", "get_filtered_news");
add_action("wp_ajax_nopriv_get_filtered_news", "get_filtered_news");

function get_filtered_news() {

  global $wpdb;

  $default_options = get_fields('en_options');

  $filter = $_REQUEST['filter'];

  // echo "<pre>";print_r($filter);exit;

  $params = array();

  if ($filter['date']=='desc') {
    $params = array(
      'post_type'     =>  'news',
      'post_status'   =>  array('publish'),
      'posts_per_page' => $default_options['number_of_news'],
      'orderby' => 'date',
      'order' => 'desc'
    );
  }else{
    $params = array(
      'post_type'     =>  'news',
      'post_status'   =>  array('publish'),
      'posts_per_page' => $default_options['number_of_news'],
      'orderby'=> 'title',
      'order' => 'asc'
    );
  }


   if($filter['paged']){
    $params['paged'] = $filter['paged'];
    global $paged;
    $paged = $filter['paged'];
  }

  $tax_query = array();

  if (!empty($filter['focus_area'])) {
    $tax_query[] = array(
        'taxonomy' => 'focus_area',
            'field' => 'id',
            'terms' => $filter['focus_area']
      );
  }

  if (!empty($filter['region'])) {
    $tax_query[] = array(
        'taxonomy' => 'regions',
            'field' => 'id',
            'terms' => $filter['region']
      );
  }

  if (!empty($filter['language'])) {
    $tax_query[] = array(
        'taxonomy' => 'resource_language',
            'field' => 'id',
            'terms' => $filter['language']
      );
  }

  if (!empty($filter['type'])) {
    $tax_query[] = array(
        'taxonomy' => 'news_type',
            'field' => 'id',
            'terms' => $filter['type']
     );
  }

  if( count($tax_query) > 0){
    $tax_query['relation'] = 'AND';
    $params['tax_query'] =  $tax_query;
  }

  if (!empty($filter['s'])) {
    $params['s']    =   trim($filter['s']);
    require_once locate_template('/libs/search-everything/search-everything.php');
  }

  $query =   new WP_Query($params);
  $search_results = $query->posts;
  $count_x_results = $query->found_posts;
  $post_count = $query->post_count;
  wp_reset_query();
  /* your args have to be the defualt query */
  query_posts($params);
  /* make sure you've got query_posts in your .php file */
  //$context['posts'] = Timber::get_posts();
  $pagination = Timber::get_pagination();
  wp_reset_query();

  $response = '';
  if (count($search_results)>0) {
    foreach ($search_results as $item) {

      $news_type = get_field('news_resource_type_cf',$item->ID)->name;

      $response .= '<div class="news_repeater fleft">
                      <a href="'.get_permalink($item->ID).'">';
                      if(get_post_thumbnail_id($item->ID)){
                          $response .=  ' <figure class="news_thumb">
                                           <img src="" data-src="'.wp_get_attachment_url(get_post_thumbnail_id($item->ID)).'" class="img-responsive" alt="" />
                                         </figure>';
                         $cls = 'withimage';
                      }

      $response .=   '<aside class="'.$cls.'">
                        <div class="caption fullwidth fleft">
                          <span class="resource-type">
                                '.$news_type.'
                                </span>
                          <span class="report-date">'.date('d F Y',strtotime($item->post_date)).'</span>
                        </div>
                        <h2>'.$item->post_title.'</h2>
                        <p> '.$item->post_content.'</p>
                      </aside>
                    </a>
                    </div>';
    }


     if($pagination){
              $pagination_nav .= '
                        <ul class="pagination" data-paged="'.$filter['paged'].'">';
                        if($pagination['prev']){
                              $pagination_nav .= '<li class="first">
                              <button class="ajax-pagebutton" data-paged="prev" aria-label="Previous">'.icl_translate('front_texts','Previous','Previous').'</button>
                              </li>
                              ';
                        }
                           foreach ($pagination['pages'] as $page)
                           {
                            if($page['name'] == $filter['paged']){
                              $cls = 'active';
                            }else{
                              $cls ='';
                            }

                             $pagination_nav .='<li class="'.$cls.'">
                               <button class="ajax-pagebutton" data-paged="'.$page['name'].'" >'.$page['name'].'</button>
                               </li>';

                            }

                        if($pagination['next']){
                          $pagination_nav .='<li class="last"><button class="ajax-pagebutton" data-paged="next" aria-label="Next">'.icl_translate('front_texts','Next','Next').'</button></li>';
                        }

              $pagination_nav .='</ul>';
            }else{
              $pagination_nav = $pagination;
            }

  }else{
    $response .= '<div class="row"><div class="col-xs-12 librarylist">
                    <div class="alert alert-danger" role="alert">'.icl_translate('front_texts','No search results','No search results').'</div>
                  </div></div>';
  }



  // echo "<pre>";print_r($params);echo "<pre>";
  // echo "<pre>";print_r($query->request);echo "<pre>";
  // echo "<pre>";print_r($search_results);echo "<pre>";

  // $result['params'] = $params;
  // $result['request'] = $query->request;

   if($response === false) {
    $result['type'] = "error";
   } else {
    $result['type'] = "success";
    $result['html'] = $response;
    $result['pagination'] = $pagination_nav;
   }

   if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $result = json_encode($result);
    echo $result;
   }
   else {
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}