<?php

/**
 * Plugin Name: App Bootstrap
 * Depends: Classes
 */

add_action('plugins_loaded',function(){

	// register custom post type starts

	/*new WPPlugins\WPExtend\RegisterPostType('resources',array(
			'singular_name'=>"resource",
			'supports'=>array(
				'editor','title','custom_fields','thumbnail'
			)
		));
*/
	// register custom post type ends

	// register taxonomy starts

	new WPPlugins\WPExtend\RegisterTaxonomy('topic_categories',array('publication'),array(
	    'show_ui' => true,
	    'rewrite' => array( 'slug' => 'topic_category' ),
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('partner_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'partner_category' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('perspective_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'perspective_category' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('publisher_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'publisher_category' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('author_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'author-category' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('keyword_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'keyword_category' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('resource_language_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'resource_language_caegory' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('region_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'region_category' )
	));

	new WPPlugins\WPExtend\RegisterTaxonomy('resource_type_categories',array('publication'),array(
		'show_ui' => true,
	    'rewrite' => array( 'slug' => 'resource_type_category' )
	));
     // register taxonomy ends
});
