<?php

namespace WPPlugins\WPExtend;

class RegisterTaxonomy{

	public $taxonomy;
	public $object_type;
	public $args = array();
	public $labels = array();

	public function __construct($taxonomy,$object_type,$args=array(),$labels=array())
	{
		$this->taxonomy = $taxonomy;
		$this->object_type = $object_type;

		$default_args = array(
			'public' 				=> true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'show_in_quick_edit'     => false,
			'query_var'             => true,
			'rewrite'               => array( 'slug' => $this->taxonomy  ),
			'capabilities' 			=> array(
				'manage_terms' => 'manage_categories',
				'edit_terms'   => 'manage_categories',
				'delete_terms' => 'manage_categories',
				'assign_terms' => 'edit_posts',
			)
		);

		$required_labels = array(
			'singular_name'	=>	str_replace('_', ' ', ucwords($this->taxonomy)),
			'plural_name'	=>	str_replace('_', ' ', ucwords($this->taxonomy))
		);

		$this->args = $args + $default_args;
		$this->labels = $labels + $required_labels;

		$this->args['labels'] = $this->labels + $this->default_labels();

		add_action('init',array($this,"register"));
	}

	public function register()
	{
		register_taxonomy($this->taxonomy,$this->object_type,$this->args);
	}

	public function default_labels()
	{
		return array(
			'name'                => $this->labels['plural_name'],
			'singular_name'       => $this->labels['singular_name'],
			'menu_name'           => $this->labels['plural_name'],
			'all_items'        => 'All '.$this->labels['plural_name'],
			'edit_item'           => 'Edit '.$this->labels['singular_name'],
			'view_item'           => 'View '.$this->labels['singular_name'],
			'update_item'           => 'Update '.$this->labels['singular_name'],
			'add_new_item'        => 'Add New '. $this->labels['singular_name'],
			'new_item_name'            => 'New '.$this->labels['singular_name'],
			'parent_item'   => 'Parent '.$this->labels['singular_name'],
			'parent_item_colon'   => 'Parent '.$this->labels['singular_name'].':',
			'search_items'        => 'Search '.$this->labels['plural_name'],
			'popular_items'        => 'Popular '.$this->labels['plural_name'],
			'separate_items_with_commas' => 'Separate '.strtolower($this->labels['plural_name']).' with commas',
		    'add_or_remove_items'        => 'Add or remove '.strtolower($this->labels['plural_name']),
		    'choose_from_most_used'      => 'Choose from most used '.strtolower($this->labels['plural_name']),
		    'not_found'                  => 'No '.strtolower($this->labels['plural_name']).' found'
		);
	}

}